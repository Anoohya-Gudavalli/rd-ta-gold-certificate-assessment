package com.epam.tests;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import org.testng.Assert;

import static io.restassured.RestAssured.given;

public class GetBookDetailsStepDefinitions {
    private Base base;
    public GetBookDetailsStepDefinitions(Base base){this.base = base;}

    @Given("user creates http request to book store books base uri")
    public void userCreatesHttpRequestToBookStoreBooksBaseUri() {
        RestAssured.baseURI = "https://demoqa.com";
        base.userRequestForBookDetails = given();
        base.userResponseForBookDetails = base.userRequestForBookDetails.request(Method.GET, "/BookStore/v1/Books");
    }

    @When("user gets the response of books")
    public void userGetsTheTitleOfBooks() {
        base.userResponseForBookDetails.getBody().prettyPrint();
        JsonPath j = new JsonPath(base.userResponseForBookDetails.asString());
//        int s = j.getInt("books.size()");
//        for (int i = 0; i < s; i++) {
//            String book_title = j.getString("books[" + i + "].title");
//            System.out.println(book_title);
//            String seeBookGitPocketGuide = j.getString("books[" + 1 + "].title");
//            Assert.assertEquals(seeBookGitPocketGuide,base.getBookStorePage().getBook1Title());

    }

    @Then("check title displayed in website")
    public void checkTitleDisplayedInWebsite() {
        JsonPath j = new JsonPath(base.userResponseForBookDetails.asString());
            String seeBookGitPocketGuide = j.getString("books[" + 0 + "].title");
            System.out.println(seeBookGitPocketGuide);
            Assert.assertEquals(base.getBookStorePage().getBook1Title(),seeBookGitPocketGuide);

    }

    @Then("check author displayed in website")
    public void checkAuthorDisplayedInWebsite() {
        JsonPath j = new JsonPath(base.userResponseForBookDetails.asString());
        String seeBookGitPocketGuide = j.getString("books[" + 0 + "].author");
        System.out.println(seeBookGitPocketGuide);
        Assert.assertEquals(base.getBookStorePage().getBook1Author(),seeBookGitPocketGuide);
    }

    @Then("check publisher displayed in website")
    public void checkPublisherDisplayedInWebsite() {
        JsonPath j = new JsonPath(base.userResponseForBookDetails.asString());
        String seeBookGitPocketGuide = j.getString("books[" + 0 + "].publisher");
        System.out.println(seeBookGitPocketGuide);
        Assert.assertEquals(base.getBookStorePage().getBook1Publisher(),seeBookGitPocketGuide);
    }


}
