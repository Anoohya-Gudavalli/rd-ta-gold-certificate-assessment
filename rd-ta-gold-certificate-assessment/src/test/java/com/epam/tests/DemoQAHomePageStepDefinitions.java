package com.epam.tests;

import com.epam.logger.LogLevel;
import com.epam.logger.LoggerType;
import com.epam.logger.loggerFactory.LoggerFactory;
import com.epam.logger.loggersImplementation.Log4J2Logger;

import io.cucumber.java.en.Given;

public class DemoQAHomePageStepDefinitions {
    private Base base;
    public DemoQAHomePageStepDefinitions(Base base){
        this.base = base;
    }

    @Given("user is navigated to bookstore page")
    public void userIsNavigatedToBookstorePage() {
        base.getDemoQAHomePage().navigateToBookStoreApplication();
    }
}
