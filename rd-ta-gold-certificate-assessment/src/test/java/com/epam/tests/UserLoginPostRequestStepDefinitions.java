package com.epam.tests;

import com.epam.pojos.RequestPojoForDemoQAUserLogin;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.Assert;

import static io.restassured.RestAssured.given;

public class UserLoginPostRequestStepDefinitions {
    private Base base;
    public UserLoginPostRequestStepDefinitions(Base base){this.base = base;}

    @Given("user creates http request to the base uri")
    public void userCreatesHttpRequestToTheBaseUri() {
        RestAssured.baseURI="https://demoqa.com";
        RestAssured.basePath="/Account/v1/User";
        base.userRequest= given();
    }

    @When("user set {string} and {string}")
    public void userSetUsernameAndPassword(String username, String password) {
       base.pojoObjectForRequest = new RequestPojoForDemoQAUserLogin();
    base.pojoObjectForRequest.setUserName(username);
   base.pojoObjectForRequest.setPassword(password);
    }


    @Then("user creates user login with post method")
    public void userCreatesUserLoginWithPostMethod() {
        base.userResponse = (Response) base.userRequest
                .header("Content-type","application/json")
                .body(base.pojoObjectForRequest)
                .post();
    }

    @Then("validate the status code with {int}")
    public void validateTheStatusCodeWith(int expectedStatusCode) {
        Assert.assertEquals(base.userResponse.getStatusCode(),expectedStatusCode);
    }
    @Then("validate username as {string}")
    public void validateUsernameAs(String expectedUserName) {
        Assert.assertEquals(base.pojoObjectForRequest.getUserName(),expectedUserName);
    }
}
