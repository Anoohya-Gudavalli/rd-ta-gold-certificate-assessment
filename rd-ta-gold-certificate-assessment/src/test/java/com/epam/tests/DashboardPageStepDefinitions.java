package com.epam.tests;

import io.cucumber.java.en.Then;
import org.testng.Assert;

public class DashboardPageStepDefinitions {
    private Base base;
    public DashboardPageStepDefinitions(Base base){
        this.base=base;
    }
    @Then("check username displayed on the dashboard page is {string}")
    public void checkUsernameDisplayedOnTheDashboardPageIs(String userName) {
        Assert.assertEquals(base.getDashboardPage().getUserNameValue(),userName);
    }
}
