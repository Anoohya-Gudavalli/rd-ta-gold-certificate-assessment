package com.epam.tests;

import com.epam.driver.browserFactory.DriverSingletonFactory;
import io.cucumber.java.AfterAll;
import io.cucumber.java.BeforeAll;

import static com.epam.tests.Base.driver;
import static com.epam.tests.Base.threadLocalDriver;


public class Hooks {

    @BeforeAll
    public static void setUp() {
        try {
            driver = DriverSingletonFactory.getDriver();
            threadLocalDriver.set(driver);
            System.out.println("Before Test Thread ID: " + Thread.currentThread().getId());
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
    @AfterAll()
    public static void stopBrowser(){
            DriverSingletonFactory.closeDriver();
            System.out.println("After Test Thread ID: "+Thread.currentThread().getId());
            threadLocalDriver.remove();
}
}
