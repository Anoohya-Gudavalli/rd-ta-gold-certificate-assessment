Feature:This feature about creating user login in demo.qa website using post method and validating
  status code and response body

  @smoke
  Scenario Outline:This Scenario is about validating status code of post request of user login
    Given user creates http request to the base uri
    And user set "<username>" and "<password>"
    When user creates user login with post method
   Then validate the status code with 201
    Examples:
      | username | password |
      | sweetu   |sweetU@2  |

     @regression @smoke
    Scenario Outline: This Scenario is about validating response body of post request of user login
      Given user creates http request to the base uri
      And user set "<username>" and "<password>"
      When user creates user login with post method
      Then validate username as "<username>"
      Examples:
        | username | password |
        | teetu   |teetU@2  |
