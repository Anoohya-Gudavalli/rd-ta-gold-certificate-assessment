package com.epam.driver.browserFactory;


import com.epam.driver.Browser;
import com.epam.driver.setUp.DriverSetUp;
import com.epam.driver.browserExceptions.BrowserNotReachable;
import com.epam.utility.FileReader;
import org.openqa.selenium.WebDriver;

import static com.epam.constants.SeleniumDriverProperties.*;


public class DriverSingletonFactory {

private static WebDriver driver;

private DriverSingletonFactory(){

}
    public static WebDriver getDriver() throws Exception {
        Browser browser = Browser.valueOf(FileReader.readPropertiesFile(BROWSER_PATH).getProperty("browser"));
        if(null==driver){
            switch (browser){
                case CHROME: {
                    driver = DriverSetUp.setUpChromeDriver();
                    driver.manage().window().maximize();
                    break;
                }
                case FIREFOX:{
                    driver = DriverSetUp.setUpFirefoxDriver();
                    driver.manage().window().maximize();
                    break;
                }
                case EDGE:{
                    driver = DriverSetUp.setUpEdgeDriver();
                    driver.manage().window().maximize();
                    break;
                }
                default:{
                    throw new BrowserNotReachable("Please launch the correct browser");
                }
            }

        }
        return driver;
    }

    public static void closeDriver(){
        driver.quit();
        driver=null;
    }

}
