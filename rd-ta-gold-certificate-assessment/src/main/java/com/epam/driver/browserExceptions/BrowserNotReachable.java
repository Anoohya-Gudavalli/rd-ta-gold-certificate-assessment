package com.epam.driver.browserExceptions;

public class BrowserNotReachable extends RuntimeException {

    public BrowserNotReachable(String errorMessage) {
        super(errorMessage);
    }
}
