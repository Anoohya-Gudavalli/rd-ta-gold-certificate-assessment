package screen;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.DataProvider;

public class DataProviderClass extends WebDriverClass
{
    public DataProviderClass(WebDriver driver) {
        super(driver);
    }

    @DataProvider(name = "dataProviderForOutputAssertions")
    public Object[][] dataProvider(){
        return new Object[][]{
                {driver.findElement(By.xpath("//*[@id=\"compute\"]/md-list/md-list-item[3]/div[1]")).getText(),"Commitment term: 1 Year"},
                {driver.findElement(By.xpath("//*[@id=\"compute\"]/md-list/md-list-item[4]/div[1]")).getText(),"Provisioning model: Regular"},
                {driver.findElement(By.xpath("/html/body/md-content/md-card/div/md-card-content[2]/md-card/md-card-content/div/div/div/md-content/md-list/md-list-item[5]/div[1]")).getText(),"Instance type: n1-standard-8"},
                {driver.findElement(By.xpath("//*[@id=\"compute\"]/md-list/md-list-item[1]/div[1]")).getText(),"Region: Frankfurt"},
                {driver.findElement(By.xpath("//*[@id=\"compute\"]/md-list/md-list-item[7]/div[1]/text()")).getText(),"Local SSD: 2x375 GiB"}
        };
    }
}
