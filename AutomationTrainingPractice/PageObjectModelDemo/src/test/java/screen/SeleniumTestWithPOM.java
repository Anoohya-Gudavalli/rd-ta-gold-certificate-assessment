package screen;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class SeleniumTestWithPOM {

    WebDriver driver;
    private SeleniumHomePage seleniumHomePage;
    @BeforeTest
    public void setUp(){
        //DriverUtility.getDriver();
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Anoohya_Gudavalli\\Documents\\Drivers\\ChromeDriver\\chromedriver.exe");
        System.setProperty("webdriver.chrome.logfile",".\\src\\main\\resources\\chromedriver.log");
        System.setProperty("webdriver.chrome.verboseLogging","true");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        driver = new ChromeDriver(options);
        driver.manage().window().maximize();
        driver.get("https://www.selenium.dev/");
        seleniumHomePage= new SeleniumHomePage(driver);
    }
    @Test
    public void testDocumentationFunctionality(){
        Assert.assertEquals(seleniumHomePage
                .navigateToDoc()
                .getDocumentation(),"Documentation");
    }
    @Test
    public void testDownloadFunctionality(){
        if(seleniumHomePage
                .getDownloadsText().equalsIgnoreCase("downloads")){
            seleniumHomePage
                    .navigateToDownloads();

        }
    }

}
