package screen;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SeleniumHomePage extends WebDriverClass {


    public SeleniumHomePage(WebDriver driver) {
      super(driver);
        PageFactory.initElements(this.driver,this);
    }
    //WebElements
    @FindBy(linkText ="Documentation")
    private WebElement documentation;
    @FindBy(linkText ="Downloads")
    private WebElement downloads;
    @FindBy(xpath = "//a[@href='/downloads']/span")
    private WebElement downloadsText ;

    public DocumentationPage navigateToDoc(){
    documentation.click();
    return new DocumentationPage(driver);
    }
    public DownloadsPage navigateToDownloads(){
        downloads.click();
        return new DownloadsPage(driver);

    }
    public String getDownloadsText(){
        return downloadsText.getText();
    }
}
