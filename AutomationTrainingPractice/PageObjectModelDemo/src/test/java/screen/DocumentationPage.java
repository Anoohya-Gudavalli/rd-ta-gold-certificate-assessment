package screen;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DocumentationPage extends WebDriverClass {

    public DocumentationPage(WebDriver driver){
        super(driver);
        PageFactory.initElements(this.driver,this);
    }
    @FindBy(xpath="//span[text()='Documentation']")
    private WebElement documentationText;
    public String getDocumentation(){
        return documentationText.getText();
    }
}
