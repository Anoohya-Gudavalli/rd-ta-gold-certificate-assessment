package com.epam.OCP;

public interface NotificationClient {
        void sendNotification(String userId);
}
