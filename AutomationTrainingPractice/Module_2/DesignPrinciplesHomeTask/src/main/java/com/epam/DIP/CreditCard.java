package com.epam.DIP;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CreditCard implements Card{
    public static final Logger LOGGER = LogManager.getLogger(CreditCard.class);
    @Override
    public void doTransaction(int amount) {
        LOGGER.info("tx done with CreditCard");
    }
}
