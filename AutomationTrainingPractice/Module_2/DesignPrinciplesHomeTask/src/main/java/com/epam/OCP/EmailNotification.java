package com.epam.OCP;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class EmailNotification  implements NotificationClient{
    public static final Logger LOGGER = LogManager.getLogger(EmailNotification.class);
    @Override
    public void sendNotification(String userId) {
        LOGGER.info("Email notification");
    }
}
