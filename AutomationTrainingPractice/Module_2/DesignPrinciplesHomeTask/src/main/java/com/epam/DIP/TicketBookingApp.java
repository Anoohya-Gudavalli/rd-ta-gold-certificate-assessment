package com.epam.DIP;

public class TicketBookingApp {
    public static void main(String[] args) {
       TicketBookingPayment ticketBookingPayment = new TicketBookingPayment(new DebitCard());
        ticketBookingPayment.doTransaction(5000);
    }

}
