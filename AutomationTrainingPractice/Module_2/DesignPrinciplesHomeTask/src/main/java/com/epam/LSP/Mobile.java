package com.epam.LSP;

public abstract class Mobile {
    public abstract void sendSMS(String message);

    public abstract void call(int Phone);

    public abstract void playMusic(String fileName);

    public abstract void playVideo(String videoFileName);
}

