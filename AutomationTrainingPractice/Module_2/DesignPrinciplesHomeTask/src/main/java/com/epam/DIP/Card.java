package com.epam.DIP;

public interface Card {
    void doTransaction(int amount);
}
