package com.epam.OCP;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class WatsappNotification implements NotificationClient {
    public static final Logger LOGGER = LogManager.getLogger(WatsappNotification.class);

    @Override
    public void sendNotification(String userId) {
        //System.out.println("Hi");
     LOGGER.info("Watsapp notification");
    }
}
