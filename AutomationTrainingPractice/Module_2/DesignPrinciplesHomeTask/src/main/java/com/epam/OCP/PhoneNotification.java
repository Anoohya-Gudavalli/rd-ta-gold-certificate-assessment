package com.epam.OCP;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PhoneNotification implements NotificationClient {
    public static final Logger LOGGER = LogManager.getLogger(PhoneNotification.class);
    @Override
    public void sendNotification(String userId) {
        LOGGER.info("Phone notification");
    }
}
