package com.epam;

class Trainer extends Employee{
    private String skills;
    private String certifications;
    public Trainer(int id, String name, float salary, Address address, String skills, String certifications) {
        super(id, name, salary, address);
        this.skills = skills;
        this.certifications = certifications;
    }

    void printDetails() {
        System.out.println("Trainer");
        super.printDetails();
        System.out.println("Skills: "+skills);
        System.out.println("Certifications: "+certifications);
    }
}
