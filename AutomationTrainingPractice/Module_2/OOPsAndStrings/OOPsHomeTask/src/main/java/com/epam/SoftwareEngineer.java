package com.epam;

class SoftwareEngineer extends Employee{
    private String  projectName;

    public SoftwareEngineer(int id, String name, float salary, Address address, String  projectName) {
        super(id, name, salary, address);
        this.projectName = projectName;
    }

    void printDetails() {
        System.out.println("Software Engineer");
        super.printDetails();
        System.out.println("Project name: "+projectName);
    }
}