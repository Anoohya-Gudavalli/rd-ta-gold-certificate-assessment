package com.epam;

class Employee{
    private int id;
    private String name;
    private float salary;
    Address address;

    public Employee(int id, String name, float salary, Address address) {
        this.id = id;
        this.name = name;
        this.salary = salary;
        this.address = address;
    }



    void printDetails() {
        System.out.println("Employee Details:");
        System.out.println("Id: "+id+" Name: "+name+" Salary: "+salary);
        System.out.println("Address: "+address.floorNumber+","+address.streetName+","+address.city+","+address.state+","+address.country);
    }
}
