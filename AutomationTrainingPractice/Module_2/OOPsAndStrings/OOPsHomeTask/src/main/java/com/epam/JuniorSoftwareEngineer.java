package com.epam;



class JuniorSoftwareEngineer extends Employee{
    private double assessmentScore;
    private String feedback;

    public JuniorSoftwareEngineer(int id, String name, float salary, Address address,double assessmentScore, String feedback) {
        super(id, name, salary, address);
        this.assessmentScore = assessmentScore;
        this.feedback = feedback;
    }

    public void printDetails(){
        System.out.println("Junior Software Engineer");
        super.printDetails();
        System.out.println("Assessment score: "+assessmentScore);
        System.out.println("Feedback: "+feedback);
    }

}