package com.epam;

class Course{
    private int courseId;
    private String courseName;
    private double courseDuration;

    public Course(int courseId, String courseName, double courseDuration) {
        this.courseId = courseId;
        this.courseName = courseName;
        this.courseDuration = courseDuration;
    }



        public void courseDetails(){
            System.out.println("Course Details: "+courseId+" "+ courseName +" "+ courseDuration+"hours");
        }

}