package com.epam;

import java.util.List;

public class WeekDays {
        public static String dayOfWeek(String day,int k) {
            List<String> days = List.of("sunday","monday","tuesday","wednesday","thursday","friday","saturday");
            day = day.toLowerCase();
            int idx = days.indexOf(day);
            String dayAfter= days.get( (idx+k)%7);
            return dayAfter;
        }

}
