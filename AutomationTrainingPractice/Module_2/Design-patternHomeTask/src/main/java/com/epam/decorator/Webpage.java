package com.epam.decorator;

public interface Webpage {
    int getRankSum();
}
