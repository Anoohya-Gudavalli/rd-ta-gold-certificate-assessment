package com.epam.decorator;

public class WebPageDecorator implements Webpage{
    private Webpage webpage;
    public WebPageDecorator(Webpage webpage){
       this.webpage= webpage;
    }
    @Override
    public int getRankSum() {
        return webpage.getRankSum();
    }
}
