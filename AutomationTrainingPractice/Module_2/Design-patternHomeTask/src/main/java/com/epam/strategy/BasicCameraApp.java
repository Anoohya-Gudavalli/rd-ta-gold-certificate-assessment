package com.epam.strategy;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BasicCameraApp implements PhoneCameraApp{
    public static final Logger LOGGER = LogManager.getLogger(BasicCameraApp.class);

    @Override
    public void takePhoto() {
        LOGGER.info("Photo taken");
    }

    @Override
    public void savePhoto() {
        LOGGER.info("Photo saved");
    }

    @Override
    public void sharePhoto(PictureSharingStrategy pictureSharingStrategy){
            PhoneCameraAppShareService sharingPicturesService = new PhoneCameraAppShareService();
            sharingPicturesService.setStrategy(pictureSharingStrategy);
            sharingPicturesService.sendPicturesThroughChosenWay(new User(""));
        }

}
