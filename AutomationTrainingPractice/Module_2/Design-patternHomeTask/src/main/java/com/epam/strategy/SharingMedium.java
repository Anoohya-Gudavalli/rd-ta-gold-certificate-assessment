package com.epam.strategy;

import org.apache.logging.log4j.LogManager;

public enum SharingMedium  {

 EMAIL(new EmailSharingClient()),TEXT(new TextSharingClient());

    public final PictureSharingStrategy sharingMedium;


   private SharingMedium(PictureSharingStrategy sharingMedium) {
       this.sharingMedium= sharingMedium;
    }


}
