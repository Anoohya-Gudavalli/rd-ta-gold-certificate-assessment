package com.epam.decorator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TestDecorator {
    public static final Logger LOGGER = LogManager.getLogger(TestDecorator.class);
    public static void main(String[] args) {

        Webpage widget = new DesktopWidget();
      Webpage afterAddingWidget = new MobileWidget(widget);
       Webpage afterAddingNewWidget = new NewWidget(afterAddingWidget);


        LOGGER.info("New total rank = "+afterAddingNewWidget.getRankSum());
    }
}
