package com.epam.builder;


public class Bicycle {

    BicycleBuilder bicycleBuilder;
    public Bicycle(BicycleBuilder bicycleBuilder) {
        this.bicycleBuilder = bicycleBuilder;
    }
    public BicycleBuilder getBicycleBuilder() {
        return bicycleBuilder;
    }


    public static BicycleBuilder builder() {
            return new BicycleBuilder();
        }

        public static class BicycleBuilder {
            // Required Parameters
            private Boolean fixGears;
            private Boolean fixDoubleSeats;
            private Boolean fixDoubleStands;
            private Boolean fixCarrier;

            public Boolean getFixGears() {
                return fixGears;
            }

            public Boolean getFixDoubleSeats() {
                return fixDoubleSeats;
            }

            public Boolean getFixDoubleStands() {
                return fixDoubleStands;
            }

            public Boolean getFixCarrier() {
                return fixCarrier;
            }

            public BicycleBuilder fixGears(final Boolean fixGears) {
                this.fixGears = getFixGears();
                return this;
            }
            public BicycleBuilder fixCarrier(final Boolean fixCarrier){
                this.fixCarrier = getFixCarrier();
                return this;
            }
            public BicycleBuilder fixDoubleSeats(final Boolean fixDoubleSeats){
                this.fixDoubleSeats = getFixDoubleSeats();
                return this;
            }

            public BicycleBuilder fixDoubleStands(final Boolean fixDoubleStands){
                this.fixDoubleStands= getFixDoubleStands();
                return this;
            }


            public Bicycle build() {

                Bicycle bicycle = new Bicycle(new BicycleBuilder());
                return bicycle;
            }
        }
    }

