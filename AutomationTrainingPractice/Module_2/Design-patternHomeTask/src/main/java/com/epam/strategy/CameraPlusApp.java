package com.epam.strategy;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CameraPlusApp implements PhoneCameraApp{
    public static final Logger LOGGER = LogManager.getLogger(CameraPlusApp.class);
    @Override
    public void takePhoto() {
            System.out.println("Photo taken");
    }

    @Override
    public void savePhoto() {
        System.out.println("Photo saved");
    }

    @Override
    public void sharePhoto(PictureSharingStrategy pictureSharingStrategy) {
        PhoneCameraAppShareService sharingPicturesService = new PhoneCameraAppShareService();
        sharingPicturesService.setStrategy(pictureSharingStrategy);
        sharingPicturesService.sendPicturesThroughChosenWay(new User(""));
    }
}
