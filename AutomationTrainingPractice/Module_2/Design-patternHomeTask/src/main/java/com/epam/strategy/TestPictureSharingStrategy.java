package com.epam.strategy;

public class TestPictureSharingStrategy {
    public static void main(String[] args) {
        BasicCameraApp basicCameraApp = new BasicCameraApp();
        basicCameraApp.takePhoto();
        basicCameraApp.savePhoto();
        basicCameraApp.sharePhoto(SharingMedium.EMAIL.sharingMedium);
        CameraPlusApp cameraPlusApp= new CameraPlusApp();
        cameraPlusApp.sharePhoto( SharingMedium.TEXT.sharingMedium);

    }
}
