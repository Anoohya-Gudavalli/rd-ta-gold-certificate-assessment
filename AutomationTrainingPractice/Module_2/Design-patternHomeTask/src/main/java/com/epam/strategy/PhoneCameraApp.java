package com.epam.strategy;

public interface PhoneCameraApp {
    void takePhoto();
    void savePhoto();
    void sharePhoto(PictureSharingStrategy pictureSharingStrategy);
}
