package com.epam.singleton;

public class TestCandyMaker {
    public static void createInstance(){
        CandyMaker candyMaker = CandyMaker.getGlobalInstance();
        System.out.println(candyMaker);
    }
    public static void main(String[] args) {

        TestCandyMaker.createInstance();
        TestCandyMaker.createInstance();
        TestCandyMaker.createInstance();

    }
}
