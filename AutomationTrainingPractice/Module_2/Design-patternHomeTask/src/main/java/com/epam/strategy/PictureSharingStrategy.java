package com.epam.strategy;

public interface PictureSharingStrategy {
    public void sendPicture(User user);
}
