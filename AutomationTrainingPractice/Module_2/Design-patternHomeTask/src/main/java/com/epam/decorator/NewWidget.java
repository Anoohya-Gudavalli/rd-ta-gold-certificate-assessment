package com.epam.decorator;

public class NewWidget extends WebPageDecorator {
    int rank = 10;

    public NewWidget(Webpage webpage) {
        super(webpage);
    }

    @Override
    public int getRankSum() {
        return rank+super.getRankSum();
    }
}
