package com.epam.strategy;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TextSharingClient implements PictureSharingStrategy {
    public static final Logger LOGGER = LogManager.getLogger(TextSharingClient.class);
    @Override
    public void sendPicture(User user) {
        LOGGER.info("Picture shared through text");
    }
}
