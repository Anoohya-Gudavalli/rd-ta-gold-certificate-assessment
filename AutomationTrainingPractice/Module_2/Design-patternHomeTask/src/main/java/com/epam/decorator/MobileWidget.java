package com.epam.decorator;

public class MobileWidget extends WebPageDecorator{
    int rank=5;

    public MobileWidget(Webpage webpage) {
        super(webpage);
    }

    @Override
    public int getRankSum() {
        return rank+super.getRankSum();
    }
}
