package com.epam.builder;

public class BicycleCustomizationApp {
    public static void main(String[] args) {
        Bicycle customer1 = Bicycle.builder()
                .fixGears(true)
                .fixDoubleSeats(true)
                .fixDoubleStands(false)
                .fixCarrier(false)
                .build();
        System.out.println(customer1);
        Bicycle customer2 = Bicycle.builder()
                .fixGears(true)
                .fixDoubleSeats(true)
                .fixDoubleStands(false)
                .fixCarrier(true)
                .build();
        System.out.println(customer2);



    }
}
