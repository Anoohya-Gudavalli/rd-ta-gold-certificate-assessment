package com.epam.strategy;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class EmailSharingClient implements PictureSharingStrategy {
    public static final Logger LOGGER = LogManager.getLogger(EmailSharingClient.class);

    @Override
    public void sendPicture(User user) {
        LOGGER.info("Picture sent through email");
    }
}
