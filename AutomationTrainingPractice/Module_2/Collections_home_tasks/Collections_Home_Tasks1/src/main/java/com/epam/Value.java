package com.epam;

class Value implements Comparable<Value>{
    private int value;
    public Value(int value) {
        this.value=value;
    }
    public int getValue()
    {
        return value;
    }
    @Override
    public String toString()
    {
        return "Value : "+value;
    }
    @Override
    public int compareTo(Value v) {
        if(this.getValue()<v.getValue()) {
            return 1;
        }
        else if(this.getValue()>v.getValue()) {
            return -1;
        }
        else {
            return 0;
        }
    }
}