package com.epam;

import java.util.Comparator;

class reverse implements Comparator<Value> {
    @Override
    public int compare(Value v1, Value v2) {
        if (v1.getValue() <v2.getValue())
            return 1;
        else if (v1.getValue() > v2.getValue())
            return -1;
        else
            return 0;
    }
}