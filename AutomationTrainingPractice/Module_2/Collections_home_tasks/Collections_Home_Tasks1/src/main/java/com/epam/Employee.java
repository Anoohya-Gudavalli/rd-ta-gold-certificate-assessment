package com.epam;

class Employee implements Comparable<Employee>{
    private String name;
    public Employee(String name) {
        this.name=name;
    }
    public String getName()
    {
        return name;
    }
    public String toString()
    {
        return name;
    }
    @Override
    public int compareTo(Employee e) {
        return this.getName().compareTo(e.getName());
    }
}

