package com.epam;
public class GenericUtility{

		//Non-Generic Method
		/*
		 * Convert this non-generic method to a generic method so that it can
		 * compare String, Double, Float and any custom class object like 
		 * in our case i.e Customer
		 */
	
	//FirstPackage.Customer.customerSalary;
	public static <T extends Comparable<T>>  T receiveLeastValue(T object1, T object2) {

	 if((object1.compareTo(object2)==-1)) {
			return object1 ;
		}
	return object2;
		}
	public static  String receiveLeastValue(Customer object1, Customer object2) {
		Double salary1 = object1.getCustomerSalary();
        Double salary2 = object2.getCustomerSalary();
		 if((salary1.compareTo(salary2)==-1)) {
				return   (object1.getCustomerName()+","+object1.getCustomerSalary());
			}
			return  (object2.getCustomerName()+","+object2.getCustomerSalary());
			}
}		
		