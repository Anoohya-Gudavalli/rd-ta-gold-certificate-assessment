package com.epam.lambdaTasksWithoutUsingLambda;

public class Main {
    public static void main(String[] args) {

       UtilityClass utilityClass = new UtilityClass();
       utilityClass.isPalindrome();
       utilityClass.secondGreatestNumber();
       utilityClass.isRotation();
       utilityClass.threadRunnable();
       utilityClass.reverseList();
       utilityClass.sortEmployeeList();
       utilityClass.reverseTreeSet();
       utilityClass.sortEmployeeTreeSet();
       utilityClass.sortTreeMapInDescendingOrder();
       utilityClass.sortEmployeesTreeMapInDescendingOrder();
       utilityClass.sortEmployees();

    }
}
