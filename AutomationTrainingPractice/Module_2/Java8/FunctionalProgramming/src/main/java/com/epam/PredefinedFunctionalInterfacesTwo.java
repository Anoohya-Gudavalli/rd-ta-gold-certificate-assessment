package com.epam;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class PredefinedFunctionalInterfacesTwo {
    public static void main(String[] args) throws IOException {
        ProductStore productStore = new ProductStore();
        List<Product> productsList = productStore.getProducts();
        //PredefinedFunctionalInterface2
        //Consumer
        //1.
        System.out.println("consumer to print the product to appropriate medium depending on the print parameter. If the print parameter is set to file, consumer shall log the product to file, if not print on the console.");
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter input to choose medium to print File/Console");
        String input = sc.nextLine();
        System.out.println("Enter Product Index");
        int productIndex = sc.nextInt();
        Consumer<Product> consumerPrintProductTo = p->
        {
            if(input.equalsIgnoreCase("File")) {
                try {
                    File f = new File("ProductsList.txt");
                    FileOutputStream oos = new FileOutputStream(f);
                    PrintStream ps = new PrintStream(oos);
                    ps.print(p);
                    ps.close();
                    System.out.println("Product written to file");
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
            else if(input.equalsIgnoreCase("Console")) {
                System.out.print(p);
                System.out.println("Product written to console");
            }
        };
        consumerPrintProductTo.accept(productsList.get(productIndex));

        System.out.println();

        //2.
        System.out.println("Consumer to update the grade of the product as 'Premium' if the price is > 1000/-. Given the product list, update the grade for each product and print all of the products.");
        Consumer<Product> consumerUpdateToPremium = p->
        {if(p.getPrice()>1000)
            p.setGrade("Premium");
        };
        forEach(productsList,consumerUpdateToPremium);
        System.out.println(productsList);
        System.out.println();
        //3.
        System.out.println("Consumer to update the name of the product to be suffixed with '*' if the price of product is > 3000/-. Given the product list, update the name for each product and print all of the products.");

        Consumer<Product> consumerUpdateName = p->
        {
            if(p.getPrice()>3000)
                p.setName("*"+p.getName());
        };
        forEach(productsList,consumerUpdateName);
        System.out.println(productsList);
        //4.
        System.out.println();
        System.out.println("Print all the Premium grade products with name suffixed with '*'.");
        Consumer<Product> consumerPN = p->
        {
            if(p.getGrade().equals("Premium")&&p.getName().startsWith("*")){
                System.out.println(p);
            }
        };
        forEach(productsList,consumerPN);
        System.out.println();
        //Supplier
        //1.
        System.out.println("supplier to produce a random product.");
        Supplier<Product> randomProductSupplier = ()->{

            Random random = new Random();
            int ProductIndex = random.nextInt(productsList.size());
            return productsList.get(ProductIndex);
        };
        System.out.println(randomProductSupplier.get());
        System.out.println();
        //2.
        System.out.println("supplier to produce a random OTP.");
        Supplier<String> randomOtpSupplier = ()->{
            String otp = "";
            Random random = new Random();
            for(int i=0;i<4;i++) {
                int randomNumber = random.nextInt(9);
                otp = otp+randomNumber;
            }
            return otp;
        };
        System.out.println(randomOtpSupplier.get());
    }
    private static void forEach(List<Product> productsList, Consumer<Product> consumerP) {
        for (Product p : productsList) {
            consumerP.accept(p);
        }
    }
}
