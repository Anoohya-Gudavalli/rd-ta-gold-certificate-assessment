package com.epam;

import java.util.List;
import java.util.stream.Collectors;

public class PredefinedFunctionalInterfacesThree {
    public static void main(String[] args) {
        ProductStore productStore = new ProductStore();
        List<Product> productsList = productStore.getProducts();
        //PredefinedFunctionalInterface3
        //1.function to calculate the cost of all products in a given list of products.
        System.out.println("Total cost of Products "+productsList.stream()
                .collect(Collectors.summingDouble(Product::getPrice)));
        //2.function to calculate the cost of all products whose prices is > 1000/- in the given list of products.
        System.out.println();
        System.out.println("Cost of products whose prices is > 1000/-  "+productsList.stream()
                .filter(product->product.getPrice()>1000)
                .mapToDouble(product->product.getPrice())
                .sum());
        //3.function to calculate the cost of all electronic products in the given list of products.
        System.out.println();
        System.out.println("cost of all electronic products "+productsList.stream()
                .filter(product->product.getCategory().equals("Electronics"))
                .mapToDouble(product->product.getPrice())
                .sum());
        System.out.println();
        //4.function to get all the products whose price is > 1000/- and belongs to electronic category.
        System.out.println("products whose price is > 1000/- and belongs to electronic category. ");
        productsList.stream()
                .filter(product->product.getCategory().equals("Electronics")&&product.getPrice()>1000)
                .forEach(System.out::println);
    }
}
