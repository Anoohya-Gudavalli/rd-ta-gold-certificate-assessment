package com.epam;

import java.io.IOException;
import java.util.List;
import java.util.function.Predicate;

public class PredefinedFunctionalInterfacesOne {
    public static void main(String args[]) throws IOException {
        ProductStore productStore = new ProductStore();
        List<Product> productsList = productStore.getProducts();
        Predicate<Product> predicatePriceLessThan = product-> product.getPrice()<100;
        Predicate<Product> predicateCategory =  product -> (product.getCategory().equals("Electronics"));
        Predicate<Product> predicatePrice = product-> product.getPrice()>100;
        //PredefinedFunctionalInterface1
        // 1.productsGreaterThan1000
        System.out.println("price of  product is greater than 1000/-.");
        Predicate<Product> productsGreaterThan1000=product -> (product.getPrice()>1000);
        productsList.stream()
                .filter( productsGreaterThan1000)
                .forEach(System.out::println);
        System.out.println();
        //2.productsInElectronics
        System.out.println("Products in electronic category");
        Predicate<Product> productsInElectronics=product -> (product.getCategory().equals("Electronics"));
        productsList.stream()
                .filter( productsInElectronics)
                .forEach(System.out::println);
        System.out.println();
        //3.productsInElectronics and greater than 100
        System.out.println("Products in Electronics category and price greater than 100");
        productsList.stream()
                .filter(predicateCategory.and(predicatePrice))
                .forEach(System.out::println);
        System.out.println();
        //4.productsInElectronics or greater than 100
        System.out.println("Products in Electronics category or price greater than 100 ");
        productsList.stream()
                .filter(predicateCategory.or(predicatePrice))
                .forEach(System.out::println);
        System.out.println();
        //5.productsInElectronics less than 100
        System.out.println("Products in Electronics category and price less than 100");
        productsList.stream()
                .filter (predicateCategory.and(predicatePriceLessThan))
                .forEach(System.out::println);



    }
}
      
    