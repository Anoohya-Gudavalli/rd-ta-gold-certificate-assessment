package com.epam.lambdaTasksWithoutUsingLambda;


import com.epam.Employee;
import com.epam.Value;

import java.util.*;

public class UtilityClass {
    Scanner sc = new Scanner(System.in);

    //1.if a given string is a palindrome
    public void isPalindrome() {
        System.out.println("Enter a string");
        String string = sc.nextLine();
        String rev = "";
        for (int i = string.length() - 1; i >= 0; i--) {
            rev = rev + string.charAt(i);
        }
        if (string.equalsIgnoreCase(rev))
            System.out.println("Is a Palindrome");
        else
            System.out.println("Not a palindrome");
    }

    //2.Second-greatest number in list
    public void secondGreatestNumber() {
        ArrayList<Integer> List = new ArrayList<>();
        List.add(31);
        List.add(26);
        List.add(23);
        List.add(41);
        List.add(15);
        Collections.sort(List, Collections.reverseOrder());
        Integer secondGreatestNumber = List.get(1);
        System.out.println(secondGreatestNumber);
    }

    //3.if two strings are rotations of each other
    public void isRotation() {
        System.out.println("Enter string1");
        String string1 = sc.nextLine();
        System.out.println("Enter string2");
        String string2 = sc.nextLine();
        String resultString = string1 + string1;
        if (string1.length() == string2.length()) {
            if (resultString.contains(string2)) {
                System.out.println(string2 + " Is a Rotation of " + string1);
            } else
                System.out.println(string2 + " Not a Rotation of " + string1);
        } else
            System.out.println(string2 + " Not a Rotation of " + string1);
    }

    //4.Use Runnable interface to start a new thread and print numbers from
    public void threadRunnable() {

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 10; i++)
                    System.out.println(i);
            }
        };
        new Thread(runnable).start();
    }

    //5.Comparator interface to sort given list of numbers in reverse order
    public void reverseList() {
        ArrayList<Value> ValueList = new ArrayList<>();
        ValueList.add(new Value(31));
        ValueList.add(new Value(26));
        ValueList.add(new Value(23));
        ValueList.add(new Value(41));
        ValueList.add(new Value(15));
        class ListComparator implements Comparator<Value> {
            public int compare(Value v1, Value v2) {
                if (v1.getValue() < v2.getValue())
                    return 1;
                else if (v1.getValue() > v2.getValue())
                    return -1;
                else
                    return 0;
            }
        }
        ListComparator listComparator = new ListComparator();
        Collections.sort(ValueList, listComparator);
        System.out.println("Reverse List : ");
        System.out.println(ValueList);
    }

    //6.Comparator interface to sort given list of Employees in the alphabetic order of their name
    public void sortEmployeeList() {
        List<Employee>employeesList = getEmployeeList();
        class CompareEmployeeList implements Comparator<Employee> {
            @Override
            public int compare(Employee e1, Employee e2) {
                return (e2.getName()).compareTo(e1.getName());
            }
        }
        CompareEmployeeList compareEmployeeList = new CompareEmployeeList();
        Collections.sort(employeesList, compareEmployeeList);
    }

    //7.TreeSet that sorts the given set of numbers in reverse order
    public void reverseTreeSet() {
        TreeSetComparator treeSetComparator = new TreeSetComparator();
        TreeSet<Value> IntegerTreeSet = new TreeSet<>(treeSetComparator);
        IntegerTreeSet.add(new Value(35));
        IntegerTreeSet.add(new Value(44));
        IntegerTreeSet.add(new Value(13));
        IntegerTreeSet.add(new Value(27));
        IntegerTreeSet.add(new Value(31));

        System.out.print("TreeSet Reverse order ");
        for (Value value : IntegerTreeSet) {
            System.out.println(value);
        }
    }

    class TreeSetComparator implements Comparator<Value> {

        @Override
        public int compare(Value v1, Value v2) {
            if (v1.getValue() < v2.getValue()) {
                return 1;
            } else if (v1.getValue() > v2.getValue()) {
                return -1;
            } else {
                return 0;
            }
        }
    }

    //8.TreeSet that sorts the given set of Employees in the alphabetic order of their name
    public void sortEmployeeTreeSet() {
        CompareEmployee compareEmployeeTreeSet = new CompareEmployee();
        TreeSet<Employee> employeesTreeSet = new TreeSet<>(compareEmployeeTreeSet);

        employeesTreeSet.add(new Employee("Krishna"));
        employeesTreeSet.add(new Employee("Shiva"));
        employeesTreeSet.add(new Employee("Radha"));
        employeesTreeSet.add(new Employee("Arjun"));
        employeesTreeSet.add(new Employee("Ram"));
        System.out.println("Employee TreeSet :");
        for (Employee emp : employeesTreeSet) {
            System.out.println(emp);
        }

    }

    //9.TreeMap that sorts the given set of values in descending order
    public void sortTreeMapInDescendingOrder() {
        Map<Integer, Integer> IntegerTreeMap = new TreeMap<Integer, Integer>();
        IntegerTreeMap.put(1, 35);
        IntegerTreeMap.put(2, 44);
        IntegerTreeMap.put(3, 13);
        IntegerTreeMap.put(4, 27);
        IntegerTreeMap.put(5, 31);
        class CompareIntegers implements Comparator<Integer> {
            @Override
            public int compare(Integer v1, Integer v2) {
                if (IntegerTreeMap.get(v1) < IntegerTreeMap.get(v2))
                    return 1;
                else if (IntegerTreeMap.get(v1) > IntegerTreeMap.get(v2))
                    return -1;
                else
                    return 0;
            }
        }
        CompareIntegers compareIntegers = new CompareIntegers();
        Map<Integer, Integer> IntegerSortedTreeMap = new TreeMap<>(compareIntegers);
        IntegerSortedTreeMap.putAll(IntegerTreeMap);
        System.out.println("TreeMap values in descending order");
        Set<Map.Entry<Integer, Integer>> entries = IntegerSortedTreeMap.entrySet();
        for (
                Map.Entry<Integer, Integer> entry : entries) {
            System.out.println(entry.getValue());
        }
    }
    //10.TreeMap that sorts the given set of employees in descending order of their name
    public void sortEmployeesTreeMapInDescendingOrder() {
        TreeMap<String, String> EmployeeTreeMap = new TreeMap<String, String>();
        EmployeeTreeMap.put("1", "Krishna");
        EmployeeTreeMap.put("2", "Shiva");
        EmployeeTreeMap.put("3", "Radha");
        EmployeeTreeMap.put("4", "Arjun");
        EmployeeTreeMap.put("5", "Ram");
        System.out.println("EmployeeTreeMap:");
        class CompareEmployeeTreeMap implements Comparator<String>{
            @Override
            public int compare(String v1, String v2) {
                return EmployeeTreeMap.get(v2).compareTo(EmployeeTreeMap.get(v1));
            }
        }
        CompareEmployeeTreeMap compareEmployeeTreeMap = new CompareEmployeeTreeMap();
        Map<String,String>EmployeeSortedTreeMap= new TreeMap<>(compareEmployeeTreeMap);
        EmployeeSortedTreeMap.putAll(EmployeeTreeMap);
        Set<Map.Entry<String, String>> entrieset = EmployeeSortedTreeMap.entrySet();
        for (
                Map.Entry<String, String> entry : entrieset) {
            System.out.println(entry.getValue());
        }
    }
    //11.Use Collections.Sort to sort the given list of Employees in descending order of their name
    public void sortEmployees() {
        List<Employee>employeesList = getEmployeeList();
        employeesList.sort(Comparator.comparing(Employee::getName, Comparator.reverseOrder()));
        System.out.println("Employee List reversed:");
        for (Employee emp : employeesList) {
            System.out.println(emp);
        }
    }
    public List<Employee> getEmployeeList(){
        List<Employee> employeesList = new ArrayList<>();
        employeesList.add(new Employee("Krishna"));
        employeesList.add(new Employee("Shiva"));
        employeesList.add(new Employee("Radha"));
        employeesList.add(new Employee("Arjun"));
        employeesList.add(new Employee("Ram"));
        return employeesList;
    }
        class CompareEmployee implements Comparator<Employee> {
            public int compare(Employee e1, Employee e2) {
                return (e2.getName()).compareTo(e1.getName());
            }
        }
}