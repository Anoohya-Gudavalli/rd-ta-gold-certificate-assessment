package com.epam;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class BiFunctionTasks {
    public static void main(String[] args) {
        ProductStore productstore = new ProductStore();
        List<Product> productsList = productstore.getProducts();
        //1.BiFunction to create product
        BiFunction<String, Double, Product> f1 = (name, price) -> new Product(null, name, price, null);
        Product p1 = f1.apply("WashingMachine", 30000.00);
        System.out.println(p1);
        //2. Bi-Function to calculate the cost of products. A cart is a map of product and quantity. Given the cart, calculate the cost of the cart.
        Map<Product, Integer> cart = new HashMap<>();
        cart.put(productsList.get(0), 2);
        cart.put(productsList.get(1), 1);
        cart.put(productsList.get(4), 3);

        BiFunction<Product, Integer, Double> totalCostBiFunction = (product, quantity) -> product.getPrice() * quantity;
        cart.entrySet().stream().mapToDouble(product -> totalCostBiFunction.apply(product.getKey(), product.getValue())).sum();

    }

}

