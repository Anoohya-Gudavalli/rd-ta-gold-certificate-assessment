package com.epam;

import java.util.Random;
import java.util.Scanner;
import java.util.function.IntConsumer;
import java.util.function.IntPredicate;
import java.util.function.IntSupplier;

public class BinaryUnaryOperatorTasks {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        //1.IntPredicate to verify if the given number is a prime number
        IntPredicate intPredicate = number->{
            for(int i =1;i<=number/2;i++) {
                if(number%i==0) {
                    return false;
                }
            }
            return true;
        };
        System.out.println(intPredicate.test(n));;
        //2.IntConsumer to print square of the given number
        IntConsumer intConsumer = number->System.out.println( number*number);
        intConsumer.accept(n);
        //3.IntSupplier to give random int below 5000.
        IntSupplier intSupplier = ()->{
            Random random= new Random();
            int randomNumber = random.nextInt(5000);
            return randomNumber;
        };
        System.out.println( intSupplier.getAsInt());


    }
}