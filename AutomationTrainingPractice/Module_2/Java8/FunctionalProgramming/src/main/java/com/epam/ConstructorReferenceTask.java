package com.epam;

class Employees{

   private String name;
   private String account;
    private double salary;
    public Employees( String name,String account, double salary)
    {

        this.name = name;
        this.account = account;
        this.salary = salary;
    }
    public String getName()
    {
        return name;
    }
    public String getAccount()
    {
        return account;
    }
    public double getSalary()
    {
        return salary;
    }

    @Override
    public String toString()
    {
        return " Name : "+name
                +",Account : "+account
                +", Salary : "+salary;
    }
}
interface EmployeeDetails{
    Employees getEmployee(String name,String account,double salary);
}
public class ConstructorReferenceTask {
    public static void main(String[] args) {
        EmployeeDetails employee = Employees::new;//Constructor reference
        System.out.println(employee.getEmployee("Ram", "872134967898", 70000));

    }
}