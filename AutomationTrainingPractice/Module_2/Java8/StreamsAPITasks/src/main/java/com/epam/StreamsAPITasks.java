package com.epam;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamsAPITasks {
    public static void main(String[] args) throws IOException {

        List<Employee> employeeList = new ArrayList<Employee>();

        employeeList.add(new Employee(111, "Jiya Brein", 32, "Female", "HR", 2011, 25000.0));
        employeeList.add(new Employee(122, "Paul Niksui", 25, "Male", "Sales And Marketing", 2015, 13500.0));
        employeeList.add(new Employee(133, "Martin Theron", 29, "Male", "Infrastructure", 2012, 18000.0));
        employeeList.add(new Employee(144, "Murali Gowda", 28, "Male", "Product Development", 2014, 32500.0));
        employeeList.add(new Employee(155, "Nima Roy", 27, "Female", "HR", 2013, 22700.0));
        employeeList.add(new Employee(166, "Iqbal Hussain", 43, "Male", "Security And Transport", 2016, 10500.0));
        employeeList.add(new Employee(177, "Manu Sharma", 35, "Male", "Account And Finance", 2010, 27000.0));
        employeeList.add(new Employee(188, "Wang Liu", 31, "Male", "Product Development", 2015, 34500.0));
        employeeList.add(new Employee(199, "Amelia Zoe", 24, "Female", "Sales And Marketing", 2016, 11500.0));
        employeeList.add(new Employee(200, "Jaden Dough", 38, "Male", "Security And Transport", 2015, 11000.5));
        employeeList.add(new Employee(211, "Jasna Kaur", 27, "Female", "Infrastructure", 2014, 15700.0));
        employeeList.add(new Employee(222, "Nitin Joshi", 25, "Male", "Product Development", 2016, 28200.0));
        employeeList.add(new Employee(233, "Jyothi Reddy", 27, "Female", "Account And Finance", 2013, 21300.0));
        employeeList.add(new Employee(244, "Nicolus Den", 24, "Male", "Sales And Marketing", 2017, 10700.5));
        employeeList.add(new Employee(255, "Ali Baig", 23, "Male", "Infrastructure", 2018, 12700.0));
        employeeList.add(new Employee(266, "Sanvi Pandey", 26, "Female", "Product Development", 2015, 28900.0));
        employeeList.add(new Employee(277, "Anuj Chettiar", 31, "Male", "Product Development", 2012, 35700.0));


        //1.No.of male and female employees  in the organization
        System.out.println("Male Employees Count: "+employeeList.stream()
                .filter(employee->employee.getGender().equals("Male"))
                .collect(Collectors.counting()));
        System.out.println("Female Employees Count: "+employeeList.stream()
                .filter(employee->employee.getGender().equals("Female"))
                .collect(Collectors.counting()));
        System.out.println();
        //2.the name of all departments in the organization
        System.out.println("Names of all departments in the organization: ");
        employeeList.stream()
                .map(employee->employee.getDepartment())
                .distinct()
                .forEach(System.out::println);
        System.out.println();
        //3.the average age of male and female employees
        System.out.println("Average age of male employees: "+employeeList.stream()
                .filter(employee->employee.getGender().equals("Male"))
                .collect(Collectors.averagingInt(employee->employee.getAge())));
        System.out.println("Average age of female employees: "+employeeList.stream()
                .filter(employee->employee.getGender().equals("Female"))
                .collect(Collectors.averagingInt(employee->employee.getAge())));
        System.out.println();
        //4.details of highest paid employee in the organization
        System.out.println("Highest paid employee: "+employeeList.stream()
                .max(Comparator.comparing(Employee::getSalary)));
        System.out.println();
        //5.the names of all employees who have joined after 2015
        System.out.println("the names of all employees who have joined after 2015: "+employeeList.stream()
                .filter(employee->employee.getYearOfJoining()>2015)
                .map(employee->employee.getName())
                .collect(Collectors.toList()));
        System.out.println();
        //6.number of employees in each department
        System.out.println("number of employees in each department: "+employeeList.stream()
                .collect(Collectors.groupingBy(employee->employee.getDepartment(),Collectors.counting())));
        System.out.println();
        //7.average salary of each department
        System.out.println("average salary of each department: "+employeeList.stream()
                .collect(Collectors.groupingBy(employee->employee.getDepartment(),Collectors.averagingDouble(employee->employee.getSalary()))));
        System.out.println();
        //8.details of youngest male employee in the product development department
        System.out.println(" details of youngest male employee in the product development department: "+employeeList.stream()
                .filter(employee->employee.getDepartment().equals("Product Development"))
                .min(Comparator.comparing(Employee::getAge)));
        System.out.println();
        //9.employee with most working experience in the organization
        System.out.println("Senior employee : "+employeeList.stream()
                .collect(Collectors.minBy(Comparator.comparing(Employee::getYearOfJoining))));
        //10.male and female employees  in the sales and marketing team
        System.out.println("no.of male employees in sales and marketing team: "+employeeList.stream()
                .filter(employee->employee.getDepartment().equals("Sales And Marketing")&&employee.getGender().equals("Male"))
                .collect(Collectors.counting()));
        System.out.println("no.of female employees in sales and marketing team: "+employeeList.stream()
                .filter(employee->employee.getDepartment().equals("Sales And Marketing")&&employee.getGender().equals("Female"))
                .collect(Collectors.counting()));
        System.out.println();
        //11.average salary of male and female employees
        System.out.println("Average salary of male employees: "+employeeList.stream()
                .filter(employee->employee.getGender().equals("Male"))
                .collect(Collectors.averagingDouble(employee->employee.getSalary())));
        System.out.println("Average salary of female employees: "+employeeList.stream()
                .filter(employee->employee.getGender().equals("Female"))
                .collect(Collectors.averagingDouble(employee->employee.getSalary())));
        System.out.println();
        //12.names of all employees in each department
        System.out.println("names of all employees in each department: "+employeeList.stream()
                .map((employee)->{ Employee e= new Employee(0, null, 0, null, null, 0, 0);
                    e.setName(employee.getName());
                    e.setDepartment(employee.getDepartment());
                    return e;
                })
                .collect(Collectors.groupingBy(employee->employee.getDepartment(),Collectors.toList())));
        System.out.println();
        //13.average salary and total salary of the whole organization
        System.out.println("Total salary: "+employeeList.stream()
                .collect(Collectors.summingDouble(employee->employee.getSalary())));
        System.out.println("Average salary: "+employeeList.stream()
                .collect(Collectors.averagingDouble(employee->employee.getSalary())));
        System.out.println();
        //14.employees who are younger or equal to 25 years from those employees who are older than 25 years
        System.out.println("employees who are younger or equal to 25 years:");
        employeeList.stream()
                .filter(employee->employee.getAge()<=25)
                .forEach(System.out::println);
        System.out.println("employees who are older than 25 years");
        employeeList.stream()
                .filter(employee->employee.getAge()>25)
                .forEach(System.out::println);
        System.out.println();
        //15.oldest employee in the organization
        System.out.println(employeeList.stream()
                .min(Comparator.comparing(Employee::getYearOfJoining))
              .map((employee)->{ Employee e1= new Employee(0, null, 0, null, null, 0, 0);
                  e1.setName(employee.getName());
                  e1.setAge(employee.getAge());
                  e1.setDepartment(employee.getDepartment());
                  return e1;
              }));



    }
}
