package utility;

import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.BeforeMethod;

import java.net.MalformedURLException;
import java.net.URL;

public class Utility {

    public static final String URL = "https://oauth-anoohyagudavalli2002-c7815:b07809d1-c54f-4a7b-9d38-e357b8c740bb@ondemand.eu-central-1.saucelabs.com:443/wd/hub";

    protected static ThreadLocal<RemoteWebDriver> driver = new ThreadLocal<>();

    @BeforeMethod
    public void setUp() throws MalformedURLException {
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setCapability("platformName","Windows");
        driver .set(new RemoteWebDriver(new URL(URL),chromeOptions));
    }
}
