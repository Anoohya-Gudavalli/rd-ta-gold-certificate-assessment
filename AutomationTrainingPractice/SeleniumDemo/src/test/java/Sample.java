import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.*;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import java.time.Duration;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

public class Sample {
    WebDriver driver;

    @Test
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Anoohya_Gudavalli\\Documents\\Drivers\\ChromeDriver\\chromedriver.exe");
        System.setProperty("webdriver.chrome.logfile", ".\\src\\test\\resources\\chromedriver.log");
        System.setProperty("webdriver.chrome.verboseLogging", "true");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        driver = new ChromeDriver(options);
    }

    @Test
    public void verifyDriverFunctionalities() throws InterruptedException {
        setUp();

        driver.get("https://www.selenium.dev/");
        System.out.println(driver.getCurrentUrl());
        System.out.println(driver.getTitle());
        driver.manage().window().minimize();

        WebElement divTagName = driver.findElement(By.tagName("div"));
        System.out.println(divTagName.getAttribute("class"));
        driver.findElement(By.linkText("Documentation")).click();
        WebElement search = driver.findElement(By.className("DocSearch-Button-Placeholder"));
        search.click();
        WebElement searchInput = driver.findElement(By.id("docsearch-input"));
        Point point = searchInput.getLocation();
        System.out.println(point.getX() + "------------" + point.getY());
        System.out.println(searchInput.getTagName());
        searchInput.sendKeys("Selenium");
        List<WebElement> searchInputs = driver.findElements(By.id("docsearch-input"));
        if (searchInputs.size() > 0) {
            searchInputs.get(0).sendKeys("selenium");
            searchInputs.get(0).submit();
        }
    }
//     private void browserNavigations() throws InterruptedException {
//             driver.navigate().to("https://www.selenium.dev/documentation");//navigate to test url
//        Thread.sleep(2000);
//        driver.navigate().back();
//        Thread.sleep(2000);
//        driver.navigate().forward();
//        Thread.sleep(2000);
//        driver.navigate().refresh();
//        }
        @Test
        public void testWaits () {
            driver.get("https://www.selenium.dev/");
            //Explicit
            WebDriverWait webDriverWait = new WebDriverWait(driver, Duration.ofMillis(20));
            webDriverWait.until(ExpectedConditions.elementToBeClickable(By.linkText("About")));

            //fluentWait
            Wait fluentWait = new FluentWait(driver)
                    .withTimeout(Duration.ofSeconds(6))
                    .pollingEvery(Duration.ofSeconds(6))
                    .ignoring(NoSuchElementException.class);
            fluentWait.until(new Function<WebDriver, WebElement>() {
                public WebElement apply(WebDriver driver) {
                    return driver.findElement(By.linkText("Abut"));
                }
            });

            //implicit wait
            driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
            WebElement webElement = driver.findElement(By.linkText("About"));
            webElement.click();

        }
        @Test
        public void testAlerts () {
            // driver.get("https://www.selenium.dev/");
            Alert alert = driver.switchTo().alert();
            System.out.println(alert.getText());
            alert.accept();
        }

//        @Test
//    public void javaScriptEx(){
//        setUp();
//
////        driver.get("https://www.selenium.dev/documentation/");
////        JavascriptExecutor javascript = (JavascriptExecutor) driver;
////        String pageTitle = (String) javascript.executeScript("return document.title");
////            System.out.println(pageTitle);
//        }

  /*  @AfterTest
    public void close(){
        driver.close();
    }*/
    @Test
    public void testtt(){
        setUp();
        driver.get("https://demoqa.com/books");
        WebElement webElement1 = driver.findElement(By.xpath("//*[@id=\"see-book-Git Pocket Guide\"]/a"));
        webElement1.getText();
    }
}