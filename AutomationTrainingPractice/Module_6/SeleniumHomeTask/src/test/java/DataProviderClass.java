import org.testng.annotations.DataProvider;

public class DataProviderClass {

    @DataProvider(name="dataProviderForDynamicContent")
    public Object[][] dataProviderForDynamicContent(){
        return new Object[][]{
                {"//*[@id=\"content\"]/div[1]/div[1]/img","//*[@id=\"content\"]/div[1]/div[2]",false},
                {"//*[@id=\"content\"]/div[2]/div[1]/img","//*[@id=\"content\"]/div[2]/div[2]",false}
        };
    }
    @DataProvider(name="dataProviderForNestedFrames")
    public Object[][] dataProviderForNestedFrames(){
        return new Object[][]{
                {"frame-left","//body[1]","LEFT"},
                {"frame-right","//body[1]","RIGHT"},
                {"frame-middle","//body[1]","MIDDLE"},
                {"frame-bottom","//body[1]","BOTTOM"}
        };
    }
    @DataProvider(name="dataProviderForAlerts")
    public Object[][]dataProviderForAlerts(){
        return new  Object[][]{
                {"//*[@id=\"content\"]/div/ul/li[1]/button","I am a JS Alert"},
                {"//*[@id=\"content\"]/div/ul/li[2]/button","I am a JS Confirm"},
                {"//*[@id=\"content\"]/div/ul/li[3]/button","I am a JS prompt"}
        };
    }
    @DataProvider(name="dataProviderForTables")
    public Object[][]dataProviderForTables(){
        return new Object[][]{
                {1,1,"Smith"},
                {1,2,"John"}
        };
    }
    @DataProvider(name="dataProviderForInputTests")
    public Object[][]dataProviderForInputTests(){
        return new Object[][]{
                {"1","1"},
                {"2","2"},
                {"34","34"}
        };
    }
}
