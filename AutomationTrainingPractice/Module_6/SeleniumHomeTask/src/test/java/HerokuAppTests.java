import com.beust.ah.A;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class HerokuAppTests {
    WebDriver driver;
    @BeforeTest
    public void setUp(){
        System.setProperty("webdriver.chrome.logfile",".\\src\\main\\resources\\chromedriver.log");
        System.setProperty("webdriver.chrome.verboseLogging","true");
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        driver = new ChromeDriver(options);
    }
    @Test
    public void  testCheckBoxes(){
        driver.get("http://the-internet.herokuapp.com/checkboxes");
        WebElement checkbox1 = driver.findElement(By.xpath("//div[2]//*[@id=\"content\"]//*[@id=\"checkboxes\"]//input[1]"));
        boolean initialStateOfCheckbox1 = checkbox1.isSelected();
        System.out.println("State of checkbox1 initially: "+initialStateOfCheckbox1);
        checkbox1.click();
        //webElement.click();
        boolean stateAfterClickOnCheckbox1 = checkbox1.isSelected();
        System.out.println(checkbox1.isDisplayed());
        System.out.println(checkbox1.isEnabled());
        System.out.println("State of checkbox1 after click: "+stateAfterClickOnCheckbox1);
        WebElement checkbox2 = driver.findElement(By.xpath("//*[@id=\"checkboxes\"]//input[2]"));
        boolean initialStateOfCheckbox2 = checkbox2.isSelected();
        System.out.println("State of checkbox2 initially: "+initialStateOfCheckbox2);

    }

    @Test(dataProviderClass = DataProviderClass.class,dataProvider = "dataProviderForDynamicContent")

    public void testDynamicContent(String xpathForImage ,String xpathForText,boolean expectedResult){//

        driver.get("http://the-internet.herokuapp.com/dynamic_content");
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        WebElement image1BeforeRefresh = driver.findElement(By.xpath(xpathForImage));
        driver.navigate().refresh();
        WebElement image1AfterRefresh = driver.findElement(By.xpath(xpathForImage));
        System.out.println(image1BeforeRefresh.equals(image1AfterRefresh));

        WebElement  text1BeforeRefresh= driver.findElement(By.xpath(xpathForText));
        driver.navigate().refresh();
        WebElement text1AfterRefresh = driver.findElement(By.xpath(xpathForText));
        Assert.assertEquals((text1BeforeRefresh.equals(text1AfterRefresh)),expectedResult);
    }
    @Test
    public void testMultipleWindows(){

        driver.get("http://the-internet.herokuapp.com/windows");
        driver.findElement(By.partialLinkText("Click")).click();
       // driver.findElement(By.linkText("Click Here")).click();
        //driver.findElement(By.xpath("//*[@id=\"content\"]/div/a")).click();
        driver.switchTo().window(driver.getWindowHandles().toArray(new String[driver.getWindowHandles().size()])[1]);
        System.out.println(driver.getCurrentUrl());
       driver.switchTo().window(driver.getWindowHandles().toArray(new String[driver.getWindowHandles().size()])[0]);
        System.out.println(driver.getCurrentUrl());
    }
    @Test(dataProviderClass = DataProviderClass.class,dataProvider = "dataProviderForNestedFrames")
    public void testNestedFrames(String frameName,String xpathForText,String expectedResult){

        driver.get("http://the-internet.herokuapp.com/nested_frames");
        driver.switchTo().frame("frame-top");
        driver.switchTo().frame(frameName);
       // driver.switchTo().defaultContent();
        //driver.switchTo().frame(frameName);
        Assert.assertEquals((driver.findElement(By.xpath(xpathForText)).getText()),expectedResult);

    }
    @Test(dataProviderClass = DataProviderClass.class,dataProvider  ="dataProviderForAlerts")
    public void testJavaScriptAlerts (String xpathForAlert,String expectedResult) {

        driver.get("http://the-internet.herokuapp.com/javascript_alerts");
        driver.findElement(By.xpath(xpathForAlert)).click();
        Alert alert = driver.switchTo().alert();
        Assert.assertEquals(alert.getText(),expectedResult);
        alert.accept();
        System.out.println(driver.findElement(By.id("result")).getText());
    }
    @Test
    public void testDropdown(){
        driver.get("http://the-internet.herokuapp.com/dropdown");
        driver.manage().timeouts().implicitlyWait(2,TimeUnit.SECONDS);
        Select select= new Select(driver.findElement(By.id("dropdown")));
        select.selectByVisibleText("Option 1");
        System.out.println(select.getFirstSelectedOption().getText());
        List<WebElement> webElements =select.getOptions();
        webElements.forEach(webElement -> webElements.get(2).click());
        System.out.println(select.isMultiple());
    }
    @Test(dataProviderClass = DataProviderClass.class,dataProvider = "dataProviderForTables")
    public void testTables(int r,int c,String expectedResult) {
        driver.get("http://the-internet.herokuapp.com/tables");
        //Number of rows
        List<WebElement> rows = driver.findElements(By.xpath("//*[@id=\"table1\"]/tbody/tr"));
        int rowCount = rows.size();
       Assert.assertEquals(rowCount,4);
       //Number of columns
        Assert.assertEquals(driver
                .findElements(By.xpath("//*[@id=\"table1\"]/thead/tr/th"))
                .size(),6);
       //cell Values
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"table1\"]/tbody/tr["+r+"]/td["+c+"]"))
                .getText(),expectedResult);
    }
    @Test(dataProviderClass = DataProviderClass.class,dataProvider = "dataProviderForInputTests")
    public void testInputs(String input,String expectedOutput){
        driver.get("http://the-internet.herokuapp.com/inputs");
    WebElement webElement =driver.findElement(By.xpath("html//body/div[2]//*[@class ='example']/input"));
    webElement.sendKeys(input);
    webElement.click();
        Assert.assertEquals(webElement.getAttribute("value"), expectedOutput);
        webElement.clear();
    }
    @Test
    public void testIFrame(){
        driver.get("http://the-internet.herokuapp.com/tinymce");
        driver.switchTo().frame("mce_0_ifr");
        WebElement webElement = driver.findElement(By.id("tinymce"));
        webElement.clear();
        webElement.sendKeys("Hello");
        Assert.assertEquals(webElement.getText(),"Hello");
    }
    @Test
    public void testActions(){
        driver.get("https://www.youidraw.com/apps/painter/");
        WebElement canvasElement = driver.findElement(By.id("catch"));
        WebElement toolToDraw = driver.findElement(By.id("pencil"));
        toolToDraw.click();
        Actions action = new Actions(driver);
        action
                .clickAndHold(canvasElement)
                .moveByOffset(10,50)
                .moveByOffset(-10,-50)
                .moveByOffset(50,10)
                .moveByOffset(-10,-50)
                .release()
                .perform();
//        FileUtils.copyFile(WebElementExtender.captureElementBitmap(canvasElement),new File(“automatedImageFile”));
//        assertEquals(CompareUtil.Result.Matched,CompareUtil.CompareImage(“baseImageFile”,”automatedImageFile”));
    }
}
