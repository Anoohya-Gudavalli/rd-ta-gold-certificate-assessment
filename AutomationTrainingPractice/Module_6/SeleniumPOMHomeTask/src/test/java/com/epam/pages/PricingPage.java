package com.epam.pages;

import com.epam.model.WebDriverClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class PricingPage extends WebDriverClass {

    @FindBy(xpath="//*[@id=\"tab-item-1\"]/div[1]/div/div[1]/div/div")
    private WebElement computeEngine;
    @FindBy(xpath = "//*[@aria-label='quantity']")
    private WebElement numberOfInstances;
    @FindBy(className="md-select-icon")
    private WebElement operatingSystem;

    @FindBy(xpath = "//*[@value='free']")
    private WebElement selectOperatingSystem;

    @FindBy(xpath = "/html/body/md-content/md-card/div/md-card-content[1]/div[2]/div/md-card/md-card-content/div/div[1]/form/div[4]/div[1]/md-input-container/md-select/md-select-value/span[2]")
    private WebElement VMClass;
    @FindBy(xpath = "//*[@value='regular']")
    private WebElement VMClassRegular ;
    @FindBy(xpath="/html/body/md-content/md-card/div/md-card-content[1]/div[2]/div/md-card/md-card-content/div/div[1]/form/div[6]/div[1]/md-input-container/md-select/md-select-value/span[2]")
    private WebElement seriesDropDown;
    @FindBy(xpath = "//*[@value='n1']")
    private WebElement seriesN1;
    @FindBy(xpath="/html/body/md-content/md-card/div/md-card-content[1]/div[2]/div/md-card/md-card-content/div/div[1]/form/div[7]/div[1]/md-input-container/md-select/md-select-value/span[2]")
    private WebElement instanceTypeDropDown ;
    @FindBy(xpath = "//*[@value='CP-COMPUTEENGINE-VMIMAGE-N1-STANDARD-8']")
    private WebElement instanceType;
    @FindBy(xpath = "//*[@aria-label='Add GPUs']")
    private WebElement checkboxGPU;
    @FindBy(xpath = "//*[@aria-label='GPU type']")
    private WebElement GPUTypeDropdown;
    @FindBy(xpath = "//*[@value='NVIDIA_TESLA_V100']")
    private WebElement GPUType;
    @FindBy(xpath = "/html/body/md-content/md-card/div/md-card-content[1]/div[2]/div/md-card/md-card-content/div/div[1]/form/div[14]/div/div[1]/div[1]/md-input-container[2]/md-select/md-select-value/span[2]")
    private WebElement numberOfGPUsDropdown;
   // @FindBy(xpath = "//*[@value='1']")
    @FindBy(id = "select_option_496")
    private WebElement numberOfGPUs;
    @FindBy(xpath="/html/body/md-content/md-card/div/md-card-content[1]/div[2]/div/md-card/md-card-content/div/div[1]/form/div[15]/div[1]/md-input-container/md-select/md-select-value/span[2]")
    private WebElement localSSDDropdown ;
    @FindBy(id = "select_option_471")
    private WebElement localSSd ;
    @FindBy(xpath = "//*[@placeholder='Datacenter location']")
    private WebElement locationDropdown;
    //@FindBy(xpath = "//*[@value='europe-west3']")
    @FindBy(xpath ="/html/body/div[11]/md-select-menu/md-content/md-optgroup/md-option[26]")
    private WebElement location;
    @FindBy(xpath ="//*[@placeholder='Committed usage']")
    private WebElement committedUsageDropdown;
    @FindBy(xpath = "/html/body/div[12]/md-select-menu/md-content/md-option[2]")
    private WebElement committedUsage;
    @FindBy(xpath = "//*[ @class='md-raised md-primary cpc-button md-button md-ink-ripple' and @type='button' ]")
    private WebElement estimateButton;



    public PricingPage(WebDriver driver) {
      super(driver);
        PageFactory.initElements(driver,this);
    }


    public PricingPage getComputeEngine() {
        WebElement webElement1 = driver.findElement(By.xpath("//*[@id=\"cloud-site\"]/devsite-iframe/iframe"));
        driver.switchTo().frame(webElement1);
        WebElement webElement = driver.findElement(By.id("myFrame"));
        driver.switchTo().frame(webElement);
       computeEngine.click();
        return new PricingPage(driver);
    }

    public PricingPage selectNumberOfInstancesInput() {
        WebDriverWait webDriverWait = new WebDriverWait(driver, Duration.ofMillis(50));
        webDriverWait.until(ExpectedConditions.elementToBeClickable(numberOfInstances));
    numberOfInstances.sendKeys("4");
        return new PricingPage(driver);
    }

    public PricingPage selectOperatingSystem() {
        operatingSystem.click();
        selectOperatingSystem.click();
        return new PricingPage(driver);
    }

    public PricingPage selectVMClass() {
        VMClass.click();
        WebDriverWait webDriverWait = new WebDriverWait(driver, Duration.ofSeconds(5));
        webDriverWait.until(ExpectedConditions.elementToBeClickable(VMClassRegular));
        VMClassRegular.click();
        return new PricingPage(driver);
    }

    public PricingPage selectSeries() {
       seriesDropDown.click();
        WebDriverWait webDriverWait = new WebDriverWait(driver, Duration.ofSeconds(1));
        webDriverWait.until(ExpectedConditions.elementToBeClickable(seriesN1));
        seriesN1.click();
        return new PricingPage(driver);
    }

    public PricingPage selectInstanceType() {
        instanceTypeDropDown.click();
        WebDriverWait webDriverWait = new WebDriverWait(driver, Duration.ofSeconds(5));
        webDriverWait.until(ExpectedConditions.elementToBeClickable(instanceType));
        instanceType.click();
        return new PricingPage(driver);
    }

    public PricingPage selectGPU() {
        WebDriverWait webDriverWait1 = new WebDriverWait(driver, Duration.ofMillis(100));
        webDriverWait1.until(ExpectedConditions.elementToBeClickable(checkboxGPU));
        checkboxGPU.click();
        WebDriverWait webDriverWait2 = new WebDriverWait(driver, Duration.ofMillis(100));
        webDriverWait2.until(ExpectedConditions.elementToBeClickable(GPUTypeDropdown));
        GPUTypeDropdown.click();
        WebDriverWait webDriverWait3 = new WebDriverWait(driver, Duration.ofSeconds(5));
        webDriverWait3.until(ExpectedConditions.elementToBeClickable(GPUType));
        GPUType.click();
        WebDriverWait webDriverWait5= new WebDriverWait(driver, Duration.ofSeconds(5));
        webDriverWait5.until(ExpectedConditions.elementToBeClickable(numberOfGPUsDropdown));
        numberOfGPUsDropdown.click();
        WebDriverWait webDriverWait4= new WebDriverWait(driver, Duration.ofSeconds(1));
        webDriverWait4.until(ExpectedConditions.elementToBeClickable(numberOfGPUs));
        numberOfGPUs.click();
        return new PricingPage(driver);
    }

    public PricingPage selectLocalSSD() {
        localSSDDropdown.click();
        WebDriverWait webDriverWait1 = new WebDriverWait(driver, Duration.ofSeconds(1));
        webDriverWait1.until(ExpectedConditions.elementToBeClickable(localSSd));
        localSSd.click();
        return new PricingPage(driver);
    }

    public PricingPage getLocation() {
       locationDropdown.click();
       WebDriverWait webDriverWait1 = new WebDriverWait(driver, Duration.ofSeconds(5));
        webDriverWait1.until(ExpectedConditions.elementToBeClickable(location));
       location.click();
        return new PricingPage(driver);
    }

    public PricingPage setCommittedUsage() {
        committedUsageDropdown.click();
        WebDriverWait webDriverWait1 = new WebDriverWait(driver, Duration.ofSeconds(1));
        webDriverWait1.until(ExpectedConditions.elementToBeClickable(committedUsage));
        committedUsage.click();
        return new PricingPage(driver);
    }
    public PricingPage estimatePrice(){
        WebDriverWait webDriverWait = new WebDriverWait(driver, Duration.ofSeconds(5));
        webDriverWait.until(ExpectedConditions.elementToBeClickable(estimateButton));
        estimateButton.click();
        return new PricingPage(driver);
    }


}