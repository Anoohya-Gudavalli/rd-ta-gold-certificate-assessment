package com.epam.model;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public abstract class WebDriverClass {
    protected  WebDriver driver;

    public WebDriverClass(WebDriver driver)
    {
        this.driver = driver;
    }
    public Boolean isElementPresent(By locator)
    {
        return driver.findElements(locator).size() > 0;
    }

}
