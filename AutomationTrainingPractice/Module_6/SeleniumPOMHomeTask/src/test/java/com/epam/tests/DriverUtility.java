package com.epam.tests;

import com.epam.driver.DriverSingleton;
import com.epam.utility.TestListener;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;

@Listeners({TestListener.class})
public class DriverUtility {
    protected WebDriver driver;
    @BeforeTest
    public void setUp(){
        try {
            driver= DriverSingleton.getDriver();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
    @AfterTest(alwaysRun = true)
    public void stopBrowser(){
        DriverSingleton.closeDriver();
    }

}
