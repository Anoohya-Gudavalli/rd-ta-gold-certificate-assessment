package com.epam.model;

import com.epam.pages.PricingPage;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;


public class HomePage extends WebDriverClass  {

    //webElements
    @FindBy(xpath="//*[@class='devsite-searchbox']/input")
    private WebElement search;
    @FindBy(xpath="//*[@class='devsite-search-container']//*[@type ='button']")
    private WebElement searchButton;
    @FindBy(xpath="//*[@id=\"___gcse_0\"]/div/div/div/div[5]/div[2]/div/div/div[1]/div[1]/div[1]/div[1]/div/a")
    private WebElement searchPricingCalculator;

    public HomePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver,this);
    }


    public HomePage search(){
         search.click();
         search.sendKeys("Google cloud pricing calculator");
        WebDriverWait webDriverWait = new WebDriverWait(driver, Duration.ofMillis(20));
        webDriverWait.until(ExpectedConditions.elementToBeClickable(search));
         Actions action = new Actions(driver);
         action
                 .keyDown(Keys.ENTER)
                 .perform();

     return new HomePage(driver);
     }


    public PricingPage getPricingCalculator(){
        WebDriverWait webDriverWait = new WebDriverWait(driver, Duration.ofSeconds(5));
        webDriverWait.until(ExpectedConditions.elementToBeClickable(searchPricingCalculator));
        searchPricingCalculator.click();
        return new PricingPage(driver);
    }

}
