package com.epam.tests;

import com.epam.driver.DriverSingleton;
import com.epam.model.HomePage;
import com.epam.pages.PricingPage;
import org.apache.logging.log4j.core.config.Order;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.epam.pages.PricingPageAfterEstimation;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.containsString;

public class Tests extends DriverUtility{
    private final String BASE_URL="https://cloud.google.com/";

    private HomePage homePage;
    private PricingPage pricingPage;
    private PricingPageAfterEstimation pricingPageAfterEstimation;



   @BeforeTest
    public void test() throws InterruptedException {

        driver.get(BASE_URL);
        homePage = new HomePage(driver);
        pricingPage = new PricingPage(driver);
        pricingPageAfterEstimation = new PricingPageAfterEstimation(driver);

        searchComputeEngineCalculator();
        estimatePriceForInput();

    }
    public void searchComputeEngineCalculator(){
        homePage.search();
        homePage.getPricingCalculator();
    }
    public void estimatePriceForInput(){
        pricingPage
                .getComputeEngine()
                .selectNumberOfInstancesInput()
                .selectOperatingSystem()
                .selectVMClass()
                .selectSeries()
                .selectInstanceType()
                .selectGPU()
                .selectLocalSSD()
                .getLocation()
                .setCommittedUsage()
                .estimatePrice();
    }
    @Test(dataProviderClass = DataProviderClass.class,dataProvider = "dataProviderForCommitmentYear")
    public void verifyCommitmentYear(String commitmentYear) {
             Assert.assertEquals(pricingPageAfterEstimation.getCommitmentYear(),commitmentYear);
    }
    @Test(dataProviderClass = DataProviderClass.class,dataProvider = "dataProviderForVMClass")
    public void verifyVMClass(String vmClass){
        Assert.assertEquals(pricingPageAfterEstimation.getVMClass(),vmClass);
    }
    @Test(dataProviderClass = DataProviderClass.class,dataProvider = "dataProviderForInstanceType")
    public void verifyInstanceType(String instanceType){
        assertThat(pricingPageAfterEstimation.getInstanceType(),containsString(instanceType));
    }
    @Test(dataProviderClass = DataProviderClass.class,dataProvider = "dataProviderForRegion")
    public void verifyRegion(String region){
        Assert.assertEquals(pricingPageAfterEstimation.getRegion(),region);
    }
    @Test(dataProviderClass = DataProviderClass.class,dataProvider = "dataProviderForLocalSSD")
    public void verifyLocalSSD(String localSSD){
        assertThat(pricingPageAfterEstimation.getLocalSSD(),containsString(localSSD));
    }

    @Test(dataProviderClass = DataProviderClass.class,dataProvider = "dataProviderForEstimatedPrice")
    public void verifyEstimatedPrice(String estimatedPrice){
        assertThat(pricingPageAfterEstimation.getResult(),containsString(estimatedPrice));
    }



}


