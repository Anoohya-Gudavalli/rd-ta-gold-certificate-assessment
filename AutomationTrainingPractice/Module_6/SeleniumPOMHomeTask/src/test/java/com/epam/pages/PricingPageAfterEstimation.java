package com.epam.pages;

import com.epam.model.WebDriverClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PricingPageAfterEstimation extends WebDriverClass {

    public PricingPageAfterEstimation(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver,this);
    }
    @FindBy(xpath = "//*[@id=\"compute\"]/md-list/md-list-item[3]/div[1]")
    private WebElement commitmentYear;
    @FindBy(xpath = "//*[@id=\"compute\"]/md-list/md-list-item[4]/div[1]")
    private WebElement VMClass;
    @FindBy(xpath = "//*[@id=\"compute\"]/md-list/md-list-item[5]/div[1]")
    private WebElement instanceType;
    @FindBy(xpath = "//*[@id=\"compute\"]/md-list/md-list-item[1]/div[1]")
    private WebElement region;
    @FindBy(xpath ="//*[@id=\"compute\"]/md-list/md-list-item[7]/div[1]")
    private WebElement localSSD;
    @FindBy(xpath = "//*[@id=\"resultBlock\"]/md-card/md-card-content/div/div/div/div[1]/h2/b")
    private WebElement resultAfterEstimation;

    public String getCommitmentYear(){
        return commitmentYear.getText();
    }
    public String getVMClass(){
        return VMClass.getText();
    }
    public String getInstanceType(){
        return instanceType.getText();
    }
    public String getRegion(){
        return region.getText();
    }
    public String getLocalSSD(){
        return localSSD.getText();
    }
    public String getResult(){
        return resultAfterEstimation.getText();
    }



}
