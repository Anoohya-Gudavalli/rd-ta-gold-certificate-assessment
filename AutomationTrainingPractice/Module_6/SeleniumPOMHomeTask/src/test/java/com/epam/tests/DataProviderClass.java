package com.epam.tests;

import org.testng.annotations.DataProvider;

public class DataProviderClass {
@DataProvider(name = "dataProviderForCommitmentYear")
    public Object[][] dataProviderForCommitmentYear(){
    return new Object[][]{
            {"Commitment term: 1 Year"}
    };

}
    @DataProvider(name = "dataProviderForVMClass")
    public Object[][] dataProviderForVMClass(){
        return new Object[][]{
                {"Provisioning model: Regular"}
        };

    }
    @DataProvider(name = "dataProviderForInstanceType")
    public Object[][] dataProviderForInstanceType(){
        return new Object[][]{
                {"Instance type: n1-standard-8"}
        };

    }
    @DataProvider(name = "dataProviderForRegion")
    public Object[][] dataProviderForRegion(){
        return new Object[][]{
                {"Region: Frankfurt"}
        };
    }
    @DataProvider(name = "dataProviderForLocalSSD")
    public Object[][] dataProviderForLocalSSD(){
        return new Object[][]{
                {"Local SSD: 2x375 GiB"}
        };
    }
    @DataProvider(name = "dataProviderForEstimatedPrice")
    public Object[][] dataProviderForEstimatedPrice(){
        return new Object[][]{
                {"1,081.2"}
        };
    }

}
