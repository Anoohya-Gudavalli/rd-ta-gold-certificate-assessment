package com.epam.exception;

public class BrowserNotReachable extends RuntimeException {

    public BrowserNotReachable(String errorMessage) {
        super(errorMessage);
    }
}
