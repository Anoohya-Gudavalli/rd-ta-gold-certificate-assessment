package com.epam.automation.pages;

import com.epam.automation.model.WebDriverClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Vitali_Shulha on 22-Oct-15.
 */
public class StartPage extends WebDriverClass {

    //@FindBy(xpath = "//a[text()='Sign in']"))]
    @FindBy(xpath = "//*[contains(text(),'Sign in')]")
    private WebElement butt0n;//button

//    public static final String GITHUB_BASE_URL = "http://www.github.com";
   // private WebDriver driver;
    public StartPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

//    public void open() {
//        driver.get(GITHUB_BASE_URL);
//    }

    public SignInPage invokeSignIn() {
        butt0n.click();
        return new SignInPage(driver);
    }
}
