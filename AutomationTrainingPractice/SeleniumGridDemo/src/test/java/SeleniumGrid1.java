import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;

public class SeleniumGrid1 {
    WebDriver driver;
//    public static final String URL ="https://oauth-anoohyagudavalli2002-c7815:b07809d1-c54f-4a7b-9d38-e357b8c740bb@ondemand.eu-central-1.saucelabs.com:443/wd/hub";
//    protected static ThreadLocal<RemoteWebDriver> driver = new ThreadLocal<>();
    @BeforeTest
        public void setUp() throws MalformedURLException{
        //        ChromeOptions chromeOptions = new ChromeOptions();
//        driver.set(new RemoteWebDriver(new URL(URL),chromeOptions));
//        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Anoohya_Gudavalli\\Documents\\Drivers\\ChromeDriver\\chromedriver.exe");
//        System.setProperty("webdriver.chrome.logfile", ".\\src\\test\\resources\\chromedriver.log");
//        System.setProperty("webdriver.chrome.verboseLogging", "true");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        driver = new ChromeDriver(options);
        driver.get("https://www.saucedemo.com/");
    }
    @Test
    public void verifyLogin(){
        WebElement uname = driver.findElement(By.id("user-name"));
        uname.sendKeys("Anoohya");
        uname.clear();
        WebElement pass = driver.findElement(By.id("password"));
        pass.sendKeys("pass");
        pass.clear();
        WebElement webElement = driver.findElement(By.id("login-button"));
        webElement.click();
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"login_button_container\"]/div/form/div[3]/h3")).getText(),"Epic sadface: Username is required" );
    }
    @Test(enabled = true)
    public void verifyLoginWithUsername(){
        WebElement uname = driver.findElement(By.id("user-name"));
        uname.clear();
        uname.sendKeys("standard_user");
        WebElement pass = driver.findElement(By.id("password"));
        pass.clear();
        WebElement webElement = driver.findElement(By.id("login-button"));
        webElement.click();
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"login_button_container\"]/div/form/div[3]/h3")).getText(),"Epic sadface: Password is required" );
    }

}
