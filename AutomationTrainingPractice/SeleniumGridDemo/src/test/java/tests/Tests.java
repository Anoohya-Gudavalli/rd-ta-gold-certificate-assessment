package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import screens.LoginPage;

public class Tests {
    WebDriver driver;
    LoginPage loginPage = new LoginPage(driver);
    @BeforeTest
    public void setUp(){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Anoohya_Gudavalli\\Documents\\Drivers\\ChromeDriver\\chromedriver.exe");
        System.setProperty("webdriver.chrome.logfile",".\\src\\main\\resources\\chromedriver.log");
        System.setProperty("webdriver.chrome.verboseLogging","true");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        driver = new ChromeDriver(options);
        driver.get("https://www.saucedemo.com/");

    }
    @Test
        public void verifyLogin(){
        loginPage.loginWithoutCredentials();

    }
}
