package screens;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage extends WebDiverClass {
    public LoginPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(this.driver,this);
    }
    @FindBy(id="user-name")
    private WebElement uname;

    @FindBy(id="pass")
    private WebElement pass;

    @FindBy(id="login-button")
    private WebElement loginButton;

    @FindBy(xpath = "//*[@id=\"login_button_container\"]/div/form/div[3]/h3")
    private WebElement errorMessage;



    public LoginPage loginWithoutCredentials(){
        uname.sendKeys("abc");
        uname.clear();
        pass.sendKeys("123");
        pass.clear();
        loginButton.click();
        String errorMsg = errorMessage.getText();
        return new LoginPage(driver);
    }

}
