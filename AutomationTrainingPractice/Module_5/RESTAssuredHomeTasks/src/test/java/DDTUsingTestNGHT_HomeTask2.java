import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class DDTUsingTestNGHT_HomeTask2 {
    public static final Logger LOGGER = LogManager.getLogger(DDTUsingTestNGHT_HomeTask2.class);
    @Test
    public void getWeatherData() {
        RestAssured.baseURI = "http://api.openweathermap.org/data/2.5/weather";
        RequestSpecification httpRequest = given().queryParam("q", "hyderabad")
                .queryParam("appid", "a2c598f643220250f137e276d81dfd33");
        Response response = httpRequest.request(Method.GET);
        response.prettyPrint();
    }

    @Test(dataProviderClass = DDTUsingTestNGHT_HomeTask2.class, dataProvider = "dataProviderForWeatherData")
    public void verifyWeatherDataCityNameAndCountryUsingCoordinates(String key, String value) {
        Response response = getResponse();
        JsonPath jsonPath = response.jsonPath();
        Assert.assertEquals(jsonPath.get(key).toString(), value);
        LOGGER.info("City name and country name are matched");
    }

    @Test(dataProviderClass = DDTUsingTestNGHT_HomeTask2.class, dataProvider = "dataProviderForWeatherDataTemperature")
    public void verifyWeatherDataMainTemperatureMinAndMaxUsingCoordinates(String key, String value) {
        Response response = getResponse();
        JsonPath jsonPath = response.jsonPath();
        assertThat(jsonPath.get(key).toString(), greaterThan(value));
        LOGGER.info("Temperature is greater than 0");
    }
    @DataProvider(name="dataProviderForWeatherDataTemperature")
    public Object[][] dataProviderForTemp(){
        return new Object[][]{
                {"main.temp_min","0"},
                {"main.temp","0"}
        };
    }

    public Response getResponse() {
        RestAssured.baseURI = "http://api.openweathermap.org/data/2.5/weather";
        RequestSpecification httpRequest = given()
                .queryParam("lat", 17.3753)
                .queryParam("lon", 78.4744)
                .queryParam("appid", "a2c598f643220250f137e276d81dfd33");
        Response response = httpRequest.request(Method.GET);
        return response;
    }

    @DataProvider(name="dataProviderForWeatherData")
    public Object[][] dataProvider(){
        return new Object[][]{
                {"name","Hyderabad"},
                {"sys.country","IN"}
        };

  }


}
