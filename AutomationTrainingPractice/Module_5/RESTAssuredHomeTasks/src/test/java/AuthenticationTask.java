import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class AuthenticationTask {
    @Test
    public void oauthTestGet(){

        given()
                .auth()
                .oauth2("ab9c97aac104fa632e6c03394ff7282c5d9f37a5231482bac5ef27beecec4cbe")
                .header("Content-type", "application/json")
                .get("https://gorest.co.in/public/v2/users")
                .then()
                .statusCode(200);
    }
    @Test
    public void oauthTestPost(){

        given()
                .auth()
                .oauth2("ab9c97aac104fa632e6c03394ff7282c5d9f37a5231482bac5ef27beecec4cbe")
                .header("Content-type", "application/json")
                .body("{\n" +
                        "                        \"id\": 1012012,\n" +
                        "                \"name\": \"Swara Iyengar III\",\n" +
                        "                \"email\": \"swara_iyengar_iii@wisoky-reinger.biz\",\n" +
                        "                \"gender\": \"female\",\n" +
                        "                \"status\": \"inactive\"\n" +
                        "    }")
                .post("https://gorest.co.in/public/v2/users")
                .then()
                .statusCode(422);//Unprocessable entity
    }

}
