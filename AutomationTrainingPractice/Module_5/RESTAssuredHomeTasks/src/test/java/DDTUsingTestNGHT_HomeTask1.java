
import groovy.json.JsonSlurper;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class DDTUsingTestNGHT_HomeTask1 {
    public static final Logger LOGGER = LogManager.getLogger(DDTUsingTestNGHT_HomeTask1.class);
    @Test(dataProviderClass = DDTUsingTestNGHT_HomeTask1.class,dataProvider = "dataProviderForTitleInEnglish")
public void getEventData(String expected) throws MalformedURLException {
        JsonSlurper slurper = new JsonSlurper();
        Object mapOfListOfEvents = slurper.parse(new URL("https://wearecommunity.io/api/v2/events"));
        Map eventsMap = (Map) mapOfListOfEvents;
        ArrayList<Map> eventsList = (ArrayList<Map>) eventsMap.get("events");
        List<String> listOfTitlesInEnglish = new ArrayList<>();
        for (int i = 0; i < eventsList.size(); i++) {
            if (eventsList.get(i).get("language") != null && eventsList.get(i).get("language").toString().equals("En")) {
                listOfTitlesInEnglish.add(eventsList.get(i).get("title").toString());
            }

        }
        String result = listOfTitlesInEnglish.toString();
        Assert.assertEquals(result,expected);
        LOGGER.info("Assertion for titles in English language is successful");


    }
        @DataProvider(name = "dataProviderForTitleInEnglish")
                public Object[][] dataProvider(){
            return new Object[][]{
                    {"[English Speaking Club (April), Negotiations Games (B1/B1+), German Hub events [April], Wroclaw board games, ECO Marathon​, Hackathon - Reimagined Futures with AI!, Business Domain streams, MoveWell LATAM 🫶🏽, EngX Coding DOJO : Insurance & BLIS, EngX Talk : Jenkins Basic to Advance, EARTH DAY SWITZERLAND - PLANT SWAP, [Offline] Gdańsk QA Community: EPAMize.IT #4, EPAMazing Offices Grand Opening event in Bogota, Dan Otvorenih Vrata | Vivify Academy (online), EngX LATAM community launch]"}
            };
        }
}
