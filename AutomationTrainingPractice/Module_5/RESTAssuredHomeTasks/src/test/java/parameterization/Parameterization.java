package parameterization;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.request;


public class Parameterization{
    private RequestSpecification userRequest;
   private PojoClassOfGoRestAPI pojoObject;
    private Response userResponse;
    @BeforeTest
    public void createRequest(){
        RestAssured.baseURI="https://gorest.co.in/public/v2";
        RestAssured.basePath="/users";
         userRequest = given();
    }
    @Test(dataProviderClass = DataProviderClass.class,dataProvider = "dataProviderForPost")
    public void createUser (int id,String name,String email,String gender,String status){
       pojoObject = new PojoClassOfGoRestAPI();
        pojoObject.setId(id);
        pojoObject.setName(name);
        pojoObject.setEmail(email);
        pojoObject.setGender(gender);
        pojoObject.setStatus(status);

        userResponse = (Response) userRequest
                .auth()
                .oauth2("ab9c97aac104fa632e6c03394ff7282c5d9f37a5231482bac5ef27beecec4cbe")
                .header("Content-type","application/json")
                .body(pojoObject)
                .post();


    }
    @Test
    public void verifyContentType(){
        Assert.assertEquals(userResponse.getContentType(),"application/json; charset=utf-8");
    }
    @Test
    public void verifyStatusCode(){
        Assert.assertEquals(userResponse.getStatusCode(),422);//201 new user
    }
    @Test(dependsOnMethods ="createUser" )
    public void getCreatedUser() {
        userResponse = userRequest.auth()
                .oauth2("ab9c97aac104fa632e6c03394ff7282c5d9f37a5231482bac5ef27beecec4cbe")
                .header("Content-type","application/json")
                .request(Method.GET,"/2007");
    }
    @Test(dataProviderClass = DataProviderClass.class,dataProvider = "dataProviderForPost")
    public void verifyName(int id,String name,String email,String gender,String status){
        Assert.assertEquals(pojoObject.getName(),name);
    }
    @Test(dataProviderClass = DataProviderClass.class,dataProvider = "dataProviderForPost")
    public void verifyEmail(int id,String name,String email,String gender,String status){
        Assert.assertEquals(pojoObject.getEmail(),email);
    }
    @Test(dataProviderClass = DataProviderClass.class,dataProvider = "dataProviderForPost")
    public void verifyGender(int id,String name,String email,String gender,String status){
        Assert.assertEquals(pojoObject.getGender(),gender);
    }
    @Test(dataProviderClass = DataProviderClass.class,dataProvider = "dataProviderForPost")
    public void verifyStatus(int id,String name,String email,String gender,String status){
        Assert.assertEquals(pojoObject.getStatus(),status);
    }
    @Test(dependsOnMethods = "createUser")
    public void updatePostName(){
        pojoObject.setName("lily kincaid");
        userResponse = (Response) userRequest
                .auth()
                .oauth2("ab9c97aac104fa632e6c03394ff7282c5d9f37a5231482bac5ef27beecec4cbe")
                .header("Content-type","application/json")
                .body(pojoObject)
                .put();
        Assert.assertEquals(userResponse.getStatusCode(),404);//201
        Assert.assertEquals(pojoObject.getName(),"lily kincaid");

    }
    @Test(dependsOnMethods = "createUser")
    public void deleteRequest(){
        Response response=userRequest.delete("/posts/"+pojoObject.getId());
        Assert.assertEquals(response.getStatusCode(),404);//200
    }

}
