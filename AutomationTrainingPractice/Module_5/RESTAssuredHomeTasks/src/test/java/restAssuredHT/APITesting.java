package restAssuredHT;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;


public class APITesting {
    private RequestSpecification httpRequest;
    @BeforeTest
    public void request(){
        RestAssured.baseURI="https://jsonplaceholder.typicode.com";
       httpRequest = given();
    }


    //1.
    @Test(priority = 1)
    public void verifyCountOfPostsResources(){
      Response httpResponse = httpRequest.request(Method.GET,"/posts");///comments/albums/photos/todos/users
      httpResponse
              .then()
              .statusCode(200)
              .assertThat()
              .body("size()",equalTo(100));
    }

    @Test(priority = 2)
    public void validatePostsResponseAndStatusCode(){
        Response postsResponse = httpRequest.request(Method.GET, "/posts?id=31");
        JsonPath postsJson = postsResponse.jsonPath();
        postsResponse
                .then()
                .assertThat()
                .body("title",equalTo(postsJson.get("title")))
                .statusCode(200);

        Assert.assertEquals(postsJson.get("title").toString(),"[ullam ut quidem id aut vel consequuntur]");

    }
    @Test(priority = 3)
    public void deleteRequest(){
        int id = 31;

        Response response = httpRequest.delete("/posts/"+id);
        Assert.assertEquals(response.getStatusCode(),200);
        System.out.println(response.getStatusCode());
    }
    @Test(priority = 4)
public void verifyModifiedResponse(){
        given()
                .header("Content-type","application/json")
                .body("{"+"\"title\":\"Dummy title\"}")
                .when()
                .put("/posts/31")
                .then()
                .assertThat()
                .statusCode(200);
    }

//    @Test(priority = 5)
//    public void verifyNewPostsCreationJson(){
//        RestAssured.basePath = "posts/";
//        RequestSpecification postsRequest = given();
//        JSONObject jsonObject = new JSONObject();
//        jsonObject.put("userId",4);
//        jsonObject.put("id",4);
//        jsonObject.put("title","DemoTitle");
//        jsonObject.put("body","DemoBody");
//
//        Response postsResponse =postsRequest
//                .header("Content-type","application/json")
//                .body(jsonObject.toString())
//                .request(Method.POST);
//        postsResponse.prettyPrint();
//        Assert.assertEquals(postsResponse.getStatusCode(),201);//post successful
//    }
@Test(priority = 5)
public void verifyNewPostsCreationJson(){
    RestAssured.basePath = "posts/";
    RequestSpecification postsRequest = given();
    PojoClassForApiTesting pojoObject = new PojoClassForApiTesting();

    pojoObject.setUserId(6);
    pojoObject.setId(4);
    pojoObject.setTitle("Title set using pojo");
    pojoObject.setBody("DemoBody set using pojo");

    Response postsResponse = postsRequest
            .header("Content-type", "application/json")
            .body(pojoObject)
            .request(Method.POST);
    Assert.assertEquals(pojoObject.getUserId(),6);
    Assert.assertEquals(pojoObject.getId(),4);
    Assert.assertEquals(pojoObject.getTitle(),"Title set using pojo");
    Assert.assertEquals(pojoObject.getBody(),"DemoBody set using pojo");
    Assert.assertEquals(postsResponse.getStatusCode(),201);//post successful
}




}
