import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.stream.Stream;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class GET_HT {
   private  RequestSpecification userRequest;
   private Response userResponse;

    @BeforeTest
    private void  request() {
        RestAssured.baseURI = "https://jsonplaceholder.typicode.com";
        userRequest = given();
        userResponse = userRequest.request(Method.GET, "/users");
    }


    @Test
    public void verifyStatusCode(){
        Assert.assertEquals(userResponse.getStatusCode(),200);
    }
    @Test
    public void verifyNumberOfUsers(){
        userResponse.then().assertThat().body("size()",greaterThanOrEqualTo(3));
    }
    @Test
    public void verifyName(){
        JsonPath jsonPath = userResponse.jsonPath();
       jsonPath.get("name").toString().contains("Ervin Howell");
    }

}
