package com.epam.runners;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(plugin = {"com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:", "pretty", "html:report.html", "com.epam.attributes.CustomAttributeReporter"})
public class BaseRunner extends AbstractTestNGCucumberTests {
}
