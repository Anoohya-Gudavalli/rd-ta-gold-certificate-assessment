package com.epam.logger.loggersImplementation;


import com.epam.logger.CustomLogger;
import com.epam.logger.LogLevel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Slf4JLogger implements CustomLogger {

    private Logger logger = LoggerFactory.getLogger("SLF4JLogger");


    @Override
    public void log(LogLevel level, String message) {
        switch (level) {
            case DEBUG:
                logger.debug(message);
                break;
            case ERROR:
                logger.error(message);
                break;
            case INFO:
                logger.info(message);
                break;
            case WARNING:
                logger.warn(message);
                break;
        }
    }

}
