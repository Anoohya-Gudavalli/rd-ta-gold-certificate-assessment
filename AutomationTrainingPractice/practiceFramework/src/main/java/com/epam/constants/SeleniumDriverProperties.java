package com.epam.constants;

public class SeleniumDriverProperties {

    public static final String CHROME_DRIVER_LOG="webdriver.chrome.logfile";
    public static final String CHROME_DRIVER_LOG_PATH=".\\src\\test\\resources\\driver.log";
    public static final String VERBOSE_LOGGING="webdriver.chrome.verboseLogging";
    public static final String VERBOSE_LOGGING_VALUE="true";
    public static final String CHROME_OPTIONS="--remote-allow-origins=*";
    public static final String BROWSER_PATH= "src/test/resources/browser.properties";


}
