package com.epam.utility;
import com.epam.driver.browserFactory.DriverSingletonFactory;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import java.io.File;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class TestListener implements ITestListener {
    private Logger log = LogManager.getRootLogger();

    @Override
    public void onTestSuccess(ITestResult iTestResult) {
        try {
            saveScreenshot();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
@Override
    public void onTestFailure(ITestResult iTestResult) {
        try {
            saveScreenshot();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
@Override
    public void onStart(ITestContext iTestContext) {
        log.info("Test started");
    }
@Override
    public void onFinish(ITestContext iTestContext) {
    log.info("Test finished");
    }

    private void saveScreenshot() throws Exception {
        File screenCapture = ((TakesScreenshot) DriverSingletonFactory
                .getDriver())
                .getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(screenCapture, new File(
                    ".//target/screenshots/"
                            + getCurrentTimeAsString() +
                            ".png"));
        } catch (IOException e) {
            log.error("Failed to save screenshot: " + e.getLocalizedMessage());
        }
    }

    private String getCurrentTimeAsString(){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern( "uuuu-MM-dd_HH-mm-ss" );
        return ZonedDateTime.now().format(formatter);
    }
}