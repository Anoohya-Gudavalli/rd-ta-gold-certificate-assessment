mvn archetype:generate -DgroupId=com.epam -DartifactId=maven-demo -DarchetypeArtifactId=maven-archetype-quickstart -DarchetypeVersion=1.0 -DinteractiveMode=false
-->creates src folder with main and test folders containing code file and test file
-->created pom.xml file containing properties, dependencies and other details of project


 1.mvn package- created target file and run test
packaged compiled code in jar format
Building jar: C:\GitDemo\RD_training\maven-demo\target\maven-demo-1.0-SNAPSHOT.jar 
Building maven-demo 1.0-SNAPSHOT

Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.01 sec

Results :

Tests run: 1, Failures: 0, Errors: 0, Skipped: 0


2. mvn clean - deleted target file and cleaned project


3.mvn compile - compiled the project and target file created
Compiling 1 source file to C:\GitDemo\RD_training\maven-demo\target\classes


4. mvn install - folder deployed in local repository
 Installing C:\GitDemo\RD_training\maven-demo\target\maven-demo-1.0-SNAPSHOT.jar to C:\Users\Anoohya_Gudavalli\.m2\repository\com\epam\maven-demo\1.0-SNAPSHOT\maven-demo-1.0-SNAPSHOT.jar


5.mvn test - test-classes folder generated in target
Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.01 sec

Results :

Tests run: 1, Failures: 0, Errors: 0, Skipped: 0


6.mvn test-compile - compiled test source code
 Compiling 1 source file to C:\GitDemo\RD_training\maven-demo\target\test-classes


7.mvn deploy - packaged JAR/ WAR file to the remote repository after compiling, running tests and building the project.


8.mvn verify - compiled and generated target file and snapshot


9. mvn validate - validated the dependencies