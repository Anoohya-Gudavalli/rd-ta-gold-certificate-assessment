package com.epam.strategy;

public class TestPictureSharingStrategy {
    public static void main(String[] args) {
        PhoneCameraAppShareService sharingPicturesService = new PhoneCameraAppShareService();
        sharingPicturesService.setStrategy(new TextSharingClient());
        sharingPicturesService.sendPicturesThroughChosenWay(new User(""));
    }
}
