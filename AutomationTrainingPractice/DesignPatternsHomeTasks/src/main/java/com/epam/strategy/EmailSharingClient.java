package com.epam.strategy;

public class EmailSharingClient implements PictureSharingStrategy {

    @Override
    public void sendPicture(User user) {
        System.out.println("Picture sent through email");
    }
}
