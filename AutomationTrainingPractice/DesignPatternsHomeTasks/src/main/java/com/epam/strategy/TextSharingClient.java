package com.epam.strategy;

public class TextSharingClient implements PictureSharingStrategy {
    @Override
    public void sendPicture(User user) {
        System.out.println("Picture shared through text");
    }
}
