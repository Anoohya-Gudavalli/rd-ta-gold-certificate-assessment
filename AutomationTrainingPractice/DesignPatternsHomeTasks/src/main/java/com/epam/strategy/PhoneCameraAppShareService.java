package com.epam.strategy;

public class PhoneCameraAppShareService {
        private PictureSharingStrategy strategy;

        public void setStrategy(PictureSharingStrategy strategy) {
            this.strategy = strategy;
        }

        public void sendPicturesThroughChosenWay(User user){
            strategy.sendPicture(user);
        }
    }
