package com.epam.builder;

import java.util.Objects;

public class Bicycle {
        // Required Parameters

    private Boolean fixGears;
    private Boolean fixDoubleSeats;
    private Boolean fixDoubleStands;
    private Boolean fixCarrier;



    public static BicycleBuilder builder() {
            return new BicycleBuilder();
        }

        public static class BicycleBuilder {
            // Required Parameters
            private Boolean fixGears;
            private Boolean fixDoubleSeats;
            private Boolean fixDoubleStands;
            private Boolean fixCarrier;



            public BicycleBuilder fixGears(final Boolean fixGears) {
                this.fixGears = fixGears;
                return this;
            }
            public BicycleBuilder fixCarrier(final Boolean fixCarrier){
                this.fixCarrier = fixCarrier;
                return this;
            }
            public BicycleBuilder fixDoubleSeats(final Boolean fixDoubleSeats){
                this.fixDoubleSeats = fixDoubleSeats;
                return this;
            }

            public BicycleBuilder fixDoubleStands(final Boolean fixDoubleStands){
                this.fixDoubleStands= fixDoubleStands;
                return this;
            }


            public Bicycle build() {
                if (!Objects.nonNull(this.fixGears) || !Objects.nonNull(this.fixDoubleSeats)
                        || !Objects.nonNull(this.fixDoubleStands) || !Objects.nonNull(this.fixCarrier)) {
                    throw new RuntimeException("Required parameters are missing");
                }
                Bicycle bicycle = new Bicycle();
                bicycle.fixGears= fixGears;
                bicycle.fixDoubleSeats= fixDoubleSeats;
                bicycle.fixCarrier= fixCarrier;
                bicycle.fixDoubleStands= fixDoubleStands;
                return bicycle;
            }
        }
    }

