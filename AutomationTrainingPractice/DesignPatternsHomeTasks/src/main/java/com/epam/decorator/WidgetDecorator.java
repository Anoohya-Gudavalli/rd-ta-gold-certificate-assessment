package com.epam.decorator;

public class WidgetDecorator implements Widget{
    private Widget widget;
    public WidgetDecorator(Widget widget){
       this.widget= widget;
    }
    @Override
    public int getRankSum() {
        return widget.getRankSum();
    }
}
