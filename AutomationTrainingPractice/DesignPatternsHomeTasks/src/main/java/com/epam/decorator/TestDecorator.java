package com.epam.decorator;

public class TestDecorator {
    public static void main(String[] args) {
        Widget widget = new DesktopWidget();
        Widget afterAddingWidget = new MobileWidget(widget);
        Widget afterAddingNewWidget = new NewWidget(afterAddingWidget);

        System.out.println(afterAddingNewWidget.getRankSum());
    }
}
