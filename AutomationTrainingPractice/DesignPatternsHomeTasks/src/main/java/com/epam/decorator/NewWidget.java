package com.epam.decorator;

public class NewWidget extends WidgetDecorator{
    int rank=10;
    public NewWidget(Widget widget) {
        super(widget);
    }
    @Override
    public int getRankSum() {
        return rank+super.getRankSum();
    }
}
