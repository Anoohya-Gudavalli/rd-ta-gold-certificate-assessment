package com.epam.decorator;

public class MobileWidget extends WidgetDecorator{
    int rank=5;
    public MobileWidget(Widget widget) {
        super(widget);
    }
    @Override
    public int getRankSum() {
        return rank+super.getRankSum();
    }
}
