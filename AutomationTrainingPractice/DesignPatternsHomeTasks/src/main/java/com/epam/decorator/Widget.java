package com.epam.decorator;

public interface Widget {
    int getRankSum();
}
