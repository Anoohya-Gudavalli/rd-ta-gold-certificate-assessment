package com.epam;

import org.testng.ITestListener;
import org.testng.ITestResult;

public class CalculatorListener implements ITestListener {
    @Override
    public void onTestSuccess(ITestResult result) {
        System.out.println("Status from listener "+result.getStatus());
    }
}

