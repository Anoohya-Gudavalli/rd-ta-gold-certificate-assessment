import org.testng.ISuite;
import org.testng.TestNG;
import org.testng.xml.XmlSuite;

import java.util.ArrayList;
import java.util.List;

public class CustomRunner {
    public static void main(String[] args) {
        TestNG tng = new TestNG();
        tng.setTestClasses(new Class[] {CalculatorTests.class});
        tng.run();
//        XmlSuite suite = new XmlSuite();
//        suite.setName("CalculatorTask");
//        suite.setParallel(XmlSuite.ParallelMode.METHODS);
//        suite.setThreadCount(2);
//        List<String>files = new ArrayList<>();
//        files.addAll(new ArrayList<>(){{
//            add("./src/main/resources/testNG.xml");
//        }});
//        suite.setSuiteFiles(files);
//
//        List<XmlSuite> suites = new ArrayList<>();
//        suites.add(suite);
//        tng.setXmlSuites(suites);
//        tng.run();
//
  }
}
