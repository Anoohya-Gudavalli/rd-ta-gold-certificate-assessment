import org.testng.annotations.DataProvider;

public class DataProviderClass {
    @DataProvider(name="dataProviderForAddForPositiveTestCases")
    public Object[][] dataProviderAddForPositiveTestCases() {
        return new Object[][]{
                {3, 2, 5},
                {1, 2, 3},
                {4, 5, 9},
                {6, 6, 12},
                {3, 4, 7},
        };

    }
    @DataProvider(name="dataProviderForAddNegativeTestCases")
    public Object[][] dataProviderAddForNegativeTestCases() {
        return new Object[][]{
                {3, 2, 1},
                {1, 2, 1},
                {4, 5, 1},
                {6, 6, 1},
                {3, 4, 1}};

    }
    @DataProvider(name="dataProviderForSubPositiveTestCases")
    public Object[][] dataProviderSubForPositiveTestCases() {
        return new Object[][]{
                {3, 2, 1},
                {1, 2, -1},
                {4, 5, -1},
                {6, 6, 0},
                {3, 4, -1},
        };

    }
    @DataProvider(name="dataProviderForSubNegativeTestCases")
    public Object[][] dataProviderSubForNegativeTestCases() {
        return new Object[][]{
                {3, 2, 3},
                {1, 2, 1},
                {4, 5, 1},
                {6, 6, 1},
                {3, 4, 1}};

    }
    @DataProvider(name="dataProviderForMultiplyForPositiveTestCases")
    public Object[][] dataProviderMultiplyForPositiveTestCases() {
        return new Object[][]{
                {3, 2, 6},
                {1, 2, 2},
                {4, 5, 20},
                {6, 6, 36},
                {3, 4, 12},
        };

    } @DataProvider(name="dataProviderForMultiplyForNegativeTestCases")
    public Object[][] dataProviderMultiplyForNegativeTestCases() {
        return new Object[][]{

                {3, 2, 1},
                {1, 2, 1},
                {4, 5, 1},
                {6, 6, 1},
                {3, 4, 1}};

    }

    @DataProvider(name="dataProviderForDividePositiveTestCases")
    public Object[][] dataProviderDivideForPositiveTestCases() {
        return new Object[][]{
                {6, 2, 3},
                {2, 2, 1},
                {40, 10, 4},
                {60, 6, 10},
                {44, 4, 11},
                };

    }
    @DataProvider(name="dataProviderForDivideNegativeTestCases")
    public Object[][] dataProviderDivideForNegativeTestCases() {
        return new Object[][]{
                {3, 2, 1},
                {1, 2, 1},
                {4, 5, 1},
                {6, 6, 5},
                {3, 4, 1}};

    }
    @DataProvider(name="dataProviderForPowerPositiveTestCases")
    public Object[][] dataProviderPowerForPositiveTestCases() {
        return new Object[][]{
                {6, 2, 36},
                {2, 2, 4},
                {40, 0, 1},
                {2, 6, 64},
                {4, 2, 16},
        };
    }
    @DataProvider(name="dataProviderForPowerNegativeTestCases")
    public Object[][] dataProviderPowerForNegativeTestCases() {
        return new Object[][]{
                {3, 2, 1},
                {1, 2, 16},
                {4, 5, 1},
                {6, 6, 5},
                {3, 4, 1}};
    }
    @DataProvider(name="dataProviderForSqrtForPositiveTestCases")
    public Object[][] dataProviderSqrtForPositiveTestCases() {
        return new Object[][]{
                {36, 6},
                {16, 4},
                {1, 1},
                {64, 8},
                {256, 16},
        };
    }
    @DataProvider(name="dataProviderForSqrtNegativeTestCases")
    public Object[][] dataProviderSqrtForNegativeTestCases() {
        return new Object[][]{
                {3, 1},
                {1, 16},
                {4, 1},
                {6, 5},
                {3,91}};
    }
    @DataProvider(name="dataProviderForTangentPositiveTestCases")
    public Object[][] dataProviderTangentPositiveTestCases() {
        return new Object[][]{
                {60,1.73},
                {0, 0},
                {15, 0.27},
                {45,1},
                {75,3.73},
               };
    }
    @DataProvider(name="dataProviderForTangentNegativeTestCases")
    public Object[][] dataProviderTangentNegativeTestCases() {
        return new Object[][]{
                {35, 1},
                {15, 16},
                {42, 1},
                {65, 5},
                {30,91}};
    }

    @DataProvider(name="dataProviderForCosineForPositiveTestCases")
    public Object[][] dataProviderCosineForPositiveTestCases() {
        return new Object[][]{
                {60,0.5},
                {0, 1},
                {15, 0.97},
                {45,0.71},
                {30,0.87},
        };
    }
    @DataProvider(name="dataProviderForCosineNegativeTestCases")
    public Object[][] dataProviderCosineNegativeTestCases() {
        return new Object[][]{

                {35, 2},
                {15, 16},
                {42, 8},
                {65, 5},
                {30,91}};
    }
    @DataProvider(name="dataProviderForSinPositiveTestCases")
    public Object[][] dataProviderSinPositiveTestCases() {
        return new Object[][]{
                {60,0.87},
                {0, 0},
                {15, 0.26},
                {45,0.71},
                {30,0.5},
        };
    }
    @DataProvider(name="dataProviderForSinNegativeTestCases")
    public Object[][] dataProviderSinNegativeTestCases() {
        return new Object[][]{
                {35, 2},
                {15, 16},
                {42, 8},
                {65, 5},
                {30,91}};
    }
}
