import org.testng.annotations.*;

public class ConfigurationTests {
    @BeforeSuite
    public void before_suite()
    {
        System.out.println("before suite");
    }
    @AfterSuite
    public void after_suit()
    {
        System.out.println("after suite");
    }
    @AfterClass
    public void after_class()
    {
        System.out.println("after class");
    }
    @BeforeClass
    public void before_class()
    {
        System.out.println("before class");
    }
    @BeforeMethod
    public void before_method(){
        System.out.println( "before method");
    }
    @AfterMethod
    public void after_method(){
        System.out.println( "after method");
    }
}