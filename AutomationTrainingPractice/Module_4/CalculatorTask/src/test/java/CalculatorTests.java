import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import java.lang.Math;

public class CalculatorTests extends ConfigurationTests
    {
        @Test(groups= "Regression",dataProviderClass = DataProviderClass.class,dataProvider = "dataProviderForAddForPositiveTestCases",priority = 2,dependsOnGroups = "Smoke",alwaysRun = true)
        public void testAddNumbersPositiveTestCases(double num1,double num2,double expectedResult)
        {
            double result = num1+num2;
            Assert.assertEquals(result, expectedResult);

        }
        @Test(groups= "Regression",dataProviderClass = DataProviderClass.class,dataProvider = "dataProviderForAddNegativeTestCases",priority = 2,dependsOnGroups = "Smoke",alwaysRun = true)
        public void testAddNumbersNegativeTestCases(double num1,double num2,double expectedResult)
        {try{
            double result = num1+num2;
            Assert.assertEquals(result, expectedResult); }
        catch(AssertionError e){
            e.printStackTrace();
        }
        }

        @Test(groups ="Regression",dataProviderClass = DataProviderClass.class,dataProvider = "dataProviderForSubPositiveTestCases",priority = 3,dependsOnGroups = "Smoke",alwaysRun = true)
        public void testSubNumbersPositiveTestCases(double num1,double num2,double expectedResult)
        {
            double result = num1-num2;
            Assert.assertEquals(result, expectedResult);

        }
        @Test(groups ="Regression",dataProviderClass = DataProviderClass.class,dataProvider = "dataProviderForSubNegativeTestCases",priority = 3,dependsOnGroups = "Smoke",alwaysRun = true)
        public void testSubNumbersNegativeTestCases(double num1,double num2,double expectedResult)
        {try{
            double result = num1-num2;
            Assert.assertEquals(result, expectedResult);
        }catch(AssertionError e){
            e.printStackTrace();
        }
        }
        @Test(groups = "Smoke",dataProviderClass = DataProviderClass.class,dataProvider = "dataProviderForMultiplyForPositiveTestCases",priority = 1)
        public void testMultiplyNumbersPositiveTestCases(double num1,double num2,double expectedResult)
        {
            double result = num1 * num2;
            Assert.assertEquals(result, expectedResult);

        }
            @Test(groups = "Smoke",dataProviderClass = DataProviderClass.class,dataProvider = "dataProviderForMultiplyForNegativeTestCases",priority = 1)
            public void testMultiplyNumbersNegativeTestCases(double num1,double num2,double expectedResult)
            {try {
                double result = num1 * num2;
                Assert.assertEquals(result, expectedResult);
            }catch(AssertionError e){
                e.printStackTrace();
            }
        }
        @Test(groups = "Smoke",dataProviderClass = DataProviderClass.class,dataProvider = "dataProviderForDividePositiveTestCases",priority = 0)
        public void testDivideNumbersPositiveTestCases(double num1,double num2,double expectedResult) {

                double result = num1 / num2;
                Assert.assertEquals(result, expectedResult);
        }
        @Test(groups = "Smoke",dataProviderClass = DataProviderClass.class,dataProvider = "dataProviderForDivideNegativeTestCases",priority = 0)
        public void testDivideNumbersNegativeTestCases(double num1,double num2,double expectedResult) {
            try {

                double result = num1 / num2;
                Assert.assertEquals(result, expectedResult);
            }catch(AssertionError e){
                e.printStackTrace();
            }

        }
        @Test(dataProviderClass = DataProviderClass.class,dataProvider = "dataProviderForPowerPositiveTestCases")
        public void testPowPositiveTestCases(double a,double b,double expectedResult){
            double result = Math.pow(a, Math.floor(b));
            Assert.assertEquals(result, expectedResult);

        }
        @Test(dataProviderClass = DataProviderClass.class,dataProvider = "dataProviderForPowerNegativeTestCases")
        public void testPowNegativeTestCases(double a,double b,double expectedResult){
            try {double result = Math.pow(a, Math.floor(b));
                Assert.assertEquals(result, expectedResult);
            }catch(AssertionError e){
                e.printStackTrace();
            }
        }
        @Test(dataProviderClass = DataProviderClass.class,dataProvider = "dataProviderForSqrtForPositiveTestCases")
        public void testSqrtPositiveTestCases(double a,double expectedResult){
            try{
            double result = Math.sqrt(Math.abs(a));
            Assert.assertEquals(result, expectedResult);
            }catch(AssertionError e){
                e.printStackTrace();
            }
        }
        @Test(dataProviderClass = DataProviderClass.class,dataProvider = "dataProviderForSqrtNegativeTestCases")
        public void testSqrtNegativeTestCases(double a,double expectedResult){
            try{
                double result = Math.sqrt(Math.abs(a));
                Assert.assertEquals(result, expectedResult);
            }catch(AssertionError e){
                e.printStackTrace();
            }
        }
        @Test(dataProviderClass = DataProviderClass.class,dataProvider = "dataProviderForTangentPositiveTestCases")
        public void testTangentPositiveCases(double a,double expectedResult){
            double b = Math.toRadians(a);
            double totalResult = Math.sin(b) / Math.cos(b);
            double result=Math.round(totalResult*100.0)/100.0;//bug -->this.sin(a)/this.cos(a), a is of type double in calculator.java
            Assert.assertEquals(result, expectedResult);
        }
        @Test(dataProviderClass = DataProviderClass.class,dataProvider = "dataProviderForTangentNegativeTestCases")
        public void testTangentNegativeTestCases(double a,double expectedResult){
            try{
                double b = Math.toRadians(a);
                double totalResult = Math.sin(b) / Math.cos(b);
                double result=Math.round(totalResult*100.0)/100.0;//bug -->this.sin(a)/this.cos(a), a is of type double in calculator.java
                Assert.assertEquals(result, expectedResult);
            }catch(AssertionError e){
                e.printStackTrace();
            }
        }
      /*  @Test(dataProviderClass = DataProviderClass.class,dataProvider = "dataProviderForTanh")
        public void testTanh(double a,double expectedResult){
        try{

            double b = Math.toRadians(a);
            double totalResult =Math.tanh(b);
            double result=Math.round(totalResult*100.0)/100.0;//bug --> a is of type double in calculator.java
            Assert.assertEquals(result, expectedResult);
    }catch(AssertionError e){
        e.printStackTrace();
    }
       }*/
        @Test(dataProviderClass = DataProviderClass.class,dataProvider = "dataProviderForCosineForPositiveTestCases")
        public void testCosPositiveTestCases(double a,double expectedResult) {

            double b = Math.toRadians(a);
            double totalResult =  Math.cos(b);
            double result = Math.round(totalResult * 100.0) / 100.0;//bug -->return Math.sin(a), a is of type double in calculator.java
            Assert.assertEquals(result, expectedResult);

            }
            @Test(dataProviderClass = DataProviderClass.class,dataProvider = "dataProviderForCosineNegativeTestCases")
            public void testCosNegativeTestCases(double a,double expectedResult) {
                try{
                    double b = Math.toRadians(a);
                    double totalResult =  Math.cos(b);
                    double result = Math.round(totalResult * 100.0) / 100.0;//bug -->return Math.sin(a), a is of type double in calculator.java
                    Assert.assertEquals(result, expectedResult);
                }catch(AssertionError e){
                    e.printStackTrace();
                }
}
        @Test(dataProviderClass = DataProviderClass.class,dataProvider = "dataProviderForSinPositiveTestCases")
        public void testSinPositiveTestCases(double a,double expectedResult) {
            try{
            double b = Math.toRadians(a);
            double totalResult = Math.sin(b);
            double result = Math.round(totalResult * 100.0) / 100.0;//bug--> a is of type double in calculator.java
            Assert.assertEquals(result, expectedResult);
            }catch(AssertionError e){
                e.printStackTrace();
            }
        }
        @Test(dataProviderClass = DataProviderClass.class,dataProvider = "dataProviderForSinNegativeTestCases")
        public void testSinNegativeTestCases(double a,double expectedResult) {
            try{
                double b = Math.toRadians(a);
                double totalResult = Math.sin(b);
                double result = Math.round(totalResult * 100.0) / 100.0;//bug--> a is of type double in calculator.java
                Assert.assertEquals(result, expectedResult);
            }catch(AssertionError e){
                e.printStackTrace();
            }
        }
        @Test(expectedExceptions = ArithmeticException.class)
        public void testExceptions()
         {
                double result = 4/0;
                Assert.assertEquals(result,2);
         }

}
