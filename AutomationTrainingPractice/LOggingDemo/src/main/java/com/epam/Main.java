package com.epam;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;



public class Main {
    public static final  Logger LOGGER = LogManager.getLogger(Main.class);
   // public static Logger logger= null;
//    public static void loadLog4J(){
//       String log4Jpath = System.getProperty("user.dir")+"/log4j.properties";
//        PropertyConfigurator.configure(log4Jpath);
//    }

    public static void main(String[] args) {
     //   logger = Logger.getLogger(Main.class.getName());
        LOGGER.debug("Debug logger");
        LOGGER.info("info logger");
        LOGGER.warn("Warn logger");
        LOGGER.error("Error logger");
        LOGGER.fatal("Fatal logger");



    }
}