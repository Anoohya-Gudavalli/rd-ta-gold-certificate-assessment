package CollectionsTask;
import java.util.*;

class Value implements Comparable<Value>{
	private int value;
	public Value(int value) {
		this.value=value;
	}
	  public int getValue() 
	    {
	        return value;
	    }
	  @Override
	  public String toString() 
	    {
	        return "Value : "+value;	
           }
	@Override
	public int compareTo(Value v) {
		if(this.getValue()<v.getValue()) {
			return 1;
		}
		else if(this.getValue()>v.getValue()) {
			return -1;
		}
		else {
			return 0;
		}
	}
}
 class reverse implements Comparator<Value>{
		@Override
	public int compare(Value v1, Value v2) {
			if (v1.getValue() <v2.getValue())
				return 1;
			else if (v1.getValue() > v2.getValue())
				return -1;
			else
				return 0;
		}
	}
	
public class ValueCollectionTask {

	public static void main(String args[]) {
	ArrayList<Value> List = new ArrayList<>();
	List.add(new Value(31));
	List.add(new Value(26));
	List.add(new Value(23));
	List.add(new Value(41));
	List.add(new Value(15));
	Collections.sort(List,new reverse());
	System.out.println("Reverse List : ");
	for(Value value:List) {
		System.out.println(value);
	}
	Value Second_Greatest_Number=List.get(1);
	System.out.println("2nd biggest number in the given list of numbers is "+Second_Greatest_Number);
//TreeSet that sorts the given set of numbers in reverse order 

	TreeSet<Value> IntegerTreeSet= new TreeSet<>();
	IntegerTreeSet.add(new Value (35));
	IntegerTreeSet.add(new Value(44));
	IntegerTreeSet.add(new Value(13));
	IntegerTreeSet.add(new Value(27));
	IntegerTreeSet.add(new Value(31));
	System.out.print("TreeSet Reverse order ");
	for(Value value:IntegerTreeSet) {
	System.out.println(value);
	}
//TreeMap that sorts the given set of values in descending order
	Map<Integer,Integer> IntegerTreeMap= new TreeMap<Integer,Integer>();
	IntegerTreeMap.put(1,35);
	IntegerTreeMap.put(2,44);
	IntegerTreeMap.put(3,13);
	IntegerTreeMap.put(4,27);
	IntegerTreeMap.put(5,31);
	Map<Integer,Integer> IntegerSortedTreeMap= new TreeMap<Integer,Integer>(new Comparator<Integer>() {
	@Override
	public int compare(Integer v1, Integer v2) {
		if (IntegerTreeMap.get(v1) <IntegerTreeMap.get(v2))
			return 1;
		else if (IntegerTreeMap.get(v1) >IntegerTreeMap.get(v2))
			return -1;
		else
			return 0;
	}
	
	});
	IntegerSortedTreeMap.putAll(IntegerTreeMap);
	System.out.println("TreeMap values in descending order");
	Set<Map.Entry<Integer, Integer>> entries = IntegerSortedTreeMap.entrySet();
	for(Map.Entry<Integer,Integer> entry:entries){
    System.out.println(entry.getValue());
	}
  }	
}