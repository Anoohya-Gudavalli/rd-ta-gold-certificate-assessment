import java.util.*;
import java.util.Map.Entry;

public class CollectionTasks {

	public static void main(String args[]) {
	ArrayList<Value> List = new ArrayList<>();
	List.add(new Value(31));
	List.add(new Value(26));
	List.add(new Value(23));
	List.add(new Value(41));
	List.add(new Value(15));
	Collections.sort(List,new reverse());
	System.out.println("Reverse List : ");
	for(Value value:List) {
		System.out.println(value);
	}
	Value Second_Greatest_Number=List.get(1);
	System.out.println("2nd biggest number in the given list of numbers is "+Second_Greatest_Number);
//TreeSet that sorts the given set of numbers in reverse order 

	TreeSet<Value> IntegerTreeSet= new TreeSet<>();
	IntegerTreeSet.add(new Value (35));
	IntegerTreeSet.add(new Value(44));
	IntegerTreeSet.add(new Value(13));
	IntegerTreeSet.add(new Value(27));
	IntegerTreeSet.add(new Value(31));
	System.out.print("TreeSet Reverse order ");
	for(Value value:IntegerTreeSet) {
	System.out.println(value);
	}
//TreeMap that sorts the given set of values in descending order
	Map<Integer,Integer> IntegerTreeMap= new TreeMap<Integer,Integer>();
	IntegerTreeMap.put(1,35);
	IntegerTreeMap.put(2,44);
	IntegerTreeMap.put(3,13);
	IntegerTreeMap.put(4,27);
	IntegerTreeMap.put(5,31);
	Map<Integer,Integer> IntegerSortedTreeMap= new TreeMap<Integer,Integer>(new Comparator<Integer>() {
	@Override
	public int compare(Integer v1, Integer v2) {
		if (IntegerTreeMap.get(v1) <IntegerTreeMap.get(v2))
			return 1;
		else if (IntegerTreeMap.get(v1) >IntegerTreeMap.get(v2))
			return -1;
		else
			return 0;
	}
	});
	IntegerSortedTreeMap.putAll(IntegerTreeMap);
	System.out.println("TreeMap values in descending order");
	Set<Map.Entry<Integer, Integer>> entries = IntegerSortedTreeMap.entrySet();
	for(Map.Entry<Integer,Integer> entry:entries){
    System.out.println(entry.getValue());
	}
	
	// Employee
	List<Employee> employeesList= new ArrayList<>();
	employeesList.add(new Employee("Krishna"));
	employeesList.add(new Employee("Shiva"));
	employeesList.add(new Employee("Radha"));
	employeesList.add(new Employee("Arjun"));
	employeesList.add(new Employee("Ram"));
	Collections.sort(employeesList,new CompareEmployee());
	System.out.println("Employee List:");
	for(Employee emp:employeesList) {
		System.out.println(emp);
	}
//TreeSet that sorts the given set of Employees in the alphabetic order of their name
	TreeSet<Employee> employeesTreeSet= new TreeSet<>();
	employeesTreeSet.add(new Employee("Krishna"));
	employeesTreeSet.add(new Employee("Shiva"));
	employeesTreeSet.add(new Employee("Radha"));
	employeesTreeSet.add(new Employee("Arjun"));
	employeesTreeSet.add(new Employee("Ram"));
	System.out.println("Employee TreeSet :");
	for(Employee emp:employeesTreeSet) {
		System.out.println(emp);
	}
//TreeMap that sorts the given set of employees in descending order of their name
	TreeMap<String,String> EmployeeTreeMap= new TreeMap<String,String>();
	EmployeeTreeMap.put("1","Krishna");
	EmployeeTreeMap.put("2","Shiva");
	EmployeeTreeMap.put("3","Radha");
	EmployeeTreeMap.put("4","Arjun");
	EmployeeTreeMap.put("5","Ram");
	System.out.println("EmployeeTreeMap:");
	Map<String,String>EmployeeSortedTreeMap= new TreeMap<String,String>(new Comparator<String>() {
	@Override
	public int compare(String v1, String v2) {
			return EmployeeTreeMap.get(v2).compareTo(EmployeeTreeMap.get(v1));
		}
	});
	EmployeeSortedTreeMap.putAll(EmployeeTreeMap);
	Set<Entry<String, String>> entrieset = EmployeeSortedTreeMap.entrySet();
	for(Entry<String, String> entry:entrieset){
    System.out.println(entry.getValue());
		}
//Collections.Sort to sort the given list of Employees in descending order of their name
	employeesList.sort(Comparator.comparing(Employee::getName, Comparator.reverseOrder()));
	System.out.println("Employee List reversed:");
	for(Employee emp:employeesList) {
		System.out.println(emp);
	}

  }	
}

