import java.util.*;
import java.io.*;
import java.util.function.Consumer;
class Employee implements Serializable {
	private String name;
	public Employee(String name) {
		this.name=name;
	}
	  public String getName() 
	    {
	        return name;
	    }
	  public String toString() 
	    {
	        return name;	
}
}
class Output {
public static void main(String args[]){
List<Employee> employeesList= new ArrayList<>();
employeesList.add(new Employee("Krishna"));
		employeesList.add(new Employee("Shiva"));
		employeesList.add(new Employee("Radha"));
		employeesList.add(new Employee("Arjun"));
		employeesList.add(new Employee("Ram"));
  Consumer<Employee> p = new Consumer<Employee>()
    		  {
    	  public void accept(Employee emp)
    	  {
 
    		  try {
  				File f = new File("Prod.txt");
  				ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(f));
  				oos.writeObject(emp);
  				oos.close();
  				System.out.println("TO File");
  			} catch (Exception e) {
  				// TODO Auto-generated catch block
  				e.printStackTrace();
  			}
     
  }
    		  };
      employeesList.forEach(p);
      
      
}
}