import myPackage.Address;
import java.util.*;
import java.lang.*;
import java.io.*;

class Employee{
	int id;
	String name;
	float salary;
	Address address;
	public void employeeDetails(int id, String name, float salary,Address address) {
		this.id=id;
		this.name=name;
		this.salary=salary;
		this.address=address;	
	}
	void printDetails() {
		System.out.println(id+" "+name+" "+salary);
		System.out.println(address.Floornumber+""+address.streetName+""+address.city+""+address.state+""+address.country);
	}
}
class JuniorSoftwarEngineer extends Employee{
	public void juniorSoftwareEngineer() {
		System.out.println("Assessment score");
		System.out.println("feedback");
	}
}
class SoftwarEngineer extends Employee{
	void printDetails() {
		System.out.println(id+" "+name+" "+salary);
		System.out.println(address.Floornumber+""+address.streetName+" "+address.city+"");
		System.out.println(address.state+""+address.country);
	}
	public void softwareEngineer() {
		System.out.println("Project name");
	}
}
class Trainer extends Employee{
	void printDetails() {
		System.out.println(id+" "+name+" "+salary);
		System.out.println(address.Floornumber+""+address.streetName);
		System.out.println(address.city+""+address.state+""+address.country);
	}
	public void trainer() {
		System.out.println("Skills");
		System.out.println("Certifications");
	}
}
class Course{
int courseId;
String courseName;
double courseDuration;
public void courseDetails(int i, String string,double d) {
	int courseId=i;
	String courseName=string;
	double courseDuration=d;
	System.out.println(courseId+" "+courseName+" "+courseDuration+" hours");
}
}
public class Training {
	public static void main(String[] args) {
		Course c1= new Course();
		Address address1= new Address(302," abc"," hyd"," telangana"," India");
		 Employee e1= new Employee();
	e1.employeeDetails(1,"Ram",1000,address1);
	JuniorSoftwarEngineer j1= new JuniorSoftwarEngineer();
	Trainer t1= new Trainer();
	t1.employeeDetails(2,"Shyam",1000,address1);
	t1.printDetails();
	j1.employeeDetails(3,"Krish",1000,address1);
		e1.printDetails();
		j1.printDetails();
		j1.juniorSoftwareEngineer();
		c1.courseDetails(516,"Java", 2.0);
		Scanner sc= new Scanner(System.in);
		String day = sc.nextLine();
		int k = sc.nextInt();
		sc.close();
		String result= Task5.dayOfWeek(day,k);
		System.out.println(result);
	
	}
class Task5{
	public static String dayOfWeek(String day,int k) {
		List<String> days = List.of("sunday","monday","tuesday","wednesday","thursday","friday","saturday");
		day = day.toLowerCase();
		int idx = days.indexOf(day);
		String dayaftr= days.get( (idx+k));
		return dayaftr;
	}
}


}
