
import java.util.*;

	public class CollectionTask{
		int Male_employees_count=0,Female_employees_count=0;
		HashSet<String> departmentList;
		HashMap<String,Integer> departmentscountMap;
		double maleEmployeeSalarySum=0;
		double femaleEmployeeSalarySum=0;
		//Task1
		public void maleAndFemaleCountInOrg(List<Employee> employeeList){
		for(Employee employee:employeeList) {
			if(employee.getGender().equals("Male")) {
				Male_employees_count++;
                       }
			else if(employee.getGender().equals("Female")) {
				Female_employees_count++;
			}
		}
		System.out.println("male and female employees are there in the organization");
		System.out.println("Male Employees Count in Organization = "+Male_employees_count);
		System.out.println("Female Employees Count in Organization = "+Female_employees_count);
		}
		//Task2
		public void departmentsList(List<Employee> employeeList){
		System.out.println("name of all departments in the organization");
		 departmentList=new HashSet<>();
		for(Employee employee:employeeList) {
		departmentList.add(employee.department);
               	}
		System.out.println("Departments:");
		for(String department:departmentList) {
		System.out.println(department);
		}
		}
		//Task3
		public void averageAgeOfEmployees(List<Employee> employeeList) {
			int Male_Employees_Age_Sum=0;
			int Female_Employees_Age_Sum=0;
			int averageAgeOfMaleEmployees;
			int averageAgeOfFemaleEmployees;
			for(Employee employee:employeeList) {
				if(employee.getGender().equals("Male")){
					Male_Employees_Age_Sum+=employee.age;
				}
				else if(employee.getGender().equals("Female")){
					Female_Employees_Age_Sum+=employee.age;
				}
			}
			 averageAgeOfMaleEmployees=(Male_Employees_Age_Sum/Male_employees_count);
			 averageAgeOfFemaleEmployees=(Female_Employees_Age_Sum/Female_employees_count);
			System.out.println("average age of male and female employees");
			System.out.println("Average age of male emp"+" "+ averageAgeOfMaleEmployees);
			System.out.println("Average age of female emp"+" "+ averageAgeOfFemaleEmployees);
			}
		//Task4
		public void detailsHighestPaidEmployee(List<Employee> employeeList) {
		System.out.println("details of highest paid employee in the organization");
		List<Double> salaryList=new ArrayList<>();
		for(Employee employee:employeeList) {
			salaryList.add(employee.salary);
		}
		Double highestPaidSalary=Collections.max(salaryList);
		for(Employee employee:employeeList) {
		if(highestPaidSalary==employee.salary) {
			System.out.println(employee);
		}
		}
		}
		//Task5
		public void employeesJoinedAfterYear(List<Employee> employeeList) {
		System.out.println("names of all employees who have joined after 2015");
		for(Employee employee:employeeList) {
			if(employee.yearOfJoining>2015) {
				System.out.println(employee.name);
			}
		}
		}
		//Task6
		public void departmentsCount(List<Employee> employeeList) {
			int count=0;
		 departmentscountMap=new HashMap<>();
		for(String department:departmentList) {
			for(Employee employee:employeeList) {
				if(employee.getDepartment().equals(department)) {
					count++;
				}
			}
			departmentscountMap.put(department,count);
			count=0;
				}
		System.out.println("number of employees in each department ");
		System.out.println(departmentscountMap);
		}
		//Task7
		public void departmentsAverageSalary(List<Employee> employeeList) {
		Double sumOfSalaryOfDepartment=0.0;
		Integer avgSalaryOfDepartment=0;
		HashMap<String,Integer> departmentsAvgSalaryMap=new HashMap<>();
		for(String department:departmentList) {
			for(Employee employee:employeeList) {
				if(employee.getDepartment().equals(department)) {
					sumOfSalaryOfDepartment+=employee.salary;
				}
				avgSalaryOfDepartment= (int) (sumOfSalaryOfDepartment/departmentscountMap.get(department));
			}
			departmentsAvgSalaryMap.put(department,avgSalaryOfDepartment);
				}
		System.out.println("average salary of each department ");
		System.out.println(departmentsAvgSalaryMap);
		}
		//Task8
		public void youngestMaleEmployeeInDepartment(List<Employee> employeeList) {
		System.out.println("details of youngest male employee in the product development department ");
		List<Integer> ageList_Of_MaleEmployees_In_ProductDevelopment=new ArrayList<>();
		for(Employee employee:employeeList) {
			if(employee.getGender().equals("Male")&&employee.getDepartment().equals("Product Development")){
				
				ageList_Of_MaleEmployees_In_ProductDevelopment.add(employee.getAge());
			}
		}
		int Minimum_Age_Of_Employee=Collections.min(ageList_Of_MaleEmployees_In_ProductDevelopment);
		for(Employee employee:employeeList) {
			if(employee.getGender().equals("Male")&&employee.getDepartment().equals("Product Development")&&employee.getAge()==(Minimum_Age_Of_Employee)) {
				System.out.println(employee);	
			}
		}
		}
		//Task9
		public void mostExperiencedEmployee(List<Employee> employeeList) {
		System.out.println("the most working experience in the organization ");
		List<Integer> yearofJoiningList=new ArrayList<>();
		for(Employee employee:employeeList) {
			yearofJoiningList.add(employee.getYearOfJoining());
		  }
		Integer LeastYear=Collections.min(yearofJoiningList);
		for(Employee employee:employeeList) {
			if(employee.yearOfJoining==LeastYear) {
				System.out.println(employee);
			}
	      }
		}
		//Task10
		public void maleAndFemaleCountOfSalesAndMarketing(List<Employee> employeeList) {

		int MaleCountOfSalesAndMarketing=0,FemaleCountOfSalesAndMarketing=0;
		for(Employee employee:employeeList) {
		if(employee.department=="Sales And Marketing" ) {
		
				if(employee.getGender().equals("Male")) {
					MaleCountOfSalesAndMarketing++;
	                       }
				else if(employee.gender=="Female") {
					FemaleCountOfSalesAndMarketing++;
				}
			}	
		}

		System.out.println("male and female employees are in the sales and marketing team ");
		System.out.println("Male"+" "+MaleCountOfSalesAndMarketing);
		System.out.println("Female"+" "+FemaleCountOfSalesAndMarketing);
			}
		//Task11
		public void employeesAverageSalary(List<Employee> employeeList) {
		for(Employee employee:employeeList) {
			if(employee.gender=="Male") {
				maleEmployeeSalarySum+=employee.salary  ;
                       }
			else if(employee.gender=="Female") {
				femaleEmployeeSalarySum+=employee.salary;
			}
		}
		int maleEmployeesAverageSalary=(int) (maleEmployeeSalarySum/Male_employees_count);
		int femaleEmployeesAverageSalary=(int) (femaleEmployeeSalarySum/Female_employees_count);
		System.out.println("the average salary of male and female employees ");
		System.out.println("Male"+" "+maleEmployeesAverageSalary);
		System.out.println("Female"+" "+femaleEmployeesAverageSalary);
		}
		//Task12
		public void departmentEmployeeNames(List<Employee> employeeList) {
		List<String> employeeName;
		HashMap<String,Object> departmentEmployeeNamesMap=new HashMap<>();
		for(String department:departmentList) {
			employeeName=new ArrayList<>();
			for(Employee employee:employeeList) {
				if(employee.getDepartment().equals(department)) {
					
					employeeName.add(employee.name);
				}
			}
			departmentEmployeeNamesMap.put(department,employeeName);
				}
		System.out.println("names of all employees in each department ");
		System.out.println(departmentEmployeeNamesMap);
		}
		//Task13
		public void averageSalaryOfEmployees(List<Employee> employeeList) {
		double TotalSalary=maleEmployeeSalarySum+femaleEmployeeSalarySum;
		System.out.println("total salary of the whole organization");
		System.out.println(TotalSalary);
		double averageSalaryOfEmployees= (TotalSalary/employeeList.size());
		System.out.println("average salary of the whole organization");
		System.out.println(averageSalaryOfEmployees);
		}
		//Task14
		public void employeesYoungerOrEqualToTwentyfive(List<Employee> employeeList) {
		List<Employee> youngEmployees=new ArrayList<>();
		List<Employee> oldEmployees=new ArrayList<>();
		for(Employee employee:employeeList) {
			if(employee.getAge()<=25) {
				youngEmployees.add(employee);
			}
			else {
				oldEmployees.add(employee);
			}
		}
		System.out.println("Employees who are younger or equal to 25 years ");
		System.out.println(youngEmployees);
System.out.println("employees who are older than 25 years ");
		System.out.println(oldEmployees);
		}
		//Task15
		public void employeeWithMaxAge(List<Employee> employeeList) {
		System.out.println("oldest employee in the organization ");
		List<Integer> ageList=new ArrayList<>();
		for(Employee employee:employeeList) {
			ageList.add(employee.getAge());
		}
		int MaxAge=Collections.max(ageList);
		for(Employee employee:employeeList) {
			if(employee.age==MaxAge) {
				System.out.println(employee.getName()+" "+employee.getAge()+" "+employee.getDepartment());
			}
		}
		}
		}
