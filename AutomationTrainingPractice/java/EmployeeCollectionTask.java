package CollectionsTask;

import java.util.*;
import java.util.Map.Entry;
class Employee implements Comparable<Employee>{
	private String name;
	public Employee(String name) {
		this.name=name;
	}
	  public String getName() 
	    {
	        return name;
	    }
	  public String toString() 
	    {
	        return name;	
}
	@Override
	public int compareTo(Employee e) {
		return this.getName().compareTo(e.getName());
	}
}
  class CompareEmployee implements Comparator<Employee>{
			@Override
			public int compare(Employee e1, Employee e2) {
				return e1.getName().compareTo(e2.getName());
			}
		}
public class EmployeeCollectionTask {
	
public static void main(String args[]) {
	List<Employee> employeesList= new ArrayList<>();
	employeesList.add(new Employee("Krishna"));
	employeesList.add(new Employee("Shiva"));
	employeesList.add(new Employee("Radha"));
	employeesList.add(new Employee("Arjun"));
	employeesList.add(new Employee("Ram"));
	Collections.sort(employeesList,new CompareEmployee());
	System.out.println("Employee List:");
	for(Employee emp:employeesList) {
		System.out.println(emp);
	}
//TreeSet that sorts the given set of Employees in the alphabetic order of their name
	TreeSet<Employee> employeesTreeSet= new TreeSet<>();
	employeesTreeSet.add(new Employee("Krishna"));
	employeesTreeSet.add(new Employee("Shiva"));
	employeesTreeSet.add(new Employee("Radha"));
	employeesTreeSet.add(new Employee("Arjun"));
	employeesTreeSet.add(new Employee("Ram"));
	System.out.println("Employee TreeSet :");
	for(Employee emp:employeesTreeSet) {
		System.out.println(emp);
	}
//TreeMap that sorts the given set of employees in descending order of their name
	TreeMap<String,String> EmployeeTreeMap= new TreeMap<String,String>();
	EmployeeTreeMap.put("1","Krishna");
	EmployeeTreeMap.put("2","Shiva");
	EmployeeTreeMap.put("3","Radha");
	EmployeeTreeMap.put("4","Arjun");
	EmployeeTreeMap.put("5","Ram");
	System.out.println("EmployeeTreeMap:");
	Map<String,String>EmployeeSortedTreeMap= new TreeMap<String,String>(new Comparator<String>() {
	@Override
	public int compare(String v1, String v2) {
			return EmployeeTreeMap.get(v2).compareTo(EmployeeTreeMap.get(v1));
		}
	});
	EmployeeSortedTreeMap.putAll(EmployeeTreeMap);
	Set<Entry<String, String>> entries = EmployeeSortedTreeMap.entrySet();
	for(Entry<String, String> entry:entries){
    System.out.println(entry.getValue());
		}
//Collections.Sort to sort the given list of Employees in descending order of their name
	employeesList.sort(Comparator.comparing(Employee::getName, Comparator.reverseOrder()));
	System.out.println("Employee List reversed:");
	for(Employee emp:employeesList) {
		System.out.println(emp);
	}
    }
}




