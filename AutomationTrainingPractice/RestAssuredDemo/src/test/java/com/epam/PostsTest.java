package com.epam;


import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;

public class PostsTest {

    @Test
    public void verifyPostsInformation() {
        RestAssured.baseURI = "https://jsonplaceholder.typicode.com";
       // RestAssured.basePath = "posts/";
        RequestSpecification httpRequest = given();
        Response httpResponse = httpRequest.request(Method.GET, "/posts/31");
        PostsPojo postsPojo = httpResponse.as(PostsPojo.class);
        System.out.println(postsPojo.getTitle());


        getResponseInbuiltMap(httpResponse);
         getRespAsString(httpResponse);
        getStatusCodeAndHeaderInfo(httpResponse);
        getPostsTitleResponse(httpResponse);
    }


    private void getResponseInbuiltMap(Response httpResponse) {
        Map postsResponse = httpResponse.as(HashMap.class);
        System.out.println(postsResponse.get("body"));
    }
    private void getRespAsString(Response httpResponse) {
        String postsResponse = httpResponse.asString();
        httpResponse.prettyPrint();
        System.out.println(postsResponse.contains("ullam ut quidem id aut vel consequuntur"));
    }
        private void getPostsTitleResponse(Response httpResponse){
        JsonPath postsResponse = httpResponse.jsonPath();
        System.out.println(postsResponse.getString("title"));
    }

        private void getStatusCodeAndHeaderInfo  (Response httpResponse){
        httpResponse.prettyPrint();
        System.out.println(httpResponse.getStatusLine());
        System.out.println(httpResponse.getHeader("Content-Type"));
        Assert.assertEquals(httpResponse.getStatusCode(),200);
    }

    @Test
    public void validateResponse(){
        RestAssured.baseURI = "https://jsonplaceholder.typicode.com";
        RequestSpecification httpRequest = given();
        Response httpResponse = httpRequest.request(Method.GET, "/posts?id=31");
       Response postsResponse = httpResponse;
       JsonPath postsJson = postsResponse.jsonPath();
        System.out.println(postsJson.get("title").toString());
    }
    @Test
    public void verifyNewPostsCreation(){
        RestAssured.baseURI = "https://jsonplaceholder.typicode.com";
        RestAssured.basePath = "posts/";
        String postsBody= "{\n"+
                "    \"userId\": 4,\n" +
                "    \"id\": 31,\n" +
                "    \"title\": \"ullam ut quidem id aut vel consequuntur\",\n" +
                "    \"body\": \"debitis eius sed quibusdam non quis consectetur vitae\\nquidem sit nostrum et maiores adipisci atque\\nquaerat voluptatem adipisci repudiandae\"\n"+
                "}";
        RequestSpecification postsRequest = given();
        Response addPostsResponse = postsRequest
                .header("Content-type","application/json")
                .body(postsBody)
                .request(Method.POST);
        addPostsResponse.prettyPrint();
    }

    @Test
    public void verifyNewPostsCreationJson(){
        RestAssured.baseURI = "https://jsonplaceholder.typicode.com";
        RestAssured.basePath = "posts/";
        RequestSpecification postsRequest = given();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("userId",4);
        jsonObject.put("id",4);
        jsonObject.put("title","DemoTitle");
        jsonObject.put("body","DemoBody");

        Response postsResponse =postsRequest
                .header("Content-type","application/json")
                .body(jsonObject.toString())
//                .post("/posts");
        .request(Method.POST);
        postsResponse.prettyPrint();

    }
    @Test
    public void setInfoUsingPojo() {
        RestAssured.baseURI = "https://jsonplaceholder.typicode.com";
        RestAssured.basePath = "posts/";
        RequestSpecification postsRequest = given();
        PostsPojo pojoObject = new PostsPojo();
        pojoObject.setUserId(6);
        pojoObject.setId(4);
        pojoObject.setTitle("Title set using pojo");
        pojoObject.setBody("DemoBody set using pojo");

        Response postsResponse = postsRequest
                .header("Content-type", "application/json")
                .body(pojoObject)
                .request(Method.POST);
        postsResponse.prettyPrint();

    }
    @Test
    public void deleteRequest(){
        int id = 31;
        RestAssured.baseURI = "https://jsonplaceholder.typicode.com";
        RequestSpecification request = given();
        Response response = request.delete("/posts/"+id);
        System.out.println(response.getStatusCode());
    }
    @Test
    public void basicTest(){
        given()
                .auth()
                .basic("postman","password")
                .get("https://postman-echo.com/basic-auth")
                .then()
                .statusCode(200);
    }
    @Test
    public void preemptiveTest(){
        given()
                .auth()
                .preemptive()
                .basic("postman","password")
                .get("https://postman-echo.com/basic-auth")
                .then()
                .statusCode(200);
    }
    @Test
    public void digestTest(){
        given()
                .auth()
                .digest("postman","password")
                .get("https://postman-echo.com/basic-auth")
                .then()
                .statusCode(200);
    }
    @Test
    public void oauthTest(){

        given()
                .auth()
                .oauth2("ab9c97aac104fa632e6c03394ff7282c5d9f37a5231482bac5ef27beecec4cbe")
                .header("Content-type", "application/json")
                .get("https://api.nytimes.com/svc/books/v3//lists/names.json")
                .then()
                .statusCode(200);
    }




}
