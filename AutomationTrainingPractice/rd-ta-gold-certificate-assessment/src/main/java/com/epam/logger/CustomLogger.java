package com.epam.logger;

public interface CustomLogger {
    public void log(LogLevel level, String message);

}
