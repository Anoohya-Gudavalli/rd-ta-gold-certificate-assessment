package com.epam.logger.loggerFactory;


import com.epam.logger.CustomLogger;
import com.epam.logger.LoggerType;
import com.epam.logger.loggerExceptions.ArgumentNotSupported;
import com.epam.logger.loggersImplementation.Log4J2Logger;
import com.epam.logger.loggersImplementation.Slf4JLogger;

public class LoggerFactory {

    private static CustomLogger logger = null;

    private LoggerFactory(){
    }

    public static CustomLogger getLogger(LoggerType loggerType) {
        if (logger == null) {
            createLogger(loggerType);
        }
        return logger;
    }

    private static void createLogger(LoggerType loggerType) {
        switch (loggerType) {
            case LOG4J2:
                logger = new Log4J2Logger();
                break;
            case SL4J:
                logger = new Slf4JLogger();
                break;
            default:
                throw new ArgumentNotSupported("Currently Supported Loggers - log4j and slf4j");
        }
    }
}
