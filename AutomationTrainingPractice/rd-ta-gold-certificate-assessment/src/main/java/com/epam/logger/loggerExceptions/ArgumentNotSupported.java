package com.epam.logger.loggerExceptions;

public class ArgumentNotSupported extends RuntimeException{
    public ArgumentNotSupported(String errorMessage) {
        super(errorMessage);
    }

}
