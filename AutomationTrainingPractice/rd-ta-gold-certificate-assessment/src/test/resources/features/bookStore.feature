Feature: This feature is about getting details of all books and validating the details with displayed
  details on book store application
  Background: Browser is launched with a url
    Given user launch the  book url

 @regression
  Scenario: This scenario is to validate title of the book
    Given user creates http request to book store books base uri
    When user gets the response of books
    Then check title displayed in website

  @regression
  Scenario: This scenario is to validate author of the book
    Given user creates http request to book store books base uri
    When user gets the response of books
    Then check author displayed in website

  @regression
  Scenario: This scenario is to validate publisher of the book
    Given user creates http request to book store books base uri
    When user gets the response of books
    Then check publisher displayed in website
