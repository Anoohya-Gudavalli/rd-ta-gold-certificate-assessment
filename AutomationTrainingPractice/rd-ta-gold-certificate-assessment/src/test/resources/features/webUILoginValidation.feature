Feature: This feature is about checking the login capabilities of demo.qa website

  @regression @smoke
  Scenario Outline: This scenario is about logging in into the website using credentials of post request
    Given user launch the login url
    When enters the "<username>" and "<password>"
    And clicks login button
    Then check username displayed on the dashboard page is "<username>"
    Examples:
      | username | password |
      | sweetu   |sweetU@2  |



