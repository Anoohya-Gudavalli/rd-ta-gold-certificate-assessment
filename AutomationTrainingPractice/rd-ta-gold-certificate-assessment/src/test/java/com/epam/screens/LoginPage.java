package com.epam.screens;

import lombok.extern.java.Log;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class LoginPage extends WebDriverClass{
    @FindBy(id="userName")
    private WebElement userNameInputField;

    @FindBy(id="password")
    private WebElement passwordInputField;
    @FindBy(id="login")
    private WebElement loginButton;
    public LoginPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(this.driver,this);
    }
    public void enterUserNameInInputField(String username){
        WebDriverWait webDriverWait= new WebDriverWait(driver, Duration.ofSeconds(5));
        webDriverWait.until(ExpectedConditions.elementToBeClickable(userNameInputField));
        userNameInputField.clear();
        userNameInputField.sendKeys(username);
    }
    public void enterPasswordInInputField(String password){
        WebDriverWait webDriverWait= new WebDriverWait(driver, Duration.ofSeconds(5));
        webDriverWait.until(ExpectedConditions.elementToBeClickable(passwordInputField));
        passwordInputField.clear();
        passwordInputField.sendKeys(password);
    }
    public DashboardPage login(){
        JavascriptExecutor javascript = (JavascriptExecutor) driver;
        javascript.executeScript("arguments[0].scrollIntoView(true);", loginButton);
        loginButton.click();
        return new DashboardPage(driver);
    }
    public String getUsernameValidationMessageForSignUp(){
        return  userNameInputField.getAttribute("validationMessage");
    }
    public String getPasswordValidationMessageForSignUp(){
        return passwordInputField.getAttribute("validationMessage");
    }


}
