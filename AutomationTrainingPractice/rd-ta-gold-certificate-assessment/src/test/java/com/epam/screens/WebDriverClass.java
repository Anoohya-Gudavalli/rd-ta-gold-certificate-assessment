package com.epam.screens;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public abstract class WebDriverClass {
    protected final WebDriver driver;

    public WebDriverClass(WebDriver driver){
        this.driver=driver;
    }
    public Boolean isElementPresent(By locator)
    {
        return driver.findElements(locator).size() > 0;
    }
}
