package com.epam.screens;

import com.epam.tests.Base;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
public class BookStorePage extends WebDriverClass{
    @FindBy(xpath = "//*[@id=\"item-0\"]")
    private WebElement loginPageButton;
    @FindBy(xpath = "//*[@id=\"app\"]/div/div/div[2]/div[1]/div/div/div[6]/span/div/div[2]/div[2]/svg")
    private WebElement dropDown;
    @FindBy(xpath = "//*[@id=\"see-book-Git Pocket Guide\"]/a")
    private WebElement book1Title;
    @FindBy(xpath = "//*[@id=\"app\"]/div/div/div[2]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/div/div[3]")
    private WebElement book1Author;

    @FindBy(xpath = "//*[@id=\"app\"]/div/div/div[2]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/div/div[4]")
    private WebElement book1Publisher;
    public BookStorePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(this.driver,this);
    }
    public LoginPage navigateToLoginPage(){

        return new LoginPage(driver);
    }
    public String getBook1Title(){
        return book1Title.getText();
    }
public String getBook1Author() {
    return book1Author.getText();
}
public String getBook1Publisher(){
    return book1Publisher.getText();
}
}
