package com.epam.runners;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(plugin = {"com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:", "pretty", "html:report.html", "com.epam.attributes.CustomAttributeReporter"},
        tags = "@regression",glue = {"com/epam/tests"},features ={"src/test/resources/features/bookStore.feature","src/test/resources/features/loginValidation.feature","src/test/resources/features/webUILoginValidation.feature"},publish = true)
public class BaseRunner extends AbstractTestNGCucumberTests {
}
