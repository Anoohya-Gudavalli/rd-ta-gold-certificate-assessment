package com.epam.bankOperations;

public class WithdrawCash {
BankingApp bankingApp = new BankingApp();
    public long withdrawal (long withdrawAmount,long transactionBalance){
        long balance = transactionBalance;
        if (balance >= withdrawAmount) {
           balance = balance - withdrawAmount;
        } else {
            System.out.println("not enough balance...Transaction failed!!");
        }
        return balance;
    }

}
