package com.epam.bankOperations;



public class BankingApp {
    long balance ;

    public long getBalance() {
        return balance;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }
    public void request(long withdrawAmount){
       WithdrawCash withdrawCash = new WithdrawCash();
        long balance = getBalance();
        long updatedBalance =
                withdrawCash.withdrawal(withdrawAmount,balance);
        setBalance(updatedBalance);
    }

}
