package com.epam;

import com.epam.bankOperations.AccountDetails;
import com.epam.bankOperations.BankingApp;
import com.epam.bankOperations.WithdrawCash;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;

public class StepDefinitions {
    private static final Logger LOGGER = LogManager.getLogger(StepDefinitions.class);
   BankingApp bankingApp = new BankingApp();

   private long   balanceBeforeWithdrawal;

    @Given("^I have a balance of \\$(\\d+) in my account$")
    public void iHaveABalanceOf$InMyAccount(long amount) {
        bankingApp.setBalance(amount);
       balanceBeforeWithdrawal =bankingApp.getBalance();
       Assert.assertEquals(bankingApp.getBalance(),amount);
        LOGGER.info("Balance = "+balanceBeforeWithdrawal);
    }
    @When("^I request \\$(\\d+)$")
    public void iRequest$(long withdrawAmount) {
      bankingApp.request(withdrawAmount);
        LOGGER.info("requested to dispense $20");

    }
    @Then("^\\$(\\d+) should be dispensed$")
    public void $ShouldBeDispensed(long dispensedAmount) {
       Assert.assertEquals(  balanceBeforeWithdrawal-bankingApp.getBalance(),dispensedAmount);
        Assert.assertEquals(bankingApp.getBalance(),80);
        LOGGER.info("$20 dispensed successfully");
    }
}
