package tests;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import utility.MyChromeDriver;
import utility.MyChromeDriverWithCapabilities;
import utility.MyGeckoDriver;
import utility.WebDriverCreator;

public class SeleniumTests {

    @Test
    public void getPage(){
        WebDriverCreator webDriverCreator= new MyChromeDriverWithCapabilities();
        WebDriver driver = webDriverCreator.getDriver();
        driver.navigate().to("https://www.selenium.dev/");
    }
}
