package utility;

import org.checkerframework.checker.units.qual.C;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class MyChromeDriverWithCapabilities extends WebDriverCreator{

    @Override
    public WebDriver getDriver() {
        Proxy proxy = new Proxy();
        proxy.setHttpProxy("myhttpproxy:3337");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        options.addArguments("start-maximized");
        options.setCapability("proxy", proxy);
        WebDriver driver = new ChromeDriver(options);
        return driver;
    }
}
