package utility;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class MyChromeDriver extends WebDriverCreator{
    @Override
    public WebDriver getDriver() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Anoohya_Gudavalli\\Documents\\Drivers\\ChromeDriver\\chromedriver.exe");
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--remote-allow-origins=*");
        WebDriver myChromeDriver =  new ChromeDriver(chromeOptions);
        return myChromeDriver;
    }
}
