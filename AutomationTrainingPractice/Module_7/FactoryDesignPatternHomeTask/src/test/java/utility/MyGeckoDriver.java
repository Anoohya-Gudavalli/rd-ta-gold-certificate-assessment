package utility;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class MyGeckoDriver extends WebDriverCreator
{

    @Override
    public WebDriver getDriver() {
        WebDriver myFirefoxDriver = new FirefoxDriver();
        return myFirefoxDriver;
    }
}
