package com.epam.screens;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class HomePage extends WebDriverClass{


    @FindBy(xpath = "//*[@id=\"header\"]/div/div/div/div[2]/div/ul/li[1]/a")
    private WebElement homePageButton;
    @FindBy(xpath="//*/a[@href='/login']")
    private WebElement signUpButton;
    @FindBy(xpath = "//*[@href='/logout']")
    private WebElement logout;

    public HomePage(WebDriver driver) {
       super(driver);
        PageFactory.initElements(driver,this);
    }
    public HomePage navigateToHomepage(){
        WebDriverWait webDriverWait= new WebDriverWait(driver, Duration.ofSeconds(2));
        webDriverWait.until(ExpectedConditions.elementToBeClickable( homePageButton));
        homePageButton.click();
        return new HomePage(driver);
    }
    public LoginPage getLoginPage(){
        WebDriverWait webDriverWait= new WebDriverWait(driver, Duration.ofSeconds(2));
        webDriverWait.until(ExpectedConditions.elementToBeClickable( signUpButton));
        signUpButton.click();
        return new LoginPage(driver);
    }

}
