package com.epam.tests;

import io.cucumber.java.en.Then;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;

public class AccountCreatedPageStepDefinitions {
    private final static Logger LOGGER = LogManager.getLogger(AccountCreatedPageStepDefinitions.class);

    private Base base;

    public AccountCreatedPageStepDefinitions(Base base){
        this.base = base;
    }
    @Then("check account is created")
    public void checkAccountIsCreated() {
        Assert.assertEquals(base.getAccountCreatedPage().getAccountCreatedText(),"ACCOUNT CREATED!");
    LOGGER.info("New account is created");
    }

    @Then("Click continue")
    public void clickContinue() {
        base.getAccountCreatedPage().clickContinue();
        LOGGER.info("clicked continue after the account creation");
    }

}
