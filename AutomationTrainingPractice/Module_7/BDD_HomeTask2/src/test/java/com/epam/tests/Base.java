package com.epam.tests;

import com.epam.driver.DriverSingleton;
import com.epam.reportportal.testng.ReportPortalTestNGListener;
import com.epam.screens.*;
import com.epam.utility.TestListener;
import io.cucumber.java.BeforeAll;
import io.cucumber.java.en.Given;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;

@Listeners({com.epam.reportportal.testng.ReportPortalTestNGListener.class, TestListener.class})
public class Base {

    protected static WebDriver driver;
    protected static ThreadLocal<WebDriver> threadLocalDriver = new ThreadLocal<>();

    public final String BASE_URL ="https://automationexercise.com/";
    public HomePage homePage;
    public LoginPage loginPage;
    public SignUpPage signUpPage;
    public AccountCreatedPage accountCreatedPage;
    public HomePageSignedIn homePageSignedIn;

    public String name;


    public HomePage getHomePage() {
        return homePage;
    }

    public void setHomePage(HomePage homePage) {
        this.homePage = homePage;
    }

    public LoginPage getLoginPage() {
        return loginPage;
    }

    public void setLoginPage(LoginPage loginPage) {
        this.loginPage = loginPage;
    }

    public SignUpPage getSignUpPage() {
        return signUpPage;
    }

    public void setSignUpPage(SignUpPage signUpPage) {
        this.signUpPage = signUpPage;
    }

    public AccountCreatedPage getAccountCreatedPage() {
        return accountCreatedPage;
    }

    public void setAccountCreatedPage(AccountCreatedPage accountCreatedPage) {
        this.accountCreatedPage = accountCreatedPage;
    }

    public HomePageSignedIn getHomePageSignedIn() {
        return homePageSignedIn;
    }

    public void setHomePageSignedIn(HomePageSignedIn homePageSignedIn) {
        this.homePageSignedIn = homePageSignedIn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Given("user launch the base url")
    public void launchBaseURL(){
        getDriver().get(BASE_URL);
        setHomePage(new HomePage(getDriver()));
        setLoginPage(new LoginPage(getDriver()));
        setSignUpPage(new SignUpPage(getDriver()));
        setAccountCreatedPage(new AccountCreatedPage(getDriver()));
        setHomePageSignedIn(new HomePageSignedIn(getDriver()));
    }

    public static WebDriver getDriver(){
        return threadLocalDriver.get();
    }


}
