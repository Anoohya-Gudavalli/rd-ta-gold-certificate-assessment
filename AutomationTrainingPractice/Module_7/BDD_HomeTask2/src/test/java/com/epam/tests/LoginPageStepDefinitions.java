package com.epam.tests;

import com.epam.screens.LoginPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;

public class LoginPageStepDefinitions {
    private final static Logger LOGGER = LogManager.getLogger(LoginPageStepDefinitions.class);

    private Base base;

    public LoginPageStepDefinitions(Base base){
        this.base = base;
    }


    @Then("message pops up validating email as required value and returns same page")
    public void messagePopsUpValidatingEmailAsRequiredValueAndReturnsSamePage() {
        Assert.assertEquals(base.getLoginPage().getEmailValidationMessageForLogin(),"Please fill out this field.");
        Assert.assertEquals(base.getDriver().getCurrentUrl(),"https://automationexercise.com/login");
        LOGGER.info("validated error message");
    }

    @And("user enters email as {string}")
    public void userEntersEmail(String email) {
        base.getLoginPage().enterLoginEmail(email);
    }

    @And("user enters password as {string}")
    public void userEntersPasswordAs(String password) {
        base.getLoginPage().enterLoginPassword(password);
    }

    @And("clicks on login")
    public void clicksOnLogin(){base.getLoginPage().login();
    }
    @Then("message pops up validating password as required value and returns same page")
    public void messagePopsUpValidatingPasswordAsRequiredValueAndReturnsSamePage() {
        Assert.assertEquals(base.getLoginPage().getPasswordValidationMessage(),"Please fill out this field.");
        Assert.assertEquals(base.driver.getCurrentUrl(),"https://automationexercise.com/login");
        LOGGER.info("validated error message");
    }

    @And("validate error message as wrong email or password and returns same page")
    public void validateErrorMessageAsWrongEmailOrPassword() {
        Assert.assertEquals(base.getLoginPage().getEmailOrPasswordInvalidMessage(),"Your email or password is incorrect!");
        Assert.assertEquals(base.driver.getCurrentUrl(),"https://automationexercise.com/login");
        LOGGER.info("validated error message");
    }

    @And("validate error message as {string} is an invalid email address for login and returns same page")
    public void validateErrorMessageAsInvalidEmail(String email) {
        Assert.assertEquals(base.getLoginPage().getEmailValidationMessageForLogin(),"Please include an '@' in the email address."+" '"+email+"' "+"is missing an '@'.");
        Assert.assertEquals(base.driver.getCurrentUrl(),"https://automationexercise.com/login");
        LOGGER.info("validated error message");
    }

    @And("enter username into input field as {string}")
    public void enterUsernameIntoInputFieldAs(String username) {
      base.getLoginPage().enterUsername(username);
      LOGGER.info("user entered username");
    }

    @And("enter email address into input field as {string}")
    public void enterEmailAddressIntoInputFieldAs(String email) {
        base.getLoginPage().enterEmailAddress(email);
        LOGGER.info("user entered email");
    }

    @And("clicks on submit button")
    public void clicksOnSubmitButton() {
        base.getLoginPage().clickSubmitButton();
        LOGGER.info("user clicked on login");
    }

    @Then("email already exists msg pops up")
    public void emailAlreadyExistsMsgPopsUp() {
        base.getLoginPage().getEmailAlreadyExistsText();
        LOGGER.info("validated email already exist message");
    }
    @Then("message pops up validating email address as required value and returns same page")
    public void messagePopsUpValidatingEmailAddressAsRequiredValueAndReturnsSamePage() {
        Assert.assertEquals(base.getLoginPage().getEmailValidationMessageForSignUp(),"Please fill out this field.");
        Assert.assertEquals(base.driver.getCurrentUrl(),"https://automationexercise.com/login");
        LOGGER.info("validated error message");
    }

    @Then("message pops up validating username as required value and returns same page")
    public void messagePopsUpValidatingUsernameAsRequiredValueAndReturnsSamePage() {
        Assert.assertEquals(base.getLoginPage().getUsernameValidationMessageForSignUp(),"Please fill out this field.");
        Assert.assertEquals(base.driver.getCurrentUrl(),"https://automationexercise.com/login");
        LOGGER.info("validated error message");
    }

    @Then("validate error message as {string} is an invalid email address for sign up and returns same page")
    public void validateErrorMessageAsIsAnInvalidEmailAddressForSignUpAndReturnsSamePage(String email) {
        Assert.assertEquals(base.getLoginPage().getEmailValidationMessageForSignUp(),"Please include an '@' in the email address."+" '"+email+"' "+"is missing an '@'.");
        Assert.assertEquals(base.driver.getCurrentUrl(),"https://automationexercise.com/login");
        LOGGER.info("validated error message");
    }

    @And ("submit the details")
    public void submitTheDetails() {
        base.getLoginPage().signUp();
        LOGGER.info("user clicked signup");
    }
}
