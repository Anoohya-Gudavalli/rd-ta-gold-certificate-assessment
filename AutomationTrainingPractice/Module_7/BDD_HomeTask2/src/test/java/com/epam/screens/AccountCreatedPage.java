package com.epam.screens;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class AccountCreatedPage extends WebDriverClass{
    @FindBy(xpath = "//*[@id=\"form\"]/div/div/div/h2/b")
    private WebElement accountCreatedText;
    @FindBy(xpath = "//*[@href='/' and @data-qa='continue-button' ]")
    private WebElement continueButton;
    public AccountCreatedPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(this.driver,this);
    }
    public String getAccountCreatedText(){
        return accountCreatedText.getText();

    }
    public HomePageSignedIn clickContinue(){
        continueButton.click();
        return new HomePageSignedIn(driver);
    }
}
