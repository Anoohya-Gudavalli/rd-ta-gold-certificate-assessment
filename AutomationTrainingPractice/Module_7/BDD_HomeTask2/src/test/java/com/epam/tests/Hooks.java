package com.epam.tests;

import com.epam.driver.DriverSingleton;
import com.epam.tests.Base;
import io.cucumber.java.AfterAll;
import io.cucumber.java.AfterStep;
import io.cucumber.java.BeforeAll;
import io.cucumber.java.BeforeStep;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

import static com.epam.tests.Base.*;


public class Hooks {

    @BeforeAll
    public static void setUp(){
        try {
         driver = DriverSingleton.getDriver();
            threadLocalDriver.set(driver);
            System.out.println("Before Test Thread ID: "+Thread.currentThread().getId());
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
    @AfterAll()
    public static void stopBrowser(){
            DriverSingleton.closeDriver();
            System.out.println("After Test Thread ID: "+Thread.currentThread().getId());
            threadLocalDriver.remove();

    }
}
