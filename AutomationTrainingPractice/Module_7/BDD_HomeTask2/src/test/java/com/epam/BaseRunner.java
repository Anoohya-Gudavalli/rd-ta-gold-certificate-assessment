package com.epam;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(plugin = {"com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:", "pretty", "html:report.html", "com.epam.attributes.CustomAttributeReporter"},
         tags = "@signInAndSignOut",glue = {"com/epam/tests"},features ={"src/test/resources/features/registrationCapabilities.feature","src/test/resources/features/signInAndSignOutCapabilities.feature"},publish = true)
public class BaseRunner extends AbstractTestNGCucumberTests {

}