package com.epam.tests;

import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class HomePageSignedInStepDefinitions {
    private final static Logger LOGGER = LogManager.getLogger(HomePageSignedInStepDefinitions.class);

    private Base base;

    public HomePageSignedInStepDefinitions(Base base){
        this.base = base;
    }
    @And("user navigated to homepage after sign in as {string}")
    public void userNavigatedToHomepageAfterSignIn(String username) {
        Assert.assertEquals(base.getHomePageSignedIn().loggedInUser(),username);
        LOGGER.info("user navigated to homepage after signin");
    }
    @When("user clicks on logout")
    public void userClicksOnLogout() {
        if(base.getDriver().getCurrentUrl().equals("https://automationexercise.com/account_created#google_vignette")){
            base.getDriver().manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            base.getDriver().navigate().refresh();
        }

        base.getHomePageSignedIn().logout();
        LOGGER.info("user clicked logout");
        String paramValue = System.getProperty("PARAM1");
        LOGGER.info(paramValue);
        System.out.println(paramValue);
        System.out.println("print on console");
    }
}
