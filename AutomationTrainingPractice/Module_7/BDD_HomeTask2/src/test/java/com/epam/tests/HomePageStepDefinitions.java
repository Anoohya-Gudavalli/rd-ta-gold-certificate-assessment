package com.epam.tests;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;

public class HomePageStepDefinitions {
    private final static Logger LOGGER = LogManager.getLogger(HomePageStepDefinitions.class);

    private Base base;

    public HomePageStepDefinitions(Base base){
        this.base = base;
    }
    @Given("a user navigates to Homepage")
    public void aUserNavigatesToHomepage() {
        base.getHomePage().navigateToHomepage();
        LOGGER.info("navigated to homepage");
    }
    @When("the user clicks on Signup")
    public void theUserClicksOnSignup() {
        base.getHomePage().getLoginPage();
        LOGGER.info("clicked on signup and navigated to registration page");
    }
    @Then("user gets logged out")
    public void userGetsLoggedOut() {
        Assert.assertEquals(base.driver.getCurrentUrl(),"https://automationexercise.com/login");
        LOGGER.info("user is logged out");
    }
}
