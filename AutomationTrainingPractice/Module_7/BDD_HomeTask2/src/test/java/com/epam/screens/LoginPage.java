package com.epam.screens;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage extends WebDriverClass {

    @FindBy(xpath = "//*[@placeholder = 'Name' and @data-qa='signup-name' ]")
    private WebElement usernameInputField;
    @FindBy(xpath = "//*[@placeholder = 'Email Address' and @data-qa='signup-email' ]")
    private WebElement emailInputField;
    @FindBy(xpath = "//*[@type='submit' and @data-qa='signup-button'] ")
    private WebElement submitButton;
    @FindBy(xpath = "//*[@data-qa='login-email' and @name='email']")
    private WebElement loginEmail;
    @FindBy(xpath = "//*[@data-qa='login-password' and @name='password']")
    private WebElement loginPassword;
    @FindBy(xpath = "//*[@type='submit' and @data-qa='login-button'] ")
    private WebElement loginInButton;

    @FindBy(xpath = "//*[@id=\"form\"]/div/div/div[3]/div/form/p")
    private WebElement emailAlreadyExistsText;
    @FindBy(xpath = "//*[@id=\"form\"]/div/div/div[1]/div/form/p")
    private WebElement emailOrPasswordInvalidMessage;

    public LoginPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver,this);
    }
    public LoginPage enterUsername(String username){
        usernameInputField.click();
        usernameInputField.sendKeys(username);
        return new LoginPage(driver);
    }
    public LoginPage enterEmailAddress(String email){
        emailInputField.click();
        emailInputField.sendKeys(email);
        return new LoginPage(driver);
    }
        public LoginPage clickSubmitButton(){
        submitButton.click();
           return new LoginPage(driver);
        }
    public SignUpPage signUp(){
        submitButton.click();
        return new SignUpPage(driver);
    }
    public LoginPage enterLoginEmail(String email){
        loginEmail.click();
        loginEmail.sendKeys(email);
        return new LoginPage(driver);
    }
    public LoginPage enterLoginPassword(String password){
        loginPassword.click();
        loginPassword.sendKeys(password);
        return new LoginPage(driver);
    }
    public HomePageSignedIn login(){
        loginInButton.click();
        return new HomePageSignedIn(driver);
    }
    public String getEmailAlreadyExistsText(){
        return emailAlreadyExistsText.getText();
    }
    public String getEmailValidationMessageForLogin(){
        return  loginEmail.getAttribute("validationMessage");
    }
    public String getPasswordValidationMessage(){
        return  loginPassword.getAttribute("validationMessage");
    }
    public String getEmailOrPasswordInvalidMessage(){
        return emailOrPasswordInvalidMessage.getText();
    }
    public String getEmailValidationMessageForSignUp(){
        return  emailInputField.getAttribute("validationMessage");
    }
    public String getUsernameValidationMessageForSignUp(){
        return  usernameInputField.getAttribute("validationMessage");
    }

}
