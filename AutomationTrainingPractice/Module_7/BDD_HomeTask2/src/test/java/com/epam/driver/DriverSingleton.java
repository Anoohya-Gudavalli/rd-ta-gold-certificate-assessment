package com.epam.driver;


import com.epam.utility.FileReader;
import com.epam.exception.BrowserNotReachable;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import static com.epam.constants.SeleniumProperties.*;

public class DriverSingleton {

private static WebDriver driver;

private DriverSingleton(){

}

    public static WebDriver getDriver() throws Exception {
        if(null==driver){
            switch (FileReader.readPropertiesFile(BROWSER_PATH).getProperty("browser")){
                case "chrome": {
                    driver =setUpChromeDriver();
                    driver.manage().window().maximize();
                    break;
                }
                case "firefox":{
                    driver = setUpFirefoxDriver();
                    driver.manage().window().maximize();
                    break;
                }
                case "edge":{
                    driver = setUpEdgeDriver();
                    driver.manage().window().maximize();
                    break;
                }
                default:{
                    throw new BrowserNotReachable("Please launch the correct browser");
                }
            }

        }
        return driver;
    }

    public static void closeDriver(){
        driver.quit();
        driver=null;
    }



    public static WebDriver setUpChromeDriver(){
        System.setProperty(CHROME_DRIVER_LOG, CHROME_DRIVER_LOG_PATH);
        System.setProperty(VERBOSE_LOGGING,VERBOSE_LOGGING_VALUE );
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments(CHROME_OPTIONS);
        WebDriverManager.chromedriver().setup();
        return new ChromeDriver(chromeOptions);

    }
    public static WebDriver setUpFirefoxDriver(){
        WebDriverManager.firefoxdriver().setup();
        return new FirefoxDriver();
    }
    public static WebDriver setUpEdgeDriver(){
    WebDriverManager.edgedriver().setup();
    return new EdgeDriver();
    }
}
