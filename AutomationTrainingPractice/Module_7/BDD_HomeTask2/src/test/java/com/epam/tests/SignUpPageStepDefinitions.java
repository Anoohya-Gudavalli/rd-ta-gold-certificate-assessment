package com.epam.tests;


import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;


public class SignUpPageStepDefinitions {

    private final static Logger LOGGER = LogManager.getLogger(SignUpPageStepDefinitions.class);

    private Base base;

    public SignUpPageStepDefinitions(Base base){
        this.base = base;
    }

    @Then("check name and email address as {string} {string}")
    public void checkNameAndEmailAddressAs(String expectedUsername, String expectedEmail) {

        base.name = base.getSignUpPage().getNameText();
        String email= base.getSignUpPage().getEmailAddressText();
        LOGGER.info("Name and Email address");
        Assert.assertEquals(base.name,expectedUsername);
        Assert.assertEquals(email,expectedEmail);
    }
    @And("fill {string},{string},{string},{string},{string},{string}")
    public void fill(String address, String countryOption, String state, String city, String zipcode, String mobileNumber) {
        base.getSignUpPage().setAddress(address)
                .selectCountry(countryOption)
                .setState(state)
                .setCity(city)
                .setZipCode(zipcode)
                .setMobileNumber(mobileNumber);
        LOGGER.info("user filled the details to signup");
    }

    @And("fill {string},{string},{string},{string},{string},{string},{string}")
    public void fillTitle( String title,String pwd, String dateOption, String monthOption, String yearOption, String first_Name, String last_Name) {

        base.getSignUpPage().setTitleInput(title)
                .setPassword(pwd)
                .selectDay(dateOption)
                .selectMonth(monthOption)
                .selectYear(yearOption)
                .setFirstName(first_Name)
                .setLastName(last_Name);
        LOGGER.info("user filled the details to signup");
    }


    @Then("user create account")
    public void userCreateAccount() {
        base.getSignUpPage().createAccount();
        LOGGER.info("user clicked on create account");

    }

}
