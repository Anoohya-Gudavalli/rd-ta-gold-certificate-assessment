package com.epam.screens;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

public class SignUpPage extends WebDriverClass{

    @FindBy(id="id_gender2")
    private WebElement titleMrs;
    @FindBy(id="id_gender1")
    private WebElement titleMr;

    @FindBy(id="name")
    private WebElement name;
    @FindBy(id="email")
    private WebElement email;
    @FindBy(id="password")
    private WebElement password;
    @FindBy(id="days")
    private WebElement daysDropdown;
    @FindBy(id="months")
    private WebElement monthsDropdown;
    @FindBy(id="years")
    private WebElement yearsDropdown;
    @FindBy(id="first_name")
    private WebElement firstName;
    @FindBy(id="last_name")
    private WebElement lastName;
    @FindBy(id="address1")
    private WebElement address1;
    @FindBy(id="address2")
    private WebElement address2;
    @FindBy(id="country")
    private WebElement countryDropdown;
    @FindBy(id="state")
    private WebElement state;
    @FindBy(id="city")
    private WebElement city;
    @FindBy(id="zipcode")
    private WebElement zipcode;
    @FindBy(id="mobile_number")
    private WebElement mobile_number;
    @FindBy(xpath = "//*[@type='submit' and @data-qa='create-account'] ")
    private WebElement createAccount;


    public SignUpPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver ,this);
    }
    public String getNameText(){
                return name.getAttribute("value");

    }
    public String getEmailAddressText(){

     return email.getAttribute("value");
    }
    public SignUpPage setTitleInput(String title) {
        if (title.equals("Mr")) {
            titleMr.click();
        } else if (title.equals("Mrs")){
            titleMrs.click();
    }
        return new SignUpPage(driver);
    }
    public SignUpPage setPassword(String pwd){
        password.click();
        password.sendKeys(pwd);
        return new SignUpPage(driver);
    }
    public SignUpPage selectDay(String dayOption) {
        Select select = new Select(daysDropdown);
        int day = Integer.parseInt(dayOption);
        List<WebElement> webElements = select.getOptions();
        webElements.forEach(webElement -> webElements.get(day).click());
        return new SignUpPage(driver);
    }
    public SignUpPage selectMonth(String monthOption) {
        Select select = new Select(monthsDropdown);
        List<WebElement> webElements = select.getOptions();
        int month = Integer.parseInt(monthOption);
        webElements.forEach(webElement -> webElements.get(month).click());
        return new SignUpPage(driver);
    }
    public SignUpPage selectYear(String yearOption) {
        Select select = new Select(yearsDropdown);
        List<WebElement> webElements = select.getOptions();
        int year = Integer.parseInt(yearOption);
        webElements.forEach(webElement -> webElements.get(year).click());
        return new SignUpPage(driver);
    }
    public SignUpPage setFirstName(String first_Name){
        firstName.click();
        firstName.sendKeys(first_Name);
        return new SignUpPage(driver);
    }
    public SignUpPage setLastName(String last_Name){
        lastName.click();
        lastName.sendKeys(last_Name);
        return new SignUpPage(driver);
    }
    public SignUpPage setAddress(String address){
        address1.click();
        address1.sendKeys(address);
        return new SignUpPage(driver);
    }
    public SignUpPage setAddress2(){
        address2.click();
        address2.sendKeys("hyd");
        return new SignUpPage(driver);
    }
    public SignUpPage selectCountry(String countryOption) {
        Select select = new Select(countryDropdown);
        List<WebElement> webElements = select.getOptions();
        int country = Integer.parseInt(countryOption);
        webElements.forEach(webElement -> webElements.get(country).click());
        return new SignUpPage(driver);
    }
    public SignUpPage setState(String stateName){
        state.click();;
        state.sendKeys(stateName);
        return new SignUpPage(driver);
    }
    public SignUpPage setCity(String cityName){
        city.click();
        city.sendKeys(cityName);
        return new SignUpPage(driver);
    }
    public SignUpPage setZipCode(String code){
        zipcode.click();
        zipcode.sendKeys(code);
        return new SignUpPage(driver);
    }
    public SignUpPage setMobileNumber(String mobileNumber){
        mobile_number.click();
        mobile_number.sendKeys(mobileNumber);
        return new SignUpPage(driver);
    }
    public AccountCreatedPage createAccount(){
        createAccount.click();
        return new AccountCreatedPage(driver);
    }


}
