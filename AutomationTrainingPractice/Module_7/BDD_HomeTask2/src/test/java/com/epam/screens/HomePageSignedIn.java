package com.epam.screens;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class HomePageSignedIn extends WebDriverClass{


    @FindBy(xpath = "//*[@href='/logout']")
    WebElement logout;

    @FindBy(xpath = "//*[@id=\"header\"]/div/div/div/div[2]/div/ul/li[10]/a/text()")
    WebElement loggedInAsText;

    @FindBy(xpath = "//*[@id=\"header\"]/div/div/div/div[2]/div/ul/li[10]/a/b")
    WebElement loggedInUser;

    public HomePageSignedIn(WebDriver driver){
       super(driver);
        PageFactory.initElements(driver,this);

    }

    public LoginPage logout(){
        WebDriverWait webDriverWait= new WebDriverWait(driver, Duration.ofSeconds(15));
        webDriverWait.until(ExpectedConditions.elementToBeClickable(logout));
        logout.click();
        return new LoginPage(driver);
    }

    public String loggedInUser(){
       return loggedInUser.getText();
    }


}
