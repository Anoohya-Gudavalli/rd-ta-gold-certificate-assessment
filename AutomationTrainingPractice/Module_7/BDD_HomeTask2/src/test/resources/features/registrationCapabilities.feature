Feature: This feature is about the registration capability in the new online shopping portal
  Background: Browser is launched with a url
    Given user launch the base url

@test
Scenario Outline: This scenario is about  clicking submit without giving email address
    Given a user navigates to Homepage
    When the user clicks on Signup
    And enter username into input field as "<username>"
    And clicks on submit button
    Then message pops up validating email address as required value and returns same page
    Examples:
      | username |
      |coco      |

@test
  Scenario Outline: This scenario is about  clicking submit without giving user name
    Given a user navigates to Homepage
    When the user clicks on Signup
    And enter email address into input field as "<email>"
    And clicks on submit button
    Then message pops up validating username as required value and returns same page
    Examples:
      | email |
      |coco123@gmail.com|

@test
  Scenario Outline: This scenario is about giving invalid email address
  Given a user navigates to Homepage
  When the user clicks on Signup
  And enter username into input field as "<username>"
  And enter email address into input field as "<email>"
  And submit the details
  Then validate error message as "<email>" is an invalid email address for sign up and returns same page
  Examples:
    | username | email |
    |coco      |coco123|

  @testRegistration
  Scenario Outline: This scenario is about  clicking submit with giving all details
    Given a user navigates to Homepage
    When the user clicks on Signup
    And enter username into input field as "<username>"
    And enter email address into input field as "<email>"
    And submit the details
    Then check name and email address as "<username>" "<email>"
    And fill "<title>","<password>","<day>","<month>","<year>","<firstName>","<lastName>"
    And fill "<address>","<country>","<state>","<city>","<zipcode>","<mobile_number>"
    And user create account
    Then check account is created
    Then Click continue
    When user clicks on logout
    Then user gets logged out
    Examples:
      | username | email | title|password | day | month | year | firstName | lastName | address | country | state | city | zipcode | mobile_number |
      |coco      |coco123@gmail.com|Mrs|1234    |6  | 4   | 20 |coco       |leaver    |hyd      |1      |telangana|hyderabad|500086 |8976453522   |
      |atlas     |atlas16@gmail.com|Mr |atlas16 |6  | 4   | 20 |atlas      |coriggan  |hyd      |1      |telangana|hyderabad|500086 |8976453534   |

  @testRegistered
  Scenario Outline: This Scenario is
    Given a user navigates to Homepage
    When the user clicks on Signup
    And enter username into input field as "<username>"
    And enter email address into input field as "<email>"
    And submit the details
    Then email already exists msg pops up
    Examples:
      | username | email |
      |coco      |coco123@gmail.com|














