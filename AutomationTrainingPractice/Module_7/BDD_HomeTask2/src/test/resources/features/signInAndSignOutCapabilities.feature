Feature: This feature is about the sign in capability in the new online shopping portal
  Background: Browser is launched with a url
    Given user launch the base url

  @signIn
  Scenario Outline: check sign in capability without giving password
    Given a user navigates to Homepage
    When the user clicks on Signup
    And user enters email as "<email>"
    And clicks on login
    Then message pops up validating password as required value and returns same page
    Examples:
      | email |
      |coco123@gmail.com|

    @signIn
  Scenario Outline: check sign in capability without giving email
    Given a user navigates to Homepage
    When the user clicks on Signup
    And user enters password as "<password>"
    And clicks on login
    Then message pops up validating email as required value and returns same page
    Examples:
      | password |
      |1234|

  @signIn
  Scenario Outline: check sign in capability  giving invalid email address
    Given a user navigates to Homepage
    When the user clicks on Signup
    And user enters email as "<email>"
    And user enters password as "<password>"
    Then clicks on login
    And validate error message as "<email>" is an invalid email address for login and returns same page
    Examples:
      | email           | password |
      |coco12           |coco      |
      |coco12           |          |



  @signIn
  Scenario Outline: check sign in capability without giving correct password
    Given a user navigates to Homepage
    When the user clicks on Signup
    And user enters email as "<email>"
    And user enters password as "<password>"
    Then clicks on login
    And validate error message as wrong email or password and returns same page
    Examples:
      | email           | password |
      |coco123@gmail.com|coco      |
      |coco1@gmail.com  |1234      |

  @signInAndSignOut
  Scenario Outline: check sign in and sign out capability
    Given a user navigates to Homepage
    When the user clicks on Signup
    And user enters email as "<email>"
    And user enters password as "<password>"
    Then clicks on login
    And user navigated to homepage after sign in as "<username>"
    When user clicks on logout
    Then user gets logged out
    Examples:
      | email | password |username|
      |coco123@gmail.com|1234|coco|
      |atlas16@gmail.com|atlas16|atlas|



