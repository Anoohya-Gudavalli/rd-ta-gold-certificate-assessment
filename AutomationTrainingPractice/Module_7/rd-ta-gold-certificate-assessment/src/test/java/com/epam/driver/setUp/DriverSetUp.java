package com.epam.driver.setUp;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import static com.epam.constants.SeleniumDriverProperties.*;

public class DriverSetUp {
    public static WebDriver setUpChromeDriver(){
        System.setProperty(CHROME_DRIVER_LOG, CHROME_DRIVER_LOG_PATH);
        System.setProperty(VERBOSE_LOGGING,VERBOSE_LOGGING_VALUE );
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments(CHROME_OPTIONS);
        WebDriverManager.chromedriver().setup();
        return new ChromeDriver(chromeOptions);

    }
    public static WebDriver setUpFirefoxDriver(){
        WebDriverManager.firefoxdriver().setup();
        return new FirefoxDriver();
    }
    public static WebDriver setUpEdgeDriver(){
        WebDriverManager.edgedriver().setup();
        return new EdgeDriver();
    }
}
