package com.epam.tests;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

public class LoginPageStepDefinitions {
    private Base base;
    public LoginPageStepDefinitions(Base base){this.base = base;}
    @When("enters the {string} and {string}")
    public void entersTheAnd(String userName, String password) {

        base.getLoginPage().enterUserNameInInputField(userName);
        base.getLoginPage().enterPasswordInInputField(password);
    }

    @And("clicks login button")
    public void clicksLoginButton() {
        base.getLoginPage().login();
    }

    @When("enters the {string}")
    public void entersThe(String userName) {
        base.getLoginPage().enterUserNameInInputField(userName);

    }

    @Then("error message pops up")
    public void errorMessagePopsUp() {
       Assert.assertEquals(base.getLoginPage().getPasswordValidationMessageForSignUp(),"Please fill out this field");
    }
}
