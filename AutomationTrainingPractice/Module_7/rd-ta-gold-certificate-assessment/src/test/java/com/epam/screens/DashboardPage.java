package com.epam.screens;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class DashboardPage extends WebDriverClass{
    @FindBy(id="userName-value")
    private WebElement userNameValue;
    public DashboardPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(this.driver,this);
    }
    public String getUserNameValue(){
        WebDriverWait webDriverWait= new WebDriverWait(driver, Duration.ofSeconds(5));
        webDriverWait.until(ExpectedConditions.elementToBeClickable(userNameValue));

        return userNameValue.getText();
    }
}
