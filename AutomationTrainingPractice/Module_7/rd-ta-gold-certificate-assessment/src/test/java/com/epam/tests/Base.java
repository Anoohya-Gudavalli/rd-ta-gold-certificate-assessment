package com.epam.tests;

import com.epam.pojos.RequestPojoForDemoQAUserLogin;
import com.epam.screens.BookStorePage;
import com.epam.screens.DashboardPage;
import com.epam.screens.DemoQAHomePage;
import com.epam.screens.LoginPage;
import com.epam.utility.TestListener;
import io.cucumber.java.en.Given;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;

@Listeners({com.epam.reportportal.testng.ReportPortalTestNGListener.class, TestListener.class})
public class Base {
    protected static WebDriver driver;
    protected static ThreadLocal<WebDriver> threadLocalDriver = new ThreadLocal<>();
    public  RequestSpecification userRequest;
    public RequestPojoForDemoQAUserLogin pojoObjectForRequest;
    public Response userResponse;

    public DemoQAHomePage demoQAHomePage;
    public LoginPage loginPage;
    public BookStorePage bookStorePage;
    public RequestSpecification userRequestForBookDetails;
    public Response userResponseForBookDetails;

    public DemoQAHomePage getDemoQAHomePage() {
        return demoQAHomePage;
    }

    public void setDemoQAHomePage(DemoQAHomePage demoQAHomePage) {
        this.demoQAHomePage = demoQAHomePage;
    }

    public LoginPage getLoginPage() {
        return loginPage;
    }

    public void setLoginPage(LoginPage loginPage) {
        this.loginPage = loginPage;
    }

    public BookStorePage getBookStorePage() {
        return bookStorePage;
    }

    public void setBookStorePage(BookStorePage bookStorePage) {
        this.bookStorePage = bookStorePage;
    }

    public DashboardPage getDashboardPage() {
        return dashboardPage;
    }

    public void setDashboardPage(DashboardPage dashboardPage) {
        this.dashboardPage = dashboardPage;
    }

    public DashboardPage dashboardPage;

    public final String BASE_URL ="https://demoqa.com/";
    @Given("user launch the base url")
    public void launchBaseURL()  {
        getDriver().get(BASE_URL);
        setDemoQAHomePage(new DemoQAHomePage(getDriver()));
        setBookStorePage(new BookStorePage(getDriver()));
        setLoginPage(new LoginPage(getDriver()));
        setDashboardPage(new DashboardPage(getDriver()));
    }
    @Given("user launch the  book url")
    public void userLaunchTheBookUrl() {
        getDriver().get("https://demoqa.com/books");
        setBookStorePage(new BookStorePage(getDriver()));
    }
    public static WebDriver getDriver(){
        return threadLocalDriver.get();
    }

    @Given("user launch the login url")
    public void userLaunchTheLoginUrl() {
        getDriver().get("https://demoqa.com/login");
        setLoginPage(new LoginPage(getDriver()));
        setDashboardPage(new DashboardPage(getDriver()));
    }
}
