package com.epam.screens;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class DemoQAHomePage extends WebDriverClass{
    @FindBy(xpath = "//*[@id=\"app\"]/div/div/div[2]/div/div[6]/div/div[1]")
    private WebElement bookStoreApplication;
    public DemoQAHomePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(this.driver,this);
    }
    public BookStorePage navigateToBookStoreApplication(){
        JavascriptExecutor javascript = (JavascriptExecutor) driver;
        javascript.executeScript("arguments[0].scrollIntoView(true);", bookStoreApplication);
        WebDriverWait webDriverWait= new WebDriverWait(driver, Duration.ofSeconds(5));
      webDriverWait.until(ExpectedConditions.elementToBeClickable(bookStoreApplication));
        bookStoreApplication.click();
        return new BookStorePage(driver);
    }
}
