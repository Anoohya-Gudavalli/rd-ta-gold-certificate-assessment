package com.epam.tests;

import io.cucumber.java.en.Given;

public class DemoQAHomePageStepDefinitions {
    private Base base;
    public DemoQAHomePageStepDefinitions(Base base){
        this.base = base;
    }

    @Given("user is navigated to bookstore page")
    public void userIsNavigatedToBookstorePage() {
        base.getDemoQAHomePage().navigateToBookStoreApplication();
    }
}
