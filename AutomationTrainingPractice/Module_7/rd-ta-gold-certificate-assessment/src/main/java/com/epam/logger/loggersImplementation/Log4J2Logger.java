package com.epam.logger.loggersImplementation;


import com.epam.logger.CustomLogger;
import com.epam.logger.LogLevel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Log4J2Logger implements CustomLogger {

    private Logger logger = LogManager.getLogger("Log4J2Logger");

    @Override
    public void log(LogLevel level, String message) {
        switch (level) {
            case DEBUG:
                logger.debug(message);
                break;
            case ERROR:
                logger.error(message);
                break;
            case INFO:
                logger.info(message);
                break;
            case WARNING:
                logger.warn(message);
                break;
        }
    }


}
