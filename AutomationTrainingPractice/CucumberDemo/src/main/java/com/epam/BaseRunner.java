package com.epam;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(plugin = {"com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:"},
        //tags= "not @search",
        tags= "@search and @selenium",
        glue = {"com.epam"},features ={"src/test/resources/features/Sample.feature"})
public class BaseRunner  extends AbstractTestNGCucumberTests
{

}