package com.epam;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.util.List;
import java.util.Map;

public class MyStepdefs {

    @BeforeAll
    public static void setUp() {
        System.out.println("before all");
    }
    @AfterAll
    public static void tearDown(){
        System.out.println("after all");
    }
    @Before(order =  3)
    public void scenarioBefore(){
        System.out.println("before scenario");
    }
    @Before(order = 1)
    public void scenarioBeforeTwo(){
        System.out.println("before scenario two");
    }
    @Before(order = 2)
    public void scenarioBeforeThree(){
        System.out.println("before scenario three");
    }
    @After
    public void scenarioAfter(){
        System.out.println("after scenario");
    }
    @BeforeStep
    public void beforeStep(){
        System.out.println("before step");
    }
    @AfterStep
    public void afterStep(){
        System.out.println("after step");
    }
    @Given("user launch the base url")
    public void userLaunchTheBaseUrl() {
    }

    @Given("a user navigates to Homepage")
    public void aUserNavigatesToHomepage() {
        System.out.println("user navigates to Homepage");
    }

    @When("the user clicks on profile")
    public void theUserClicksOnProfile() {
        System.out.println("user Clicks on profile");
    }

    @Then("username should be displayed")
    public void usernameShouldBeDisplayed() {
        System.out.println("username is displayed");
    }

    @Given("a user navigates to SearchPage")
    public void aUserNavigatesToSearchPage() {
    }
    @When("the user enters the {string} in search field")
    public void theUserEnterTheInSearchField(String searchData){
        System.out.println("search information"+searchData);
    }
    @Then("search result page should  display the results")
    public void searchResultPageShouldDisplayTheResults(){
    }
//    @When("the user enters the search data in search field")
//    public void theUserEnterTheInSearchField(DataTable dataTable){
//        System.out.println(dataTable.asList());
//    }
    @When("the user enters the search data in search field")
    public void theUserEnterTheInSearchField(List<String> dataTable){
    System.out.println(dataTable);
}

    @Given("I have {int} carrots\\(s) in my bag}")
    public void iHaveCarrotsInMyBag(int data) {
        System.out.println(data);
        }


    @When("the user enter the search data in search field")
    public void theUserEnterTheSearchDataInSearchField() {

    }

    @Then("search result page should display the results")
    public void searchDataResultPageShouldDisplayTheResults() {
    }
//    @And("fill title,password,date of birth")
//    public void fillTitlePasswordDateOfBirth(DataTable dataTable) {
//        List<Map<String, String>> list = dataTable.asMaps(String.class, String.class);
//        for (int i = 0; i < list.size(); i++) {
//            String title = list.get(i).get("title");
//            String pwd = list.get(i).get("password");
//            String datOption = list.get(i).get("day");
//            String monthOption = list.get(i).get("month");
//            String yearOption = list.get(i).get("year");
//            String first_Name = list.get(i).get("firstName");
//            String last_Name = list.get(i).get("lastName");
//            base.signUpPage.setTitleInput(title)
//                    .setPassword(pwd)
//                    .selectDay(datOption)
//                    .selectMonth(monthOption)
//                    .selectYear(yearOption)
//                    .setFirstName(first_Name)
//                    .setLastName(last_Name);
//        }
//    }
//    @And("fill address information")
//    public void fillAddressInformation(DataTable dataTable) {
//        List<Map<String, String>> list = dataTable.asMaps(String.class, String.class);
//        for (int i = 0; i < list.size(); i++) {
//            String address = list.get(i).get("address");
//            String countryOption = list.get(i).get("country");
//            String state = list.get(i).get("state");
//            String city = list.get(i).get("city");
//            String zipcode = list.get(i).get("zipcode");
//            String mobileNumber = list.get(i).get("mobile_number");
//
//            base.signUpPage.setAddress(address)
//                    .selectCountry(countryOption)
//                    .setState(state)
//                    .setCity(city)
//                    .setZipCode(zipcode)
//                    .setMobileNumber(mobileNumber);
//        }
//    }
}
