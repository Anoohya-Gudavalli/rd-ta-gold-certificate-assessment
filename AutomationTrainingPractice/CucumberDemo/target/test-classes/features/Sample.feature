Feature: This feature is about testing the functionality of Cucumber
Background: Browser is launched with a url
    Given user launch the base url


    Scenario: This scenario is about checking cucumber base functionalities
        Given a user navigates to Homepage
        When the user clicks on profile
        Then username should be displayed

    @search @selenium
    Scenario: This scenario is about checking cucumber advanced functionalities
        Given a user navigates to SearchPage
        When the user enters the "search data" in search field
        Then search result page should  display the results

    @search
    Scenario: This scenario is about using DataTables example
        When the user enter the search data in search field
        |data 1|
        |data 2|
        |data 3|
        Then search result page should display the results
    @search
    Scenario Outline: This scenario is about examples
        Given a user navigates to SearchPage
        When the user enters the "<search data>" in search field
        Then search result page should  display the results

        Examples:
            |search data|
            |data 1|
            |data 2|
            |data 3|
#    Scenario Outline: This scenario is about examples
#        Given a user navigates to SearchPage
#        When the user enters the "<File Name>" in search field
#        Then search result page should  display the results
#
#        Examples:
#            |File Name|
#            |path to the FileName.xls|

    Scenario:
        Given I have 1 carrot(s) in my bag


