import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.request;

public class ReviewTests {
        @Test()
    public void getBookDetails() {
            RestAssured.baseURI = "https://api.nytimes.com/svc/books/v3";
            RequestSpecification httpRequest = given().queryParam("isbn", "9781668001226")
                    .queryParam("title", "IT STARTS WITH US")
                    .queryParam("author", "Colleen Hoover")
                    .queryParam("api-key", "C02DyUeAArS7Gi0N0Vb9dSdNTXDyGyBj");
            Response httpResponse = httpRequest.request(Method.GET, "/reviews.json");
            httpResponse.prettyPrint();
            Assert.assertEquals(httpResponse.getStatusCode(),200);

           JsonPath j = new JsonPath(httpResponse.asString());
           int s = j.getInt("results.size()");
           for(int i = 0 ;i<s;i++){
               String book_title = j.getString("results["+i+"].book_title");
               String book_author = j.getString("results["+i+"].book_author");
               String isbn13 = j.getString("results["+i+"].isbn13["+i+"]");
               System.out.println(book_title);
               System.out.println(book_author);
               System.out.println(isbn13);
               Assert.assertEquals(book_title,"The Underground Railroad");
               Assert.assertEquals(book_author,"Colson Whitehead");
               Assert.assertEquals(isbn13,"9780385542364");

           }

            }



}
