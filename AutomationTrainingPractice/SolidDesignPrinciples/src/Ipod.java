public class Ipod extends Mobile {


    @Override
    public void sendSMS() {

    }

    @Override
    public void call() {

    }

    @Override
    public void playMusic(String fileName) {
        System.out.println("Playing music " + fileName);

    }

    @Override
    public void playVideo(String videoFileName) {
        // not applicable
    }

}
