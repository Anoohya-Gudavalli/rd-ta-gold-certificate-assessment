package OCP;

public class SendNotification {
    private NotificationClient notificationClient;
    public SendNotification( NotificationClient notificationClient) {
        this.notificationClient = notificationClient;
    }
    public void SendOTP(String medium) {
        notificationClient.sendNotification(medium);
    }
}
