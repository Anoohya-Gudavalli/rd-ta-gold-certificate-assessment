package OCP;

public interface NotificationClient {
    public void sendNotification(String userId);
}
