package DIP;

public interface Card {
    void doTransaction(int amount);
}
