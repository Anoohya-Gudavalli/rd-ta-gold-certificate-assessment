package DIP;

public class DebitCard implements Card{
    @Override
    public void doTransaction(int amount) {
        System.out.println("tx done with DebitCard");
    }
}
