package DIP;

public class CreditCard implements Card{
    @Override
    public void doTransaction(int amount) {
        System.out.println("tx done with CreditCard");
    }
}
