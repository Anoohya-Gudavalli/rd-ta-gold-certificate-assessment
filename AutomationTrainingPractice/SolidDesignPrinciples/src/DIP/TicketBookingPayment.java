package DIP;

public class TicketBookingPayment {
    private Card card;

    public TicketBookingPayment(Card card){
        this.card = card;
    }
    public void doTransaction(int amount){
        card.doTransaction(amount);
    }
}
