USE sakila;

CREATE TABLE employee_details(Emp_Id int not null UNIQUE,Emp_Name varchar(50),
		Emp_Designation varchar(50) not null,Emp_Location varchar(50) default 'India',Emp_Salary int not null);

ALTER TABLE employee_details ADD primary key (Emp_Id);

ALTER TABLE employee_details ADD (Emp_Department varchar(50));

INSERT INTO  employee_details (Emp_Id,Emp_Name,Emp_Designation,Emp_Location,
	Emp_Salary,Emp_Department) VALUES (501,'SaiTeja','Tester',default,100000,'A');

INSERT INTO  employee_details (Emp_Id,Emp_Name,Emp_Designation,Emp_Location,
	Emp_Salary,Emp_Department) VALUES (503,'Shankar','Tester',default,100000,'A');

-- INSERT INTO  employee_details (Emp_Id,Emp_Name,Emp_Designation,Emp_Location,
-- 	Emp_Salary,Emp_Department) VALUES (504,'Rohith',null,default,100000,'A');//ERROR

INSERT INTO  employee_details (Emp_Id,Emp_Name,Emp_Designation,Emp_Location,
	Emp_Salary,Emp_Department) VALUES (504,'Rohith','Developer','USA',100000,'B');

-- INSERT INTO  employee_details (Emp_Id,Emp_Name,Emp_Designation,Emp_Location,
-- 	Emp_Salary,Emp_Department) VALUES (504,'Abhinav','Developer',default,150000,'C');//ERROR

INSERT INTO  employee_details (Emp_Id,Emp_Name,Emp_Designation,Emp_Location,
	Emp_Salary,Emp_Department) VALUES (505,'Abhinav','Developer',default,150000,'C');

 INSERT INTO  employee_details (Emp_Id,Emp_Name,Emp_Designation,Emp_Location,
	Emp_Salary,Emp_Department) VALUES (506,'Sai Krishna','Tester',default,175000,'A');

USE world;

SELECT * FROM (SELECT * FROM country ORDER BY code DESC LIMIT 10) as bottom_tenRows ORDER BY code ASC;

SELECT Name,Population FROM city WHERE Population = (SELECT MAX(Population) FROM city);

SELECT * FROM country WHERE Name LIKE '%Island' AND LifeExpectancy > 70;

SELECT language ,count(language ) from countrylanguage  GROUP BY language;

SELECT count(Name) as IndependentCountries FROM country WHERE InDepYear > 1971;

SELECT city.Name as CityName,country.population,country.Name as CountryName,country.HeadOfState 
 from country INNER JOIN city on country.code = city.countrycode;

SELECT HeadOfState,count(city.Name) as CitiesCount FROM country INNER JOIN city on country.Code = city.CountryCode GROUP BY Code;

CREATE view country_details as (SELECT  city.name as City_Name,country.Name as Country_Name, Continent ,HeadOfState, language FROM city
	INNER JOIN country on city.CountryCode = country.code INNER JOIN countrylanguage on city.countrycode = countrylanguage.countrycode);

 SELECT * FROM country_details;

 ALTER view  country_details as (SELECT  city.name as City_Name,country.Name as Country_Name, Continent ,HeadOfState, language  GovernmentForm FROM city INNER JOIN country on city.CountryCode = country.code
   INNER JOIN countrylanguage on city.countrycode = countrylanguage.countrycode);

SELECT * FROM country_details;

        
