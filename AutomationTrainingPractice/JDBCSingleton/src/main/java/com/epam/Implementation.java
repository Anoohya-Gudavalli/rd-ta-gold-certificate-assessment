package com.epam;

import java.sql.*;

public class Implementation {

    public static void main(String[] args) throws SQLException {
      Connection con = CreateConnection.getDBConnection();
      try{
          String query = "select * from employee_details";
          PreparedStatement st = con.prepareStatement(query);
          ResultSet rs = st.executeQuery();
          while (rs.next() == true) {
              String name = rs.getString("Emp_Name");
              System.out.println(name);
          }
          st.close();
      }catch(Exception e){
          e.printStackTrace();
      }
        con.close();
    }
}
